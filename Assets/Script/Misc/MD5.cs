﻿using System.Security.Cryptography;
using System.IO;
using System.Linq;

namespace Hashing
{
    public static class Hash
    {
        public static byte[] hashFile(string file)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(file))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }
        public static byte[] hashFile(byte[] file)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = new MemoryStream(file))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }
        public static byte[] hashToByte(string Text)
        {
            if (string.IsNullOrEmpty(Text))
                return new byte[0];

            using (var md5 = MD5.Create())
            {
                byte[] Buffer = System.Text.Encoding.ASCII.GetBytes(Text);

                using (var stream = new MemoryStream(Buffer))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }
        public static string hashToText(string Text)
        {
            if (string.IsNullOrEmpty(Text))
                return string.Empty;

            using (var md5 = MD5.Create())
            {
                byte[] Buffer = System.Text.Encoding.ASCII.GetBytes(Text);

                using (var stream = new MemoryStream(Buffer))
                {
                    return System.Convert.ToBase64String(md5.ComputeHash(stream));
                }
            }
        }
        public static bool hashCheck(string Text, string Hash)
        {
            using (var md5 = MD5.Create())
            {
                byte[] Buffer = System.Text.Encoding.ASCII.GetBytes(Text);

                using (var stream = new MemoryStream(Buffer))
                {
                    byte[] newHash = md5.ComputeHash(stream);

                    bool isEqual = System.Convert.ToBase64String(newHash) == Hash;

                    return isEqual;
                }
            }
        }
    }
}