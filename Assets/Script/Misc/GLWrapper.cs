﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GLWrapper : MonoBehaviour
{
    public void DrawLine(Vector2 a, Vector3 b, Color clr = default(Color))
    {        
        GL.Begin(GL.LINES);

        GL.Color(clr);

        GL.Vertex3(a.x, a.y, 0);
        GL.Vertex3(b.x, b.y, 0);

        GL.End();
    }
    public void DrawCircle(float radius,int iteration,Vector2 origin,Color clr = default(Color))
    {
        if (radius <= 0)
            return;

        GL.Begin(GL.LINES);

        GL.Color(clr);

        for (int i = 0; i < iteration; i++)
        {
            float x = radius * Mathf.Cos(Mathf.Rad2Deg + Mathf.Rad2Deg * i) + origin.x;
            float y = radius * Mathf.Sin(Mathf.Rad2Deg + Mathf.Rad2Deg * i) + origin.y;
            GL.Vertex3(x, y, 0);
                  x = radius * Mathf.Cos(Mathf.Rad2Deg + Mathf.Rad2Deg * i) + origin.x;
                  y = radius * Mathf.Sin(Mathf.Rad2Deg + Mathf.Rad2Deg * i) + origin.y;
            GL.Vertex3(x, y, 0);
        }

        GL.End();
    }
    public void DrawTriangle(Vector2 a,Vector2 b,Vector2 c, Color clr = default(Color))
    {
        GL.Begin(GL.TRIANGLES);

        GL.Color(clr); 
        
        GL.Vertex3(a.x,a.y,0); 
        GL.Vertex3(b.x,b.y,0); 
        GL.Vertex3(c.x,c.y,0);

        GL.End();
    }
    public void DrawSquare(float size, Vector2 center, Color clr = default(Color))
    {
        if (size <= 0)
            return;

        GL.Begin(GL.QUADS);
        float half = size / 2;
        GL.Color(clr);

        GL.Vertex3(center.x - half, center.y + half, 0);
        GL.Vertex3(center.x + half, center.y + half, 0);
        GL.Vertex3(center.x + half, center.y - half, 0);
        GL.Vertex3(center.x - half, center.y - half, 0);

        GL.End();
    }
    public void DrawRectangle(float x, float y, Vector2 center, Color clr = default(Color))
    {
        if (x <= 0 || y <= 0)
            return;

        GL.Begin(GL.QUADS); 
        float half_x = x / 2;
        float half_y = y / 2;
        GL.Color(clr);

        GL.Vertex3(center.x - half_x, center.y + half_y, 0);
        GL.Vertex3(center.x + half_x, center.y + half_y, 0);
        GL.Vertex3(center.x + half_x, center.y - half_y, 0);
        GL.Vertex3(center.x - half_x, center.y - half_y, 0);

        GL.End();
    }
    public void DrawBounds(float thickness,Transform _t, Color clr = default(Color))
    {
        if (thickness <= 0)
            return;

        GL.Begin(GL.QUADS);
        GL.Color(clr);
        /*
            Pattern
            [1,2]
            [4,3]   
        */
        Bounds b = _t.GetComponent<Renderer>().bounds;

        //Left Side
        GL.Vertex3(b.min.x, b.max.y, 0);
        GL.Vertex3(b.min.x + thickness, b.max.y, 0);
        GL.Vertex3(b.min.x + thickness, b.min.y, 0);
        GL.Vertex3(b.min.x, b.min.y, 0);
        //Top Side
        GL.Vertex3(b.min.x, b.max.y, 0);
        GL.Vertex3(b.max.x, b.max.y, 0);
        GL.Vertex3(b.max.x, b.max.y - thickness, 0);
        GL.Vertex3(b.min.x, b.max.y - thickness, 0);
        //Bottom Side
        GL.Vertex3(b.min.x, b.min.y + thickness, 0);
        GL.Vertex3(b.max.x, b.min.y + thickness, 0);
        GL.Vertex3(b.max.x, b.min.y, 0);
        GL.Vertex3(b.min.x, b.min.y, 0);
        //Right Side
        GL.Vertex3(b.max.x - thickness, b.max.y, 0);
        GL.Vertex3(b.max.x, b.max.y, 0);
        GL.Vertex3(b.max.x, b.min.y, 0);
        GL.Vertex3(b.max.x - thickness, b.min.y, 0);

        GL.End(); //End of Quad
    }
    public void DrawHexagon(float space, Vector2 center, Color clr = default(Color))
    {
        GL.Begin(GL.LINES);

        GL.Color(clr);

        GL.Vertex3(center.x - space, center.y + space / 2, 0);
        GL.Vertex3(center.x - space, center.y - space / 2, 0);
        GL.Vertex3(center.x + space, center.y + space / 2, 0);
        GL.Vertex3(center.x + space, center.y - space / 2, 0);

        GL.Vertex3(center.x - space / 2, center.y + space, 0);
        GL.Vertex3(center.x + space / 2, center.y + space, 0);
        GL.Vertex3(center.x - space / 2, center.y - space, 0);
        GL.Vertex3(center.x + space / 2, center.y - space, 0);
    }
}
