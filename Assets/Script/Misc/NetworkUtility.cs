﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public static class NetworkUtility
{
    private static string _ip;
    public static string Ip
    {
        get
        {
            if (string.IsNullOrEmpty(_ip))
                GetLocalIp();

            return _ip;
        }
        private set
        {
            _ip = value;
        }
    }

    private static void GetLocalIp()
    {
        string ip = string.Empty;
        var Adresler = Dns.GetHostAddresses(Dns.GetHostName());

        foreach (var i in Adresler)
            if (i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                ip = i.ToString();

        //return ip;
        Ip = ip;
    }
 
    public static string Ping(string remoteIp)
    {
        const string BufferSize = "4";
        const string PingAmount = "2";

        System.Diagnostics.ProcessStartInfo sinfo = new System.Diagnostics.ProcessStartInfo(@"cmd.exe",
                        @"/C ping " + remoteIp + " -l " + BufferSize + " -n " + PingAmount)
        {
            UseShellExecute = false,
            RedirectStandardOutput = true,
            CreateNoWindow = true
        };

        System.Diagnostics.Process pcs = new System.Diagnostics.Process();
        pcs = System.Diagnostics.Process.Start(sinfo);

        string cmd = string.Empty;

        while (!pcs.StandardOutput.EndOfStream)
        {
            cmd = pcs.StandardOutput.ReadLine();
            // do something with line
        }

        pcs.WaitForExit();

        var arr = cmd.Split('=');

        string ping = arr[arr.Length - 1].TrimStart().TrimEnd();

        return ping;

    }

    #region TCP

    public static class TCP
    {

        public static Action OnSent;
        public static Action OnReceived;

        private static void OnSentMethod()
        {
            if (OnSent != null)
                OnSent();
        }
        private static void OnReceiveMethod()
        {
            if (OnReceived != null)
                OnReceived();
        }

        public static void TCP_Send(byte[] data)
        {

            System.Threading.ThreadStart x = () =>
            {
                TcpListener listener = new TcpListener(IPAddress.Any, 7779);


                listener.Start();

                TcpClient client = listener.AcceptTcpClient();

                NetworkStream stream = client.GetStream();

                stream.Write(data, 0, data.Length);

                stream.Close();

                client.Close();

                listener.Stop();

                OnSentMethod();



            };

            Thread t = new Thread(x);
            t.Start();
        }
        public static byte[] TCP_Receive(string IP)
        {
            try
            {
                TcpClient client = new TcpClient(IP, 7779);

                while (!client.Connected)
                {
                    System.Threading.Thread.Sleep(50);
                }
                NetworkStream stream = client.GetStream();

                MemoryStream mem = new MemoryStream(client.ReceiveBufferSize);

                byte[] buffer = new byte[client.ReceiveBufferSize];
                int readbyte = 0;

                do
                {
                    readbyte = 0;

                    readbyte = stream.Read(buffer, 0, buffer.Length);
                    mem.Write(buffer, 0, readbyte);
                } while (readbyte > 0);


                stream.Close();
                client.Close();

                byte[] Received = mem.ToArray();

                mem.Close();

                OnReceiveMethod();

                return Received;
            }
            catch
            {
                return null;
            }
        }
    }
    #endregion
}
