﻿using System.Collections.Generic;
using UnityEngine;

public static class TextureExtension
{
    public static Sprite CreateSprite(this Texture2D tex)
    {
        float pixels = 100 * ((float)tex.width / 200);
        Sprite sprt = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.width), new Vector2(0.5f, 0.5f), pixels);
        return sprt;
    }
    public static Texture2D ByteToTexture(this byte[] array)
    {
        Texture2D tex = new Texture2D(1, 1, TextureFormat.RGBAHalf, false);
        tex.LoadImage(array);
        return tex;
    }
}

public static class TransformExtension
{
    public static Transform FindTransform(this Transform t, string search)
    {
        if (t.name == search)
            return t;

        for (int i = 0; i < t.childCount; i++)
        {

            if (t.GetChild(i).name == search)
                return t.GetChild(i);

            Transform r = FindTransform(t.GetChild(i), search);

            if (r == null)
            {
                continue;
            }
            else
            {
                return r;
            }
        }

        return null;
    }

}

public static class UI_Extensions
{
    public static void Text(this UnityEngine.UI.Button button, string label)
    {
        button.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = label;
    }

}

public static class GameObjectExtension
{
    public static int GetPhotonId(this GameObject gameObject)
    {
        var Component = gameObject.GetComponent<PhotonView>();

        if (Component != null)
            return Component.ownerId;
        else
            return -1;
    }
}


public static class IEnumerableExtension
{
    //public static T MaxBy<T>(this IEnumerable<T> collection)
    //{

    //    T biggest = collection.Aggregate((i1, i2) => i1.ID > i2.ID ? i1 : i2);

    //}
}

