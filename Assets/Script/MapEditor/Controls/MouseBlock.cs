﻿using UnityEngine;

public class MouseBlock : MonoBehaviour
{
    public static bool isBlocked = false;

    public void MouseEnter()
    {
        isBlocked = true;
    }

    public void MouseExit()
    {
        isBlocked = false;
    }

}
