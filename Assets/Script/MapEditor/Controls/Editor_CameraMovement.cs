﻿using UnityEngine;
[RequireComponent(typeof(TweenPosition),typeof(TweenOrthoSize))]
public class Editor_CameraMovement : MonoBehaviour
{

    //Zoom
    public float CameraDefaultZoom = 5f;
    public float CameraMinZoom = 2;
    public float CameraMaxZoom = 10;
    public float CameraSpanDrag = 40;

    private bool isPointerOnMap = true;

    //Tween
    private TweenOrthoSize TweenFov;
    private TweenPosition TweenPos;

    //Move
    private Vector3 lastPosition = new Vector3(0,0,0);
    private Vector3 m_Pos = new Vector3(0,0,0);

    void Start()
    {
        TweenFov = this.GetComponent<TweenOrthoSize>();
        TweenPos = this.GetComponent<TweenPosition>();
    }

    void Update()
    {
        #region Camera_Move
        if (Input.GetMouseButton(2)) // MiddleMouse Move
        {
            //Move Camera

            lastPosition = Input.mousePosition;
            Vector3 temp = this.transform.position + ((m_Pos - lastPosition) / CameraSpanDrag);
            Vector3 to = LimitMovement(temp);

            SetPositionTween(this.transform.position, to);
        }
        else if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) //Keyboard Move
        {
            Vector3 temp = this.transform.position;
            Vector3 to = LimitMovement(new Vector3(temp.x + Input.GetAxis("Horizontal"),temp.y + Input.GetAxis("Vertical"),temp.z));

            SetPositionTween(this.transform.position, to);
        }    
        TweenPos.PlayForward();

        m_Pos = Input.mousePosition;

        #endregion

        #region Camera_Zoom
        //Change Camera Fov
        if (Input.mouseScrollDelta.y != 0 && isPointerOnMap == true)
        {
            SetZoomTween(-Input.mouseScrollDelta.y);
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            SetZoomTween(1);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            SetZoomTween(-1);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            //Revert to Game View
            SetZoomTween(CameraDefaultZoom);
        }
        #endregion
    }

    private Vector3 LimitMovement(Vector3 _t)
    {
        float min_x = (-1) * Grid.instance.GridSize_X + Grid.instance.Offset_X;
        float min_y = Grid.instance.Offset_Y;
        float max_y = Grid.instance.GridSize_Y + Grid.instance.Offset_Y;
        float x = Mathf.Clamp(_t.x, min_x, -min_x);
        float y = Mathf.Clamp(_t.y, min_y, max_y );
        float z = _t.z;
        Vector3 pos = new Vector3(x, y, z);
        return pos;
    }
    private void SetPositionTween(Vector3 from, Vector3 to)
    {
        TweenPos.from = from;
        TweenPos.to = to;
        TweenPos.ResetToBeginning();
    }
    private void SetZoomTween(float Direction)
    {
        float from = this.GetComponent<Camera>().orthographicSize;
        float to = Mathf.Clamp(from + Direction, CameraMinZoom, CameraMaxZoom);

        TweenFov.ResetToBeginning();
        TweenFov.from = from;
        TweenFov.to = to;
        TweenFov.PlayForward();
    }
    public void PointerOnUI()
    {
        isPointerOnMap = false;
    }
    public void PointerOnMap()
    {
        isPointerOnMap = true;
    }
    

}
