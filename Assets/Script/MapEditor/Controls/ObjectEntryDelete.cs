﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectEntryDelete : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        //-1 left
        //-2 right click
        if(eventData.pointerId == -2)
        {
            Destroy(this.gameObject);
        }
    }
}
