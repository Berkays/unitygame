﻿using UnityEngine;
using UnityEngine.UI;

public class Editor_ObjectMove : MonoBehaviour
{
    //UI
    public Text GizmoInfo;
    public Text ObjectInfo;
    public Material mat;


    public enum GizmoType
    {
        Move = 0,
        Rotate,
        Scale,
        None
    }
    System.Action GizmoEvent;
    GizmoType _gizmo;
    GizmoType Gizmo
    {
        get
        {
            return _gizmo;
        }

        set
        {
            if (_gizmo == GizmoType.Move)
            {
                GizmoEvent = Move;
            }
            else if (_gizmo == GizmoType.Rotate)
            {
                GizmoEvent = Rotate;
            }
            else if (_gizmo == GizmoType.Scale)//SCALE
            {
                GizmoEvent = Scale;
            }
            else //None
            {
                GizmoEvent = null;
                isGizmo = false;
                SetColliders(true);
            }
            GizmoInfo.text = "Current Gizmo : " + value.ToString();
            _gizmo = value;

        }
    }

    Transform ActiveObject;
    Vector2 RefPoint;
    bool isX = false;
    bool isGizmo = false;

    void Start()
    {
        Gizmo = GizmoType.Move;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !MouseBlock.isBlocked)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(ray.origin.x, ray.origin.y), Vector2.zero);


            if (hit == true)
            {

                if (hit.collider.tag == "Gizmo") //Gizmo Hit
                {
                    RefPoint = ActiveObject.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    isX = hit.collider.name == "X" ? true : false;
                    if (ActiveObject != null)
                    {
                        isGizmo = true;
                    }
                    return;
                }
                if (ActiveObject == null || (hit.collider.transform != ActiveObject && hit.collider.tag != "Gizmo"))
                {

                    if (ActiveObject != null)
                    {
                        SetColliders(true);
                    }
                    ActiveObject = hit.collider.transform;
                    SetColliders(false);
                }
            }
            else //No hit Remove Selection
            {
                if (ActiveObject != null)
                {
                    SetColliders(true);
                }
                ActiveObject = null;
                isGizmo = false;
            }
        }

        #region Gizmo
        if (Input.GetMouseButton(0) && ActiveObject != null && isGizmo) //Drag
        {
            GizmoEvent();
        }
        else
        {
            isGizmo = false;
        }
        #endregion
        #region ChangeGizmo
        if (Input.GetKeyDown(KeyCode.Alpha1)) //Move
        {
            Gizmo = GizmoType.Move;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) //Rotate
        {
            Gizmo = GizmoType.Rotate;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))  //Scale
        {
            Gizmo = GizmoType.Scale;
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            Gizmo = GizmoType.None;
        }
        #endregion
        #region Delete
        if (Input.GetKeyDown(KeyCode.Delete) && ActiveObject != null)
        {
            Destroy(ActiveObject.gameObject);
        }
        #endregion
        #region ObjectInfo
        if (ActiveObject == null)
        {
            ObjectInfo.text = string.Empty;
        }
        else
        {
            ObjectInfo.text = GetObjectInfo();
        }
        #endregion

    }

    void Move()
    {
        float GridX = Grid.instance.GridSize_X;
        float GridY = Grid.instance.GridSize_Y;
        float OffsetX = Grid.instance.Offset_X;
        float OffsetY = Grid.instance.Offset_Y;

        Vector2 mouseposition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector2 Pos = ActiveObject.position;
        Vector2 Snapped = SnapSystem.RoundToNearest(mouseposition + RefPoint);

        if (isX)
        {
            float Min_X = GridX + OffsetX;
            float pos = Mathf.Clamp(Snapped.x, -Min_X, Min_X);
            Pos.x = pos;
        }
        else
        {
            float pos = Mathf.Clamp(Snapped.y, 0, GridY + OffsetY);
            Pos.y = pos;
        }
        ActiveObject.position = new Vector3(Pos.x, Pos.y, 0);
    }
    void Rotate()
    {
        if (Input.GetMouseButtonUp(0))
            ActiveObject.Rotate(new Vector3(0, 0, 90));
        if (Input.GetMouseButtonUp(1))
            ActiveObject.Rotate(new Vector3(0, 0, -90));
    }
    void Scale()
    {

        if (isX)
        {
            Vector3 mouseposition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 0));
            Vector2 Snapped = SnapSystem.RoundToNearest(mouseposition);

            float pos = Mathf.Clamp(Snapped.x, 0, 5);

            ActiveObject.localScale = new Vector3(pos, ActiveObject.localScale.y, 0);

        }
        else
        {
            Vector3 mouseposition = Camera.main.ScreenToWorldPoint(new Vector3(0, Input.mousePosition.y, 0));
            Vector2 Snapped = SnapSystem.RoundToNearest(mouseposition);

            float pos = Mathf.Clamp(Snapped.y, 0, 5);

            ActiveObject.localScale = new Vector3(ActiveObject.localScale.x, pos, 0);
        }

    }

    string GetObjectInfo()
    {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();

        builder.Append("Object Type : ");
        builder.Append(ActiveObject.tag);
        builder.Append("\n\nPosition : (X,Y) = (");
        builder.Append(ActiveObject.position.x);
        builder.Append(",");
        builder.Append(ActiveObject.position.y);
        builder.Append(")\nScale     : (X,Y) = (");
        builder.Append(ActiveObject.localScale.x);
        builder.Append(",");
        builder.Append(ActiveObject.localScale.y);
        builder.Append(")");

        return builder.ToString();
    }

    void SetColliders(bool Main)
    {
        if (Main)
        {
            ActiveObject.GetComponent<BoxCollider2D>().enabled = true;
            ActiveObject.GetChild(0).GetComponent<BoxCollider2D>().enabled = false;
            ActiveObject.GetChild(1).GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            ActiveObject.GetComponent<BoxCollider2D>().enabled = false;
            ActiveObject.GetChild(0).GetComponent<BoxCollider2D>().enabled = true;
            ActiveObject.GetChild(1).GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    void OnPostRender()
    {
        mat.SetPass(0);

        if (MouseBlock.isBlocked)
        {
            GL.Begin(GL.QUADS);
            Vector2 CamPos = SnapSystem.RoundToNearest(Camera.main.transform.position);
            GL.Color(Color.yellow);
            GL.Vertex3(CamPos.x - 0.12f, CamPos.y + 0.12f, 0);
            GL.Vertex3(CamPos.x + 0.12f, CamPos.y + 0.12f, 0);
            GL.Vertex3(CamPos.x + 0.12f, CamPos.y - 0.12f, 0);
            GL.Vertex3(CamPos.x - 0.12f, CamPos.y - 0.12f, 0);
            GL.End();
        }
        

        if (ActiveObject == null)
            return;
        
        Bounds b = ActiveObject.GetComponent<SpriteRenderer>().bounds;

        #region Bounds
        GL.Begin(GL.QUADS); // Quads
        GL.Color(Color.blue);
        /*
            Pattern
            [1,2]
            [4,3]   
        */

        //Left Side
        GL.Vertex3(b.min.x, b.max.y, 0);
        GL.Vertex3(b.min.x + 0.1f, b.max.y, 0);
        GL.Vertex3(b.min.x + 0.1f, b.min.y, 0);
        GL.Vertex3(b.min.x, b.min.y, 0);
        //Top Side
        GL.Vertex3(b.min.x, b.max.y, 0);
        GL.Vertex3(b.max.x, b.max.y, 0);
        GL.Vertex3(b.max.x, b.max.y - 0.1f, 0);
        GL.Vertex3(b.min.x, b.max.y - 0.1f, 0);
        //Bottom Side
        GL.Vertex3(b.min.x, b.min.y + 0.1f, 0);
        GL.Vertex3(b.max.x, b.min.y + 0.1f, 0);
        GL.Vertex3(b.max.x, b.min.y, 0);
        GL.Vertex3(b.min.x, b.min.y, 0);
        //Right Side
        GL.Vertex3(b.max.x - 0.1f, b.max.y, 0);
        GL.Vertex3(b.max.x, b.max.y, 0);
        GL.Vertex3(b.max.x, b.min.y, 0);
        GL.Vertex3(b.max.x - 0.1f, b.min.y, 0);

        GL.End(); //End of Quad
        #endregion


        if (Gizmo == GizmoType.None)
        {
            return;
        }
        else if (Gizmo == GizmoType.Move)
        {
            #region Move
            GL.Begin(GL.TRIANGLES); //Triangles
            if (isX == false && isGizmo)
            {
                GL.Color(Color.yellow); //Y Triangle
            }
            else
            {
                GL.Color(Color.green); //Y Triangle
            }
            float a = Camera.main.orthographicSize / 5;

            GL.Vertex3(b.center.x, b.max.y + (3 * a), 0); //Top of Triangle
            GL.Vertex3(b.center.x - (0.3f * a), b.max.y + (2.4f * a), 0); //Left Vertex
            GL.Vertex3(b.center.x + (0.3f * a), b.max.y + (2.4f * a), 0); //Right Vertex

            if (isX == true && isGizmo)
            {
                GL.Color(Color.yellow); //X Triangle
            }
            else
            {
                GL.Color(Color.red); //X Triangle
            }
            GL.Vertex3(b.max.x + 3 * a, b.center.y, 0); //Top of Triangle
            GL.Vertex3(b.max.x + 2.4f * a, b.center.y + 0.3f * a, 0); //Left Vertex
            GL.Vertex3(b.max.x + 2.4f * a, b.center.y - 0.3f * a, 0); //Right Vertex

            GL.End(); //End Of Triangle

            GL.Begin(GL.LINES); //Lines

            GL.Color(Color.green); //Y Line

            GL.Vertex3(b.center.x, b.max.y + 2.4f * a, 0);
            GL.Vertex3(b.center.x, b.center.y, 0);

            GL.Color(Color.red); //X Line

            GL.Vertex3(b.center.x, b.center.y, 0);
            GL.Vertex3(b.max.x + 2.4f * a, b.center.y, 0);

            GL.End(); //End of Line
            #endregion
        }
        else if (Gizmo == GizmoType.Rotate)
        {
            #region Rotate
            GL.Begin(GL.LINES);

            GL.Color(Color.red);

            GL.Vertex3(b.min.x, b.center.y, 0);
            GL.Vertex3(b.max.x, b.center.y, 0);

            GL.Color(Color.green);

            GL.Vertex3(b.center.x, b.max.y, 0);
            GL.Vertex3(b.center.x, b.min.y, 0);
            GL.End();

            GL.Begin(GL.LINES);
            for (int i = 0; i < 100; i++)
            {
                GL.Color(Color.blue);
                float x = 3 * Mathf.Cos(Mathf.Rad2Deg * i) + b.center.x;
                float y = 3 * Mathf.Sin(Mathf.Rad2Deg * i) + b.center.y;
                GL.Vertex3(x, y, 0);
                x = 3 * Mathf.Cos(Mathf.Rad2Deg + Mathf.Rad2Deg * i) + b.center.x;
                y = 3 * Mathf.Sin(Mathf.Rad2Deg + Mathf.Rad2Deg * i) + b.center.y;
                GL.Vertex3(x, y, 0);
            }
            GL.End();
            #endregion
        }
        else if (Gizmo == GizmoType.Scale) //Scale
        {
            #region Scale
            GL.Begin(GL.LINES);

            GL.Color(Color.red); // X Line

            GL.Vertex3(b.center.x, b.center.y, 0);
            GL.Vertex3(b.max.x + 3, b.center.y, 0);

            GL.Color(Color.green); // Y Line

            GL.Vertex3(b.center.x, b.center.y, 0);
            GL.Vertex3(b.center.x, b.max.y + 3, 0);

            GL.End();

            GL.Begin(GL.QUADS); // Quad

            GL.Color(Color.red); // X Quad

            GL.Vertex3(b.max.x + 2.4f, b.center.y + 0.3f, 0);
            GL.Vertex3(b.max.x + 3, b.center.y + 0.3f, 0);
            GL.Vertex3(b.max.x + 3, b.center.y - 0.3f, 0);
            GL.Vertex3(b.max.x + 2.4f, b.center.y - 0.3f, 0);

            GL.Color(Color.green); // Y Quad

            GL.Vertex3(b.center.x - 0.3f, b.max.y + 2.4f, 0);
            GL.Vertex3(b.center.x + 0.3f, b.max.y + 2.4f, 0);
            GL.Vertex3(b.center.x + 0.3f, b.max.y + 3, 0);
            GL.Vertex3(b.center.x - 0.3f, b.max.y + 3, 0);

            GL.End();
            #endregion
        }

    }
}
