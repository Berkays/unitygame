﻿using UnityEngine;

public class SnapSystem
{


    public static Vector2 RoundToNearest(Vector3 vector)
    {
        float SnapX = Grid.instance.Snap_x;
        float SnapY = Grid.instance.Snap_y;

        float x = Mathf.Round(vector.x / SnapX) * SnapX;
        float y = Mathf.Round(vector.y / SnapY) * SnapY;

        return new Vector2(x, y);
    }

}