﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using LevelSerializerProtocol;
using Hashing;

public class LevelLoader : MonoBehaviour
{
    public static string LoadedName, LoadedAuthor, LoadedDescription;
    public static List<byte[]> Hashes = new List<byte[]>();

    public static void LoadMap(string MapName)
    {
        Level lvl = LevelDeserializer.Deserialize(MapName);

        if (lvl == null)
        {
            return; // File Invalid
        }

        CleanUp();

        #region RetrieveResources
        List<byte[]> ResourceList = new List<byte[]>();

        foreach (GameObjectData data in lvl.Objects)
        {
            if (data.ObjectType == ObjectType.Element)
            {
                byte[] image = data.Resource;
                if (image.Length == 1)
                {
                    continue;
                }
                ResourceList.Add(image);
                Hashes.Add(Hash.hashFile(image));
                ObjectPanel.instance.CreateListEntry(image.ByteToTexture());
            }                   
        }
        #endregion
        #region Populate GameObjects

        GameObject baseObject = ObjectPanel.instance.GameSpritePrefab;
        Transform ElementContainer = ObjectPanel.instance.ElementContainer;

        foreach (GameObjectData data in lvl.Objects.Where(x => x.ObjectType == ObjectType.Element))
        {

            GameObject instance = Instantiate(baseObject) as GameObject;

            instance.transform.SetParent(ElementContainer, false);
            instance.transform.eulerAngles = new Vector3(data.position.rot_x, data.position.rot_y, data.position.rot_z);
            instance.transform.localScale = new Vector3(data.position.size_x, data.position.size_y, 0);
            instance.transform.position = new Vector3(data.position.pos_x, data.position.pos_y, 0);

            Texture2D tex = ResourceList[data.ResourceName].ByteToTexture();
            instance.GetComponent<SpriteRenderer>().sprite = tex.CreateSprite(); //Texture Extension
            instance.AddMissingComponent<BoxCollider2D>();

        }
        #endregion
        #region Populate SpawnPoints
        GameObject SpawnObject = ObjectPanel.instance.SpawnPrefab;
        Transform SpawnContainer = ObjectPanel.instance.SpawnLocationContainer;

        foreach (GameObjectData data in lvl.Objects.Where(x => x.ObjectType == ObjectType.SpawnPoint))
        {
            GameObject instance = Instantiate(SpawnObject) as GameObject;

            instance.transform.SetParent(SpawnContainer, false);
            instance.transform.position = new Vector3(data.position.pos_x, data.position.pos_y, 0);
        }
        #endregion
        LoadedName = lvl.Name;
        LoadedAuthor = lvl.Author;
        LoadedDescription = lvl.Description;

        Debug.Log("Loaded Map");
    }

    private static void CleanUp()
    {
        Transform List = ObjectPanel.instance.ElementContainer;

        foreach (Transform i in List)
        {
            Destroy(i.gameObject);
        }

        List = ObjectPanel.instance.ButtonContainer;

        for (int i = 1; i < List.childCount; i++)
        {
            Destroy(List.GetChild(i).gameObject);
        }

        List = ObjectPanel.instance.SpawnLocationContainer;

        foreach (Transform i in List)
        {
            Destroy(i.gameObject);
        }


    }

}
