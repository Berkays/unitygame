﻿using UnityEngine;

using System.Collections;

public class TestGUI : MonoBehaviour
{
    public Rect Button1 = new Rect(80, 80, 100, 25);
    public Rect Button2 = new Rect(80, 130, 100, 25);
    public Rect Button3 = new Rect(80, 180, 100, 25);

    void OnGUI()
    {
        if (GUI.Button(Button1, "Save This Map"))
        {
            LevelSaver.SaveMap("Deneme", "Berkay Gursoy", "Aciklama");
        }

        if (GUI.Button(Button2, "Load From File"))
        {
            LevelLoader.LoadMap("Deneme");
        }



        if (GUI.Button(Button3, "Print Info"))
        {
            try
            {
                Debug.Log("Map Name is " + LevelLoader.LoadedName);
                Debug.Log("Map Author is " + LevelLoader.LoadedAuthor);
                Debug.Log("Description is " + LevelLoader.LoadedDescription);
            }
            catch
            { }
        }


    }
	
}
