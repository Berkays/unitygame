﻿using UnityEngine;
using System.IO;
using ProtoBuf;
using System.Collections.Generic;
using System.Linq;
using Hashing;

namespace LevelSerializerProtocol
{
    public class LevelSerializer
    {
        private const string Extension = ".map";

        public static string ReturnPath(string MapFile)
        {
            string DataPath = Application.dataPath + @"\Maps";
            if (Directory.Exists(DataPath) == false)
                Directory.CreateDirectory(DataPath);

            return DataPath + "\\" + MapFile + Extension;
        }

        public static void Serialize(string MapName, string Author, string Description)
        {
            string filepath = LevelSerializer.ReturnPath(MapName);

            if (File.Exists(filepath))
            {
                Debug.LogWarning("Map with the name " + MapName + " exists");
                Debug.LogWarning("Overwriting...");
                //return;
            }

            List<byte[]> ResourceList = new List<byte[]>(32);

            Level lvl = new Level(MapName, Author, Description);

            List<GameObjectData> LevelObjects = new List<GameObjectData>(32);

            #region Elements
            GameObject[] Elements = GameObject.FindGameObjectsWithTag("Element");
            for (int index = 0; index < Elements.Length; index++)
            {
                GameObjectData data = new GameObjectData();

                data.position = new Position();
                data.position.pos_x = Elements[index].transform.position.x;
                data.position.pos_y = Elements[index].transform.position.y;
                data.position.rot_x = Elements[index].transform.localEulerAngles.x;
                data.position.rot_y = Elements[index].transform.localEulerAngles.y;
                data.position.rot_z = Elements[index].transform.localEulerAngles.z;
                data.position.size_x = Elements[index].transform.localScale.x;
                data.position.size_y = Elements[index].transform.localScale.y;

                data.ObjectType = ObjectType.Element;

                Texture2D tex = Elements[index].GetComponent<SpriteRenderer>().sprite.texture;

                byte[] image = tex.EncodeToPNG();
                byte[] hash = Hash.hashFile(image);
                var _index = ResourceList.FindIndex(x => x.SequenceEqual(hash));
                if (_index == -1) //Unique Element
                {
                    data.ResourceName = ResourceList.Count;
                    ResourceList.Add(hash);
                    data.Resource = image;
                }
                else
                {
                    data.ResourceName = _index;
                    data.Resource = new byte[] { 0 };
                }

                //lvl.Objects[index] = data;
                LevelObjects.Add(data);
            }
            #endregion
            #region SpawnObjects
            GameObject[] SpawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");

            for (int index = 0; index < SpawnPoints.Length; index++)
            {
                GameObjectData data = new GameObjectData();

                data.position = new Position();
                data.position.pos_x = SpawnPoints[index].transform.position.x;
                data.position.pos_y = SpawnPoints[index].transform.position.y;

                data.ObjectType = ObjectType.SpawnPoint;

                LevelObjects.Add(data);
            }
            #endregion

            lvl.Objects = LevelObjects.ToArray<GameObjectData>();

            FileStream file = new FileStream(filepath, FileMode.Create, FileAccess.Write);
            ProtoBuf.Serializer.SerializeWithLengthPrefix<LevelMeta>(file, lvl.Meta, PrefixStyle.Fixed32);
            ProtoBuf.Serializer.SerializeWithLengthPrefix<GameObjectData[]>(file, lvl.Objects, PrefixStyle.Fixed32);
            file.Close();

            Debug.Log("Map Saved");
        }




        public static void Save(string mapName, byte[] data)
        {
            string path = LevelSerializer.ReturnPath(mapName);

            FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write);
            file.Write(data, 0, data.Length);
            file.Close();
        }

        public static byte[] Read(string mapName)
        {
            string path = LevelSerializer.ReturnPath(mapName); //Get map path

            byte[] data = File.ReadAllBytes(path);

            return data;
        }

        public static bool HashCheck(string mapName, byte[] md5)
        {
            string path = LevelSerializer.ReturnPath(mapName); //Get map path

            if (!File.Exists(path))
                return false;

            byte[] file = GetHash(mapName); 

            bool valid = md5 == file ? true : false;

            return valid;
        }

        public static byte[] GetHash(string mapName)
        {
            string path = LevelSerializer.ReturnPath(mapName); //Get map path

            return Hashing.Hash.hashFile(path);
        }

    }




}