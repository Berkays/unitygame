﻿using UnityEngine;
using LevelSerializerProtocol;

public class LevelSaver : MonoBehaviour
{
    public static void SaveMap(string Name, string Author, string Description)
    {
        LevelSerializer.Serialize(Name, Author, Description);
    }


    public static void PlaceBounds()
    {
        GameObject[] arr = GameObject.FindGameObjectsWithTag("Element");

        float Min_X, Min_Y;
        float Max_X, Max_Y;
        Min_X = Max_X = arr[0].transform.position.x;
        Min_Y = Max_Y = arr[0].transform.position.y;

        for (int i = 1; i < arr.Length; i++)
        {
            Bounds bounds = arr[i].GetComponent<Renderer>().bounds;
            if (Min_X > bounds.min.x)
            {
                Min_X = bounds.min.x;
            }
            if (Max_X > bounds.max.x)
            {
                Max_X = bounds.max.x;
            }
            if (Min_Y > bounds.min.y)
            {
                Max_Y = bounds.min.y;
            }
            if (Max_Y > bounds.max.y)
            {
                Max_Y = bounds.max.y;
            }
        }



    }
}
