﻿using UnityEngine;
using System.IO;
//using ProtoBuf;

namespace LevelSerializerProtocol
{
    public class LevelDeserializer
    {
        public static Level Deserialize(string MapName)
        {
            string filepath = LevelSerializer.ReturnPath(MapName);
            if (File.Exists(filepath) == false)
            {
                Debug.Log("File Not Found");
                return null;
            }

            FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            Level lvl = new Level();

            LevelMeta meta = ProtoBuf.Serializer.DeserializeWithLengthPrefix<LevelMeta>(file, ProtoBuf.PrefixStyle.Fixed32);
            GameObjectData[] data = ProtoBuf.Serializer.DeserializeWithLengthPrefix<GameObjectData[]>(file, ProtoBuf.PrefixStyle.Fixed32);

            lvl.Meta = meta;
            lvl.Objects = data;
            file.Close();

            return lvl;
        }
        public static LevelMeta DeserializeMeta(string MapName)
        {
            string filepath = LevelSerializer.ReturnPath(MapName);

            if (File.Exists(filepath) == false)
            {
                Debug.Log("File Not Found");
                throw new FileNotFoundException();
                //return null;
            }
                FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read);

                LevelMeta meta = ProtoBuf.Serializer.DeserializeWithLengthPrefix<LevelMeta>(file, ProtoBuf.PrefixStyle.Fixed32);
                file.Close();

                return meta;
        }

    }
}