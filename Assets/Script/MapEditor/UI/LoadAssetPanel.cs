﻿using System.IO;
using Hashing;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LoadAssetPanel : MonoBehaviour
{
    public InputField _FileName;

    public void OpenFileDialog()
    {
        string processpath = Application.dataPath + @"\Managed\file.exe";
        
        if (File.Exists(processpath) == false)
        {
            //Reinstall Game
            return;
        }
        System.Diagnostics.ProcessStartInfo sinfo = new System.Diagnostics.ProcessStartInfo(processpath);
        sinfo.UseShellExecute = false;
        sinfo.RedirectStandardOutput = true;
        sinfo.CreateNoWindow = true;

        System.Diagnostics.Process pcs = new System.Diagnostics.Process();
        pcs.StartInfo = sinfo;
        pcs.Start();

        string path = string.Empty;

        while (!pcs.StandardOutput.EndOfStream)
        {
            path = pcs.StandardOutput.ReadLine();
            // do something with line
        }

        pcs.WaitForExit();
        int return_code = pcs.ExitCode;

        if (return_code == 0) //Selected File
        {
            _FileName.text = path;
        }
        else
        {
            Debug.Log("No File Selected");     //NULL
        }

    }

    public void SaveAsset()
    {

        Texture2D tex = File.ReadAllBytes(_FileName.text).ByteToTexture();
        byte[] arr = Hash.hashFile(tex.EncodeToPNG()); //! Can be optimized

        if (LevelLoader.Hashes.Any(x => x.SequenceEqual(arr)) == false)
        {
            LevelLoader.Hashes.Add(arr);
            ObjectPanel.instance.CreateListEntry(tex);
        }

    }
    

}
