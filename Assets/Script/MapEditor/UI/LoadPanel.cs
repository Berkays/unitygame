﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.EditorMenu
{
    public class LoadPanel : MonoBehaviour
    {
        public InputField MapName;

        public void Button_Load()
        {
            LevelLoader.LoadMap(MapName.text);
        }

    }
}