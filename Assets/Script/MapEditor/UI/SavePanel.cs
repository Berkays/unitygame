﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.EditorMenu
{
    public class SavePanel : MonoBehaviour
    {
        public InputField SaveMapName, Author, Description;

        void Start()
        {
            if (string.IsNullOrEmpty(LevelLoader.LoadedName))
            {
                this.gameObject.SetActive(true);
                return;
            }
            try
            {
                SaveMapName.text = LevelLoader.LoadedName;
                Author.text = LevelLoader.LoadedAuthor;
                Description.text = LevelLoader.LoadedDescription;
            }
            catch
            {
                Debug.Log("No Level Loaded");
            }
        }
        public void Button_Save()
        {
            LevelSaver.SaveMap(SaveMapName.text, Author.text, Description.text);

            //TODO Save prompt
        }

    }
}
