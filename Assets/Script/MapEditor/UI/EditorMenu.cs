﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameUI.EditorMenu
{
    public class EditorMenu : MonoBehaviour
    {
        public static Transform ObjectsPanel,LoadResourcePanel, SaveMapPanel, LoadMapPanel;

        public Transform ButtonContainer;
        public Transform PanelContainer;
        
        private bool isOpened = false;

        void Start()
        {
            ObjectsPanel = PanelContainer.GetChild(0);
            LoadResourcePanel = PanelContainer.GetChild(1);
            SaveMapPanel = PanelContainer.GetChild(2);
            LoadMapPanel = PanelContainer.GetChild(3);

            ObjectsPanel.gameObject.SetActive(true);
        }

        public void GoBack()
        {
            isOpened = false;
            for (int i = 1; i < PanelContainer.childCount; i++)
            {
                PanelContainer.GetChild(i).gameObject.SetActive(false);
            }
        }

        public void OpenPanel(int index)
        {
            if (isOpened == true) //Return if any panel is active
                return;

            if (!(index == 0 || index == 4)) //Object and Quit Buttons shouldnt effect other panels
            {
                isOpened = true;
            }

            switch (index)
            {
                case 0:
                    ObjectsPanel.gameObject.SetActive(true);
                    break;
                case 1:
                    LoadResourcePanel.gameObject.SetActive(true);
                    break;
                case 2:
                    SaveMapPanel.gameObject.SetActive(true);
                    break;
                case 3:
                    LoadMapPanel.gameObject.SetActive(true);
                    break;
                case 4:
                    //Application.LoadLevel(0); //Exit to menu
                    SceneManager.LoadScene("MainMenu");
                                              //TODO prompt save
                    break;

            }

        }

    }
}