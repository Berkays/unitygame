﻿using UnityEngine;
using UnityEngine.UI;

public class ObjectPanel : MonoBehaviour
{
    public static ObjectPanel instance; //Use GetComponent

    public GameObject GameSpritePrefab;
    public GameObject ButtonPrefab;
    public GameObject SpawnPrefab;

    public Transform ButtonContainer;
    public Transform ElementContainer;
    public Transform SpawnLocationContainer;
    
    // Use this for initialization
    void Start ()
    {
        instance = this;
	}

    public void CreateListEntry(Texture2D tex)
    {
        GameObject entry = Instantiate(ButtonPrefab) as GameObject;
        entry.transform.SetParent(ButtonContainer);
        entry.GetComponentInChildren<RawImage>().texture = tex;
        
        entry.GetComponent<Button>().onClick.AddListener(() =>
        {
            GameObject obj = Instantiate<GameObject>(GameSpritePrefab);
            obj.transform.SetParent(ElementContainer);
            obj.GetComponent<SpriteRenderer>().sprite = tex.CreateSprite(); //Texture Extension
            obj.transform.position = SnapSystem.RoundToNearest(Camera.main.transform.position);
            //obj.transform.localScale = new Vector3(Grid.instance.Snap_x / 2, Grid.instance.Snap_y / 2, 0);
            obj.transform.localScale = new Vector3(1, 1, 0);
            obj.AddMissingComponent<BoxCollider2D>();

        });
        
    }

    public void CreateSpawnPoint()
    {
        GameObject spawnpoint = Instantiate(SpawnPrefab) as GameObject;
        spawnpoint.transform.SetParent(SpawnLocationContainer);
        spawnpoint.transform.position = SnapSystem.RoundToNearest(Camera.main.transform.position);
    }

}
