﻿using UnityEngine;

public class Grid : MonoBehaviour
{
    public static Grid instance;

    public int GridSize_X = 20;
    public int GridSize_Y = 5;
    public float Offset_X = 0.5f;
    public float Offset_Y = 5;
    public float Snap_x = 1f;
    public float Snap_y = 1f;

    public Material lineMaterial;
    public Color GridColor;

    void Start()
    {
        instance = this;
    }
    void OnPostRender()
    {
        if (Snap_y <= 0 || Snap_x <= 0)
        {
            Snap_x = Snap_y = 1;
        }
        lineMaterial.SetPass(0);
        GL.Begin(GL.LINES);
        GL.Color(GridColor);

        for (float x = (-1) * GridSize_X + Offset_X; x < GridSize_X + Offset_X; x += Snap_x) // Vertical Axis
        {
            GL.Vertex3(x, GridSize_Y + Offset_Y, 0);
            GL.Vertex3(x, Offset_Y, 0);
        }
        for (float y = Offset_Y; y < GridSize_Y + Offset_Y; y += Snap_y) // Horizontal Axis
        {
            GL.Vertex3(GridSize_X + Offset_X, y, 0);
            GL.Vertex3((-1) * GridSize_X + Offset_X, y, 0);
        }

        GL.Color(Color.red);
        GL.Vertex3((-1) * GridSize_X + Offset_X, Offset_Y, 0); // Bottom Line
        GL.Vertex3(GridSize_X + Offset_X, Offset_Y, 0);

        GL.Vertex3((-1) * GridSize_X + Offset_X, GridSize_Y + Offset_Y, 0); //Top Line
        GL.Vertex3(GridSize_X + Offset_X,GridSize_Y + Offset_Y, 0);

        GL.Vertex3((-1) * GridSize_X + Offset_X, GridSize_Y + Offset_Y, 0); //Left Line
        GL.Vertex3((-1) * GridSize_X + Offset_X, Offset_Y, 0);

        GL.Vertex3(GridSize_X + Offset_X, GridSize_Y + Offset_Y, 0); //Right Line
        GL.Vertex3(GridSize_X + Offset_X, Offset_Y, 0);

        GL.End();
    }
}
