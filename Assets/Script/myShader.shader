﻿Shader "Berkay/UnlitTest"
{
	Properties
	{
		_Color ("Main Color",Color) = (0,0,0,1)
	}

	SubShader
	{

	Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

		struct appdata
		{
			float4 vertex : POSITION;
		};

		struct v2f
		{
			float4 fvertex : SV_POSITION;
		};

		fixed4 _Color;

		v2f vert(appdata IN)
		{
			v2f Out;
			Out.fvertex = mul(UNITY_MATRIX_MVP, IN.vertex);
			return Out;
		}

		fixed4 frag(v2f somevert) : COLOR
		{
			return _Color;
		}

		ENDCG
	}

	}
}