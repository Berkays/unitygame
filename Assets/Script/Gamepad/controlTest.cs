﻿using UnityEngine;
using System.Collections;

using XInputDotNetPure;
 
public class controlTest : MonoBehaviour
{

    GamePadState State;
    GamePadState prevState;

    int counter = 0;
	// Update is called once per frame
	void Update ()
    {
        prevState = State;
        State = GamePad.GetState(XInputDotNetPure.PlayerIndex.One);

        if (!State.IsConnected)
        {
            return;
        }

        if (prevState.Buttons.A == ButtonState.Released && State.Buttons.A == ButtonState.Pressed)
        {
            counter++;
        }
        if (prevState.Buttons.B == ButtonState.Released && State.Buttons.B == ButtonState.Pressed)
        {
            for (int i = 0; i < counter; i++)
            {
                GamePad.SetVibration(PlayerIndex.One, 1f, 1f);
                System.Threading.Thread.Sleep(300);
                GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
                System.Threading.Thread.Sleep(300);

            }
            counter = 0;
        }

    }
}
