﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loadScreen : Photon.MonoBehaviour
{
    public Image slider;
    public Text text;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine("test");
	}

    IEnumerator test()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(3);
        operation.allowSceneActivation = false;

        slider.fillAmount = 0;

        while (!operation.isDone)
        {
            slider.fillAmount = operation.progress;
            text.text = "Loading... ( % " + (int)(operation.progress * 100) + " )";
            yield return new WaitForEndOfFrame();
        }

        slider.fillAmount = 1;
        text.text = "Loading... ( % 100 )";

        yield return new WaitForSeconds(1);

        operation.allowSceneActivation = true;

        SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
    }
	
}
