﻿using UnityEngine;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;

[ExecuteInEditMode]
public class MenuHelper : MonoBehaviour
{

    class Group
    {
        public string Name { get; private set; }
        Element[] Elements;

        public Group(string Name, Transform[] Selection)
        {
            this.Name = Name;
            this.Elements = new Element[Selection.Length];

            for (int i = 0; i < Selection.Length; i++)
            {
                Element e = new Element()
                {
                    tform = Selection[i],
                    State = Selection[i].gameObject.activeSelf
                };
                this.Elements[i] = e;
            }
        }
        public Group(string Name, Element[] Elements)
        {
            this.Name = Name;
            this.Elements = Elements;
        }

        public Element[] GetElements()
        {
            return Elements;
        }

        public class Element
        {
            public Transform tform;
            public bool State;
        }
    }


    List<Group> Groups = new List<Group>();
    public uint GroupCount;

    public event System.Action GroupAdded;
    public event System.Action GroupRemoved;

    void Start()
    {
        GroupCount = 0;
    }

    /// <summary>
    /// Adds Group to the list
    /// </summary>
    /// <param name="Name">Name of the group</param>
    /// <param name="Selected">Selected Transforms</param>
    public void AddGroup(string Name, Transform[] Selected)
    {
        bool DoesExist = Groups.Any(x => x.Name == Name); //Check if the group exists.
        if (DoesExist)
            RemoveGroup(Name);


        Group g = new Group(Name, Selected);
        Groups.Add(g);

        GroupCount++;

        if (GroupAdded != null)
            GroupAdded();

    }

    /// <summary>
    /// Remove a group
    /// </summary>
    /// <param name="Name">Name of the group to be removed</param>
    /// <returns></returns>
    public bool RemoveGroup(string Name)
    {
        int Index = Groups.FindIndex(x => x.Name == Name); // Check if group exists
        if (Index == -1)
        {
            return false;
        }
        else
        {
            Groups.RemoveAt(Index);
            GroupCount--;

            if (GroupRemoved != null)
                GroupRemoved();

            return true;
        }
    }

    /// <summary>
    /// Removes all groups and removes xml data file
    /// </summary>
    public void RemoveAllGroups()
    {
        Groups.Clear();
        GroupCount = 0;
        Delete(); //Delete Xml File
    }

    /// <summary>
    /// Triggers the group
    /// </summary>
    /// <param name="Name">Group name to be triggered</param>
    public void Trigger(string Name)
    {
        foreach (var i in Groups.Where(x => x.Name == Name).First().GetElements()) //Get the elements from the group
        {
            i.tform.gameObject.SetActive(i.State);
        }
    }

    /// <summary>
    /// Get groups for the UI
    /// </summary>
    /// <returns></returns>
    public string[] GetNameList()
    {
        try
        {
            var Names = from g in Groups
                        select g.Name;

            return Names.ToArray();
        }
        catch
        {
            return new string[0];
        }
    }


    /// <summary>
    /// Saves groups data to xml with XML To Linq
    /// <para />
    /// Returns if no element exists in the group
    /// </summary>
    public bool Save()
    {
        if (Groups.Count == 0)
            return false;
       

        XElement[] arr = new XElement[Groups.Count];

        for (int i = 0; i < Groups.Count; i++)
        {
            List<XElement> xelem = new List<XElement>();

            //var dup = Groups[i].GetElements().GroupBy(x => x.tform.name).Where(g => g.Count() > 1);
            var dupe = Groups[i].GetElements().GroupBy(x => x.tform.name).Where(g => g.Count() > 1).SelectMany(x => x).Select(x => x.tform.name);

            foreach (var e in Groups[i].GetElements())
            {
                if (dupe.Contains(e.tform.name) == true)
                    continue;
                xelem.Add(new XElement("Object", new XAttribute("Name", e.tform.gameObject.name), new XAttribute("State", e.State)));
            }
            arr[i] = new XElement("Group", new XAttribute("Name", Groups[i].Name), xelem.ToArray());
        }

        XDocument doc = new XDocument(
            new XDeclaration("1.0", "utf-8", "false"),
                new XElement("Groups",
                    arr));


        string SceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        string XmlPath = Application.dataPath + @"/MenuHelperData(" + SceneName + ").xml";

        doc.Save(XmlPath);

        return true;

    }
    public bool Load()
    {
        string SceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        
        try
        {
            string XmlPath = Application.dataPath + @"/MenuHelperData(" + SceneName + ").xml";
            XDocument doc = XDocument.Load(XmlPath);

            Groups = new List<Group>();
            GroupCount = 0;

            foreach (var group in doc.Elements("Groups").Elements("Group"))
            {
                string GroupName = group.Attribute("Name").Value;

                List<Group.Element> listOfElements = new List<Group.Element>();

                foreach (var elem in group.Elements("Object"))
                {
                    Group.Element element = new Group.Element()
                    {
                        tform = FindTransform(GameObject.Find("MainMenu").transform,elem.Attribute("Name").Value),
                        State = bool.Parse(elem.Attribute("State").Value)
                    };

                    listOfElements.Add(element);
                }

                Groups.Add(new Group(GroupName, listOfElements.ToArray()));
                GroupCount++;
            }
        }
        catch (System.IO.FileNotFoundException)
        {
            return false;
        }

        return true;
    }
    public void Delete()
    {
        string SceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

        try
        {
            string XmlPath = Application.dataPath + @"/MenuHelperData(" + SceneName + ").xml";
            System.IO.File.Delete(XmlPath);
        }
        catch (System.IO.FileNotFoundException)
        {
            //Nothing to worry about.
        }
    }

    /// <summary>
    /// Finds the transform beneath the first parameter even if its inactive<para />
    /// Name needs to be unique
    /// </summary>
    /// <param name="t"></param>
    /// <param name="search"></param>
    /// <returns></returns>
    public Transform FindTransform(Transform t,string search)
    {
        if (t.name == search)
            return t;

        for (int i = 0; i < t.childCount; i++)
        {

            if (t.GetChild(i).name == search)
                return t.GetChild(i);

            Transform r = FindTransform(t.GetChild(i), search);

            if (r == null)
            {
                continue;
            }
            else
            {
                return r;
            }
        }

        return null;
    }


}


