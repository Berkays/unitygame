﻿using UnityEngine;


namespace GameUI
{
    public class Loader : MonoBehaviour
    {
        const int TIME = 9;
        // Use this for initialization
        void Start()
        {
            Invoke("LoadMenu", TIME);
        }

        // Update is called once per frame
        void LoadMenu()
        {
            UnityEngine.SceneManagement.SceneManager.UnloadScene(0);
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }

}
