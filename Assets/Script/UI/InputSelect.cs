﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputSelect : MonoBehaviour
{
    public GameObject DefaultObject;
    public Axis axis = Axis.Horizontal;
    Vector2 lastPositon;

    public enum Axis
    {
        Horizontal,
        Vertical
    }

    public static void ResetCurrent()
    {
        EventSystem.current.SetSelectedGameObject(null);
    }

    void Update()
    {
        Vector2 CurrentPosition = Input.mousePosition;


        if (CurrentPosition != lastPositon || Input.GetAxis("Submit") > 0)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
        else if (Input.GetAxis(axis.ToString()) != 0 && EventSystem.current.IsPointerOverGameObject() == false)
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                EventSystem.current.SetSelectedGameObject(DefaultObject);
            }

        }

        lastPositon = Input.mousePosition;
    }
}
