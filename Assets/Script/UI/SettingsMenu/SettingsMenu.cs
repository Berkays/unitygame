﻿using UnityEngine;

namespace GameUI.SettingsMenu
{
    public class SettingsMenu : MonoBehaviour
    {
        private Transform[] Panels;

        public GameObject PanelContainer;
        public GameObject ButtonContainer;

        void Start()
        {
            Panels = new Transform[4];
            for (int i = 0; i < 4; i++)
                Panels[i] = PanelContainer.transform.GetChild(i);
        }


        public void GoBack()
        {
            foreach (Transform tf in PanelContainer.transform)
            {
                tf.gameObject.SetActive(false);
            }
            PanelContainer.SetActive(false);
            ButtonContainer.SetActive(true);
        }
        private void SetState()
        {
            PanelContainer.SetActive(true);
            ButtonContainer.SetActive(false);
        }

        public void Open_Panel(int Index)
        {
            SetState();

            Panels[Index].gameObject.SetActive(true);
        }

    }
}