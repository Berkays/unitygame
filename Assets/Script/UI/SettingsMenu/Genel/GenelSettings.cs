﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameUI.SettingsMenu.GeneralSettings
{
    public class GenelSettings : MonoBehaviour
    {
        string PlayerName;
        public InputField NameField;

        void Start() // TODO ChangeText for Every Open
        {
            PlayerName = PlayerPrefs.GetString("PlayerName", "");

            if (string.IsNullOrEmpty(PlayerName))
            {
                string placeholder = Random.Range(100000, 1000000).ToString();
                PlayerName = "Player(" + placeholder + ")";
                PlayerPrefs.SetString("PlayerName", PlayerName);
            }

            NameField.text = PlayerName;

        }
        public void Revert()
        {
            NameField.text = PlayerName;
        }
        public void SaveSettings()
        {
            PlayerPrefs.SetString("PlayerName", NameField.text);
            PlayerPrefs.Save();
        }
    }
}