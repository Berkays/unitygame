﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

namespace GameUI.SettingsMenu.SoundSettings
{
    public class SoundSettings : MonoBehaviour
    {
        public AudioMixer Mixer;
        public Slider MusicSlider, FXSlider;


        int MusicVolume, EffectsVolume;

        public void Init()
        {
            MusicVolume = PlayerPrefs.GetInt("MusicVol", 0);
            EffectsVolume = PlayerPrefs.GetInt("FxVol", 0);
            MusicSlider.value = MusicVolume;
            FXSlider.value = EffectsVolume;
            Mixer.SetFloat("musicVol", MusicVolume);
            Mixer.SetFloat("effectVol", EffectsVolume);
        }

        public void ValueChange()
        {
            Mixer.SetFloat("musicVol", MusicSlider.value);
            Mixer.SetFloat("effectVol", FXSlider.value);
        }

        public void Revert()
        {
            Mixer.SetFloat("musicVol", MusicVolume);
            Mixer.SetFloat("effectVol", EffectsVolume);
            MusicSlider.value = MusicVolume;
            FXSlider.value = EffectsVolume;
        }

        public void SaveSettings()
        {
            MusicVolume = (int)MusicSlider.value;
            EffectsVolume = (int)FXSlider.value;
            PlayerPrefs.SetInt("MusicVol", (int)MusicSlider.value);
            PlayerPrefs.SetInt("FxVol", (int)FXSlider.value);
        }

    }
}