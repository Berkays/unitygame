﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.SettingsMenu.VideoSettings
{
    public class VideoSettings : MonoBehaviour
    {

        public Dropdown ResolutionDropdown;
        public Text Vsync;


        private Resolution TempRes;
        private int VsyncControl = 1;

        public void Init()
        {
            #region Resolution
            foreach (var i in Screen.resolutions) //Populate Supported Resolutions
            {
                Dropdown.OptionData data = new Dropdown.OptionData(i.width.ToString() + "x" + i.height.ToString());
                ResolutionDropdown.options.Add(data);
            }

            ResolutionDropdown.options.Reverse();

            TempRes = new Resolution();

            try
            {
                TempRes.height = PlayerPrefs.GetInt("DefaultResH");
                TempRes.width = PlayerPrefs.GetInt("DefaultResW");
            }
            catch (System.FormatException)
            {
                TempRes = Screen.currentResolution;
            }
            #endregion


            #region Vsync
            VsyncControl = PlayerPrefs.GetInt("DefaultVsync", 1);
            if (VsyncControl == 0)
                Vsync_Event();
            #endregion
        }

        public void SetResolution()
        {

            string[] res_data = ResolutionDropdown.captionText.text.Split('x');
            try
            {
                int width = int.Parse(res_data[0]);
                int height = int.Parse(res_data[1]);

                if (height == Screen.currentResolution.height || width == Screen.currentResolution.width)
                    return;

                if (ResolutionDropdown.value == 0)
                {
                    Screen.SetResolution(width, height, true);
                }
                else
                {
                    Screen.SetResolution(width, height, false);
                }
                //Application.LoadLevel(0);
            }
            catch (System.FormatException)
            {
                Debug.LogWarning("FormatERROR");
            }


        }
        public void Vsync_Event()
        {
            if (VsyncControl == 1)
            {
                Vsync.text = "Disabled";
                VsyncControl = 0;
            }
            else
            {
                Vsync.text = "Enabled";
                VsyncControl = 1;
            }
        }
        public void Revert()
        {
            if (Screen.currentResolution.height != TempRes.height)
                Screen.SetResolution(TempRes.width, TempRes.height, false);
        }

        public void SaveOptions()
        {
            TempRes = Screen.currentResolution;
            SetResolution();
            PlayerPrefs.SetInt("DefaultResW", TempRes.width);
            PlayerPrefs.SetInt("DefaultResH", TempRes.height);
            PlayerPrefs.SetInt("DefaultVsync", VsyncControl);

        }
    }
}