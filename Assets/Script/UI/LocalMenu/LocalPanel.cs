﻿using UnityEngine;

using Game.NetworkUtility;

namespace GameUI.LocalPlayMenu
{
    public class LocalPanel : MonoBehaviour
    {
        public GameObject HostPanel, ClientPanel;
        public GameObject ConnectPanel, PasswordPanel;

        private ClientPanel client_component;


        void Start()
        {
            client_component = ClientPanel.GetComponent<ClientPanel>();
        }
        #region Toggle
        public void EnableHostPanel()
        {
            if (HostPanel.activeSelf)
                return;

            client_component.Disconnect();

            HostPanel.SetActive(true);
            ClientPanel.SetActive(false);

        }
        public void EnableClientPanel()
        {
            if (ClientPanel.activeSelf)
                return;

            ClientPanel.SetActive(true);
            HostPanel.SetActive(false);

            //? FIX
            ClientPanel.GetComponent<ClientPanel>().Refresh_On();
        }
        #endregion

        public void CloseDialog()
        {
            if (HostPanel.activeSelf == true)
            {
                HostPanel.GetComponent<HostPanel>().Button_DisconnectHost();
                UDP_Broadcast.StopBroadcast();
            }
            else
            {
                client_component.Disconnect();
                UDP_Receive.StopReceive();
                //UDP_Receive.StopThread();
            }

        }



    }
}