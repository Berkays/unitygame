﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.LocalPlayMenu
{
    public class PlayerCountTool : MonoBehaviour
    {
        [Range(3, 32)]
        public int MaxPlayer;
        [Range(2, 30)]
        public int MinPlayer;

        private Text component;
        private int counter = 4;


        void Start()
        {
            component = this.GetComponentInChildren<Text>();
        }

        public void IncreaseCounter()
        {
            counter++;

            counter = Mathf.Clamp(counter, MinPlayer, MaxPlayer);

            component.text = counter.ToString();
        }
        public void DecreaseCounter()
        {
            counter--;

            counter = Mathf.Clamp(counter, MinPlayer, MaxPlayer);

            component.text = counter.ToString();
        }

    }
}