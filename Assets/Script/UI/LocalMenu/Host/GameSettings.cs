﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ServerDataSerialization;

using Game.NetworkUtility; //UDP Message

namespace GameUI.LocalPlayMenu
{
    public class GameSettings : MonoBehaviour
    {

        void Start()
        {
            InitialLoad();
        }
        public void InitialLoad()
        {
            ServerSync.serverData.SetValueEvent( () => { try { UDP_Broadcast.SetMessage(ServerSync.serverData.ToString()); } catch { } });
            LoadMaps(); // Load Maps from Disk
            GetPowerUps();
        }

        #region Map
        public Text SelectedMap;
        List<string> MapList = new List<string>(16);
        int MapCounter = 0;


        public void LoadMaps()
        {

            string path = Application.dataPath + @"\Maps\";

            if (Directory.Exists(path) == false)
            {
                //Corrupted
                return;
            }

            MapList.Clear();

            foreach (string file in Directory.GetFiles(path))
            {
                if (file.EndsWith(".map"))
                {
                    FileInfo info = new FileInfo(file);

                    string map = info.Name.Substring(0, info.Name.Length - 4);
                    if (MapList.Contains(map) == false)
                        MapList.Add(map);
                }
            }
            //foreach (var item in Resources.LoadAll("test") as TextAsset[])
            //{
            //    Debug.Log(item.name);
            //    MemoryStream ms = new MemoryStream(item.bytes);
            //    LevelSerializerProtocol.LevelMeta meta = ProtoBuf.Serializer.DeserializeWithLengthPrefix<LevelSerializerProtocol.LevelMeta>(ms, ProtoBuf.PrefixStyle.Fixed32);
            //    ms.Close();
            //    MapList.Add(meta.Name);
            //}


            if (MapList.Count == 0)
            {
                SelectedMap.text = "No Maps Found";
            }
            else
            {
                SelectedMap.text = MapList[0];
                ServerSync.serverData.Map_Name = MapList[0];
            }


        }
        public void ChangeMap(int i)
        {

            if (MapList.Count == 0)
                return;


            MapCounter += i;

            if (MapCounter < 0)
            {
                //return to end
                MapCounter = MapList.Count - 1;
            }
            else if (MapCounter > MapList.Count - 1)
            {
                //return to start
                MapCounter = 0;
            }

            SelectedMap.text = MapList[MapCounter];
            ServerSync.serverData.Map_Name = MapList[MapCounter];
        }
        #endregion
        #region GameType

        public Dropdown SelectedType;
        public Transform UI_Timed, UI_Score;
        private GameType selection = GameType.Timed;

        public void OnGameTypeChanged()
        {
            selection = (GameType)SelectedType.value;

            //Set Server Data
            ServerSync.serverData.GameType = selection;

            if (selection == GameType.Timed)
            {
                //Time Input
                UI_Timed.gameObject.SetActive(true);
                UI_Score.gameObject.SetActive(false);

                //Time Serverdata

            }
            else if (selection == GameType.Scored)
            {
                UI_Score.gameObject.SetActive(true);
                UI_Timed.gameObject.SetActive(false);
                //Score Input
                //Score Serverdata
            }
            else if (selection == GameType.TimedAndScored)
            {
                //Both Up
            }
            else if (selection == GameType.HoldTheFlag)
            {
                //Time
            }
        }
        public void OnTypeParameterChanged(Text element)
        {

            int Value = int.Parse(element.text.Split(' ')[0]);
            if (selection == GameType.Timed)
            {
                ServerSync.serverData.SetParameters(Time: Value);
            }
            else if (selection == GameType.Scored)
            {
                ServerSync.serverData.SetParameters(Score: Value);
            }

        }
        #endregion
        #region PowerUps

        public InputField PowerInput;
        private void GetPowerUps()
        {
            var Powers = System.Reflection.Assembly.GetAssembly(typeof(PowerUpSystem.PowerUpBase)).GetTypes().Where(x => x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Self)) || x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Enemy)));
            //var query = from p in Powers
            //            select new Dropdown.OptionData
            //            {
            //                text = p.Name
            //            };
            //PowerDropdown.options.AddRange(query);
            //PowerDropdown.value = 0

            var query = from p in Powers
                        select p.Name;
            
            string Selected = string.Join(";", query.ToArray());
            ServerSync.serverData.PowerUps = PowerInput.text = Selected;
        }

        #endregion
    }
}