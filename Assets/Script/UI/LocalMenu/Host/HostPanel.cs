﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Linq;
using BeardedManStudios.Network;

using Game.NetworkUtility; //UDP Broadcast

namespace GameUI.LocalPlayMenu
{
    public class HostPanel : MonoBehaviour
    {
        public CanvasGroup HostCanvasGroup, ToggleGroup;

        public Transform PlayerList; //Lobby Player Parent
        public GameObject PlayerEntry; //Lobby Player Prefab

        public Text Label_ServerIP, Label_PlayerCount, Label_GameType, Label_MapName; //Labels
        public InputField Field_ServerName, Field_PlayerCount, Field_Password; //Fields
        public Button Btn_Create, Btn_Disconnect; //Buttons

        #region FORGE NETWORKING
        private NetWorker socket = null;
        public ushort port;
        public Networking.TransportationProtocolType protocolType = Networking.TransportationProtocolType.UDP;
        public bool useNatHolePunching = false;
        #endregion

        void Start()
        {
            Btn_Create.onClick.AddListener(Button_HostCreate);
        }

        #region Button_Events
        public void Button_HostCreate()
        {
            UI_Host();

            int MaxPlayerCount = int.Parse(Field_PlayerCount.text) - 1;
            socket = Networking.Host(port, protocolType, MaxPlayerCount);

            AddEvents();
        }
        public void Button_DisconnectHost()
        {
            try
            {
                UDP_Broadcast.StopBroadcast();
                Networking.NetworkingReset();

                foreach (GameObject i in PlayerList)
                    Destroy(i);
            }
            catch
            { }
        }
        #endregion

        #region Events
        public void Connected()
        {

            ServerSync.serverData.Server_Name = Field_ServerName.text;
            ServerSync.serverData.Server_Ip = NetworkUtility.Ip;
            ServerSync.serverData.MaxPlayerCount = int.Parse(Field_PlayerCount.text);
            ServerSync.serverData.Password = Hashing.Hash.hashToByte(Field_Password.text.Trim());
            ServerSync.serverData.PlayerCount = 0;

            UDP_Broadcast.SetMessage(ServerSync.serverData.ToString());
            UDP_Broadcast.StartBroadcast();

            Networking.SetPrimarySocket(socket);
            Networking.PrimarySocket.Me.SetName(PlayerPrefs.GetString("PlayerName"));
            OnPlayer_Connected(Networking.PrimarySocket.Me); // Add Server Player To Lobby

            UI_Connected(); //Disable UI Elements
        }
        public void Disconnected()
        {
            UI_Disconnected(); //Enable UI Elements
            UDP_Broadcast.StopBroadcast();

        }

        private void OnPlayer_Connected(NetworkingPlayer player)
        {
            Networking.PrimarySocket.GetNewPlayerUpdates(); //Get Updates From Player
            StartCoroutine("IOnPlayer_Connected", player);
        }
        private IEnumerator IOnPlayer_Connected(NetworkingPlayer player)
        {
            yield return new WaitForSeconds(0.5f);

            GameObject playerEntry = (GameObject)GameObject.Instantiate(PlayerEntry); //Instantiate The prefab 
            playerEntry.transform.SetParent(PlayerList, false);

            string[] tmp = player.Ip.Split('+');
            string playerInfo = player.Name + "\n" + tmp[0]; //Name + IP
            playerEntry.GetComponent<PlayerLobbyPrefab>().Id = player.NetworkId; //Assign Player ID for prefab removal
            playerEntry.GetComponentInChildren<Text>().text = playerInfo;

            Label_PlayerCount.text = "Connected Players : " + (socket.Players.Count + 1) + "/" + Field_PlayerCount.text; //Set UI

            ServerSync.serverData.PlayerCount++;
            UDP_Broadcast.SetMessage(ServerSync.serverData.ToString());


        }

        private void OnPlayer_Disconnected(NetworkingPlayer player)
        {
            if (player.NetworkId == Networking.PrimarySocket.Me.NetworkId) //If Owner
                return;

            GameObject obj = PlayerList.GetComponentsInChildren<PlayerLobbyPrefab>().Single(x => x.Id == player.NetworkId).gameObject;
            GameObject.Destroy(obj);
            Label_PlayerCount.text = "Connected Players : " + (socket.Players.Count + 1) + "/" + Field_PlayerCount.text;

            ServerSync.serverData.PlayerCount--;
            UDP_Broadcast.SetMessage(ServerSync.serverData.ToString());

        }

        private void AddEvents()
        {
            socket.disconnected += Disconnected;
            socket.connected += Connected;
            socket.playerConnected += OnPlayer_Connected;
            socket.playerDisconnected += OnPlayer_Disconnected;
        }
        private void RemoveEvents()
        {
            socket.disconnected -= Disconnected;
            socket.playerConnected -= OnPlayer_Connected;
            socket.playerDisconnected -= OnPlayer_Disconnected;
            socket.connected -= Connected;
        }

        protected void OnApplicationQuit()
        {
            Button_DisconnectHost();
        }
        #endregion

        #region Network

        private void StartGame()
        {
            ServerSync.Init();

            Networking.PrimarySocket.GetNewPlayerUpdates();

            System.Threading.Thread t = new System.Threading.Thread(() =>
           {

               foreach (NetworkingPlayer player in Networking.PrimarySocket.Players)
               {
                   ServerSync.Transfered = false;

                   if (ServerSync.StartServer())
                   {
                       RpcHandler.instance.TCPRequest(player);
                   }
                   else
                   {
                       return;
                   }

                   while (!ServerSync.Transfered)
                       System.Threading.Thread.Sleep(100);


               }
           });

            t.Start();

            while (t.ThreadState == System.Threading.ThreadState.Running)
            {
                //Wait A little bit
            }


            RemoveEvents();
            UDP_Broadcast.StopBroadcast();

            if (socket.Connected)
            {
                LoadLevel();
            }
            else
            {
                socket.connected += LoadLevel;
            }

            //Networking.ChangeClientScene(Networking.PrimarySocket, "PlayMap");
            RpcHandler.instance.UniversalRPC("LoadLevel", NetworkReceivers.OthersBuffered);
        }

        void LoadLevel()
        {
            socket.connected -= LoadLevel;
            //Application.LoadLevel("PlayMap");
            SceneManager.LoadScene("PlayMap");
        }
        #endregion

        #region UI_Event
        private void UI_Host()
        {
            Btn_Create.GetComponentInChildren<Text>().text = "Creating Match...";

            if (string.IsNullOrEmpty(Field_ServerName.text))
                Field_ServerName.text = "Server(" + Random.Range(10000, 99999).ToString() + ")";
        }
        private void UI_Connected()
        {
            Btn_Create.GetComponentInChildren<Text>().text = "Start Game";
            Btn_Create.onClick.RemoveAllListeners();
            Btn_Create.onClick.AddListener(StartGame);

            //Set Interactable
            Btn_Disconnect.interactable = true;
            ToggleGroup.interactable = false;
            HostCanvasGroup.interactable = false;

            //Set Labels
            Label_ServerIP.text = "Server IP : " + NetworkUtility.Ip;
            Label_PlayerCount.text = "Connected Player : 1/" + Field_PlayerCount.text;
            Label_GameType.text = "Game Type : " + ServerSync.serverData.GameType.ToString();
            Label_MapName.text = "Current Map : " + ServerSync.serverData.Map_Name;
        }
        private void UI_Disconnected()
        {
            //Host Create Button
            Btn_Create.GetComponentInChildren<Text>().text = "Create Server";
            Btn_Create.onClick.RemoveAllListeners();
            Btn_Create.onClick.AddListener(Button_HostCreate);

            //Set Interactable
            Btn_Disconnect.interactable = false;
            ToggleGroup.interactable = true;
            HostCanvasGroup.interactable = true;

            foreach (Transform i in PlayerList)
                GameObject.Destroy(i.gameObject);

            //Set Labels
            Label_ServerIP.text = "Server IP : None";
            Label_PlayerCount.text = "Connected Players : None";
            Label_GameType.text = "Game Type : None";
            Label_MapName.text = "Current Map : None";
        }
        #endregion
    }
}