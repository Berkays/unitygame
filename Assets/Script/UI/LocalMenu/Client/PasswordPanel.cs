﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.LocalPlayMenu
{

    public class PasswordPanel : MonoBehaviour
    {
        public static PasswordPanel instance;
        CanvasGroup cGroup;

        [HideInInspector]
        public string Pass = string.Empty;


        void Awake()
        {
            instance = this;
            cGroup = this.GetComponent<CanvasGroup>();
        }
        public void Close()
        {
            //this.gameObject.SetActive(false);
            cGroup.alpha = 0;
            cGroup.blocksRaycasts = false;
        }
        public void Open()
        {
            //this.gameObject.SetActive(true);
            cGroup.alpha = 1;
            cGroup.blocksRaycasts = true;
        }

        public void SetPassword(InputField field)
        {
            Pass = field.text;
        }
    }
}