﻿using UnityEngine;
using UnityEngine.UI;

public class HostEntry : MonoBehaviour
{
    public Text lbl_Name, lbl_Ip, lbl_Players, lbl_MapName, lbl_Password;
    public Button JoinButton;

    public bool   isPrivate { get; private set; }
    public string Ip        { get; private set; }
    public string Password  { get; private set; }

    public void SetValues(string[] arr,System.Action<HostEntry> pressEvent)
    {
        lbl_Name.text = arr[0].Trim(); //Set Name
        Ip = lbl_Ip.text = arr[1].Trim(); //Set IP
        lbl_Players.text = arr[2].Trim(); //Set Players
        lbl_MapName.text = arr[3].Trim(); //Set Map(Comment)
        Password = arr[4].Trim(); //Set Password
        
        isPrivate = !string.IsNullOrEmpty(Password);

        lbl_Password.text = isPrivate ? "    Yes" : "    No";

        JoinButton.onClick.RemoveAllListeners();
        JoinButton.onClick.AddListener(() => { pressEvent(this); });
    }
}
