﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace GameUI.LocalPlayMenu
{
    public class ConnectPanel : MonoBehaviour
    {

        public static ConnectPanel instance;

        CanvasGroup cGroup;
        public Text Status;

        // Use this for initialization
        void Awake()
        {
            instance = this;
            cGroup = this.GetComponent<CanvasGroup>();
        }
        
        //Wait for Seconds
        private IEnumerator IClosePanel(int seconds)
        {
            yield return new WaitForSeconds(seconds);
            ClosePanel();
        }
        //Starts Coroutine
        public void ClosePanel(int seconds)
        {
            StartCoroutine("IClosePanel", seconds);
        }

        //Closes Panel
        public void ClosePanel()
        {
            cGroup.alpha = 0;
            cGroup.blocksRaycasts = false;

            GameObject.Find("LocalMenu").GetComponent<CanvasGroup>().interactable = true;

            //Disconnect
            GameObject.Find("ClientPanel").GetComponent<ClientPanel>().Disconnect();
        }
        //Opens Panel
        void OpenPanel()
        {
            cGroup.alpha = 1;
            cGroup.blocksRaycasts = true;

            GameObject.Find("LocalMenu").GetComponent<CanvasGroup>().interactable = false;
        }

        //Set Text and open panel if closed
        public void SetStatus(string text)
        {
            bool isActive = cGroup.alpha == 1;

            if (isActive == false)
                OpenPanel();

            Status.text = text;
        }
        //Overloaded method with time
        public void SetStatus(string text,int Time)
        {
            bool isActive = cGroup.alpha == 1;

            if (isActive == false)
                OpenPanel();

            Status.text = text;

            ClosePanel(Time);
        }


    }
}