﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RadialRotater : MonoBehaviour
{
    public bool ClockWise;
    public bool AutoFill;
    public float RotateSpeed;
    public float TimeToFill;
    private Image Radial;

    public System.Action OnFill;

    void Awake()
    {

        Radial = this.GetComponent<Image>();
        if (ClockWise == true)
            RotateSpeed *= -1;
        if (AutoFill == true)
        {
            OnFill += delegate () { StartCoroutine("RadialFill"); };
            Fill();
        }
    }
	void Update ()
    {
        Radial.transform.Rotate(0, 0, RotateSpeed * Time.deltaTime, Space.Self);
	}

    public void Fill()
    {
        StopFill();
        StartCoroutine("RadialFill");
    }
    public void StopFill()
    {
        StopCoroutine("RadialFill");
        CancelInvoke("Fill");
        Radial.fillAmount = 0;
    }
    IEnumerator RadialFill()
    {
        float rate = 1/TimeToFill;
        float i = 0;
        Radial.fillAmount = 0;
        while (i < 1)
        {
            //if (this.gameObject.activeSelf == false || Refreshing == false) //Dont Refresh in HostScreen
            //    yield break;
            i += Time.deltaTime * rate;
            Radial.fillAmount = i;

            yield return null;
        }

        if (OnFill != null)
            OnFill();
    }
}
