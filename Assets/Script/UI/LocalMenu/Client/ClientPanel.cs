﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using BeardedManStudios.Network;
using BeardedManStudios.Network.Unity;

using Game.NetworkUtility; //UDP Receive

namespace GameUI.LocalPlayMenu
{
    public class ClientPanel : MonoBehaviour
    {
        public Transform ListOfServers;
        public GameObject ServerPrefab;

        public HostEntry SelectedServer;

        public Text StatusText;
        public Text RefreshButton;
        public InputField IpInput;
        public RadialRotater Radial;

        bool _refreshing = false;
        NetWorker _socket = null;


        #region REFRESH

        void Clearlist()
        {
            foreach (Transform t in ListOfServers)
                Destroy(t.gameObject);
        }
        void RefreshList()
        {
            Clearlist();

            int serverCount = 0;

            System.Action<HostEntry> onPressed = (x) =>
            {
                Refresh_Off();
                SelectedServer = x;
            };

            foreach (string host in UDP_Receive.ServerList)
            {
                GameObject entry = Instantiate(ServerPrefab) as GameObject;
                entry.transform.SetParent(ListOfServers,false);

                var info = host.Split(',');
                entry.GetComponent<HostEntry>().SetValues(info, onPressed);
                serverCount++;
            }

            StatusText.text = "Found " + serverCount + " Servers";
            Radial.Fill();
        }

        public void ChangeAutoRefresh()
        {
            if (_refreshing == true)
            {
                //Stop Refreshing When Clicked
                Refresh_Off();
            }
            else
            {
                //Start Refreshing
                Refresh_On();
            }
        }
        public void Refresh_Off() //Stop Refreshing When Clicked
        {
            if (_refreshing == false)
                return;

            _refreshing = false;

            //UI
            RefreshButton.text = "Start Searching";
            Radial.OnFill -= RefreshList;
            Radial.StopFill();

            UDP_Receive.StopReceive();
        }
        public void Refresh_On() //Start Refreshing
        {
            if (_refreshing == true)
                return;

            _refreshing = true;

            //UI
            RefreshButton.text = "Cancel Searching";
            Clearlist();

            UDP_Receive.StartReceive();
            Radial.OnFill += RefreshList;
            Radial.Fill();
        }
        #endregion

        #region Connection / Disconnection
        public void ConnectInit() //UDP Connection
        {
            if (SelectedServer == null)
                return;

            bool PasswordProtected = SelectedServer.isPrivate;

            if (PasswordProtected == true)
            {
                //If Password Protected
                //Show Password Panel
                //TODO Disable Connect Button
                PasswordPanel.instance.Open();
            }
            else
            {
                Connect();
            }

        }
        public void ConnectWithPassword()
        {
            if (Hashing.Hash.hashCheck(PasswordPanel.instance.Pass, SelectedServer.Password))
            {
                Connect();
            }
            else
            {
                //Password Failed
                ConnectPanel.instance.SetStatus("CONNECTING..." + "\n" + "CONNECTION FAILED" + "\n" + "Wrong Password");
                ConnectPanel.instance.ClosePanel(5);
            }

        }
        public void Connect()
        {
            Join(SelectedServer.Ip, 7777);

            SelectedServer = null;
        }
        public void ConnectInitIP() //Direct IP Connection
        {
            Join(IpInput.text, 7777);
        }
        public void Disconnect()
        {
            try
            {
                _socket.Disconnect();
            }
            catch { }

            UDP_Receive.StopReceive();
            Networking.NetworkingReset();
            SelectedServer = null;
        }

        private void Join(string Ip, ushort port)
        {
            _socket = Networking.Connect(Ip, port, Networking.TransportationProtocolType.UDP, false, false);

            ServerTimeout(); //Add Server Timeout Event

            _socket.serverDisconnected += OnServerDisconnected;

            if (_socket.Connected)
                MainThreadManager.Run(OnConnected);
            else
                _socket.connected += OnConnected;

        }
        #endregion

        #region Events
        void OnConnected()
        {
            Networking.SetPrimarySocket(_socket);
            NetworkingManager.Instance.SetName(PlayerPrefs.GetString("PlayerName")); //Set Player Name
            OnConnected_UI();
        }
        void OnConnected_UI()
        {
            ConnectPanel.instance.SetStatus("CONNECTING..." + "\n" + "CONNECTION SUCCESSFUL" + "\n" + "WAITING FOR SERVER TO START");
        }

        void ServerTimeout()
        {
            NetWorker.BasicEvent timeoutEvent = delegate ()
            {
                ConnectPanel.instance.SetStatus("CONNECTING..." + "\n" + "CONNECTION FAILED" + "\n" + "Server Timeout");
                ConnectPanel.instance.ClosePanel(5);
                Networking.Disconnect();
            };

            if (!_socket.Connected)
            {
                ConnectPanel.instance.SetStatus("CONNECTING...");
                _socket.ConnectTimeout = 5000;
                _socket.connectTimeout += timeoutEvent;
            }
        }

        public void OnServerDisconnected(string reason)
        {
            ConnectPanel.instance.SetStatus("CONNECTING..." + "\n" + "CONNECTION FAILED" + "\n" + "Reason : " + reason);
            ConnectPanel.instance.ClosePanel(5);
            Disconnect();
        }

        void OnApplicationQuit()
        {
            Disconnect();
        }
        #endregion

    }
}