﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;

namespace Game.NetworkUtility
{
    public class UDP_Receive : ScriptableObject
    {
        private static UdpClient client;
        private static bool Canceled;

        private static string[] serverList;
        public static string[] ServerList
        {
            get
            {
                if (serverList == null)
                    serverList = new string[] { };

                return serverList;
            }

            set
            {
                serverList = value;
            }
        } //Property

        public static event System.Action OnReceivedEvent;
        private static void OnReceived()
        {
            if (OnReceivedEvent != null)
                OnReceivedEvent();
        }


        public static void StartReceive()
        {
            Canceled = false;

            if (client == null)
                client = new UdpClient(7778);

            client.BeginReceive(new System.AsyncCallback(recv), null);

        }
        public static void StopReceive()
        {
            Canceled = true;  
        }
        private static void recv(System.IAsyncResult res)
        {
            ServerList = new string[] { };

            if (Canceled == true)
            {
                client.Client.Shutdown(SocketShutdown.Both);
                client.Client.Close();
                return;
            }

            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 7778);

            byte[] received = client.EndReceive(res, ref RemoteIpEndPoint);

            string data = System.Text.Encoding.ASCII.GetString(received);

            ServerList = data.Split('\n');

            //OnReceived();

            client.BeginReceive(new System.AsyncCallback(recv), null);

        }

    }
}
