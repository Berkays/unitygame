﻿using UnityEngine;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;

namespace Game.NetworkUtility
{
    public class UDP_Broadcast : ScriptableObject
    {
        private const int SleepTime = 5000;
        private static Thread thread;
        private static bool Canceled = false;

        //private static string[] MessageGroup;
        private static bool MessageChanged = false;
        private static string _message = string.Empty;
        private static string Message
        {
            get
            { return _message; }
            set
            {
                if (!string.IsNullOrEmpty(_message) && thread != null && thread.ThreadState != ThreadState.Unstarted)
                    MessageChanged = true;

                _message = value;
            }
        }

        public static void StartBroadcast()
        {
            Canceled = false;

            thread = new Thread(SendBroadcast);
            thread.Start();

        }
        public static void StopBroadcast()
        {
            //if (thread != null)
            //{
            //    thread.Abort();
            //}
            Canceled = true;
        }
        private static void SendBroadcast()
        {
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Broadcast, 7778);
            UdpClient server = new UdpClient();
            server.Connect(endpoint);

            byte[] arr = System.Text.Encoding.ASCII.GetBytes(Message); //Message Encoded To Bytes

            while (!Canceled)
            {
                server.Send(arr, arr.Length); //Send Message
                Thread.Sleep(SleepTime); //Wait

                Debug.Log("UDP Data : " + System.Text.Encoding.ASCII.GetString(arr));

                if (MessageChanged)
                {
                    arr = System.Text.Encoding.ASCII.GetBytes(Message);
                    MessageChanged = false;
                }

            }

            server.Client.Shutdown(SocketShutdown.Both);
            server.Client.Close();
        }

        private static void AsyncBroadcast(System.IAsyncResult ar)
        {
            UdpClient server = (UdpClient)ar.AsyncState;

            byte[] arr = System.Text.Encoding.ASCII.GetBytes(Message); //Message Encoded To Bytes

            Debug.Log("UDP Data : " + System.Text.Encoding.ASCII.GetString(arr));

            server.EndSend(ar);

            if (Canceled)
            {
                server.Client.Shutdown(SocketShutdown.Both);
                server.Client.Close();
                return;
            }
            server.BeginSend(arr, arr.Length, new System.AsyncCallback(AsyncBroadcast), server);

        }

        //public static void SetMessage(string[] Data)
        //{

        //    System.Text.StringBuilder builder = new System.Text.StringBuilder(Data.Length);
        //    builder.Append(Data[0]);
        //    builder.Append(',');
        //    builder.Append(ServerSync.serverData.Server_Ip);
        //    builder.Append(',');
        //    builder.Append(ServerSync.serverData.PlayerCount + "/" + ServerSync.serverData.MaxPlayerCount);
        //    builder.Append(',');
        //    builder.Append(ServerSync.serverData.Map_Name);
        //    builder.Append(',');

        //    if (ServerSync.serverData.Password != null)
        //        builder.Append(System.Convert.ToBase64String(ServerSync.serverData.Password)); //Convert byte array to string

        //    Message = builder.ToString();
        //}
        //public static void SetMessage(string Server_Name, string IP_Address, string Player_Count, string Map_Name, byte[] Hash)
        //{
        //    if (MessageGroup == null)
        //        MessageGroup = new string[5];

        //    MessageGroup[(int)MessagePart.Server_Name] = Server_Name;
        //    MessageGroup[(int)MessagePart.IP_Address] = IP_Address;
        //    MessageGroup[(int)MessagePart.Player_Count] = Player_Count;
        //    MessageGroup[(int)MessagePart.Map_Name] = Map_Name;

        //    if (Hash != null)
        //    {
        //        MessageGroup[(int)MessagePart.Hash] = System.Convert.ToBase64String(Hash);
        //    }

        //    MessageChanged = true;
        //}
        public static void SetMessage(string Data)
        {
            Message = Data;
        }
        //public static void ModifyMessage(MessagePart _m, string data, byte[] Hash = null)
        //{   
        //    if (MessageGroup == null)
        //        MessageGroup = new string[5];

        //    if (_m == MessagePart.Hash)
        //    {
        //        if (Hash != null)
        //        {
        //            MessageGroup[(int)MessagePart.Hash] = System.Convert.ToBase64String(Hash);
        //            MessageChanged = true;
        //        }
        //    }
        //    else
        //    {
        //        MessageGroup[(int)_m] = data;
        //        MessageChanged = true;
        //    }

        //}

        //public enum MessagePart : int
        //{
        //    Server_Name = 0,
        //    IP_Address,
        //    Player_Count,
        //    Map_Name,
        //    Hash
        //}


    }
}