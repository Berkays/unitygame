﻿using UnityEngine;
using System.IO;
using System.Linq;
using ProtoBuf;
using LevelSerializerProtocol;
using ServerDataSerialization;

public class ServerSync : ScriptableObject
{
    public static ServerData serverData = new ServerData();
    private static byte[] FileData;

    private static bool transfered = false;
    public static bool Transfered
    {
        get
        {
            return transfered;
        }

        set
        {
            Debug.Log("Transfer Completed");
            transfered = value;
        }
    }

    public static void Init()
    {
        string path = LevelSerializer.ReturnPath(serverData.Map_Name); //Pass Selected Map Name

        string MapName = LevelDeserializer.DeserializeMeta(serverData.Map_Name).Name;

        serverData.Map_Name = MapName;
        serverData.Map_Data = File.ReadAllBytes(path);

        //TODO MAP INTEGRITY CHECK
        MemoryStream mem = new MemoryStream(); //Used for Byte Converting
        ProtoBuf.Serializer.SerializeWithLengthPrefix<ServerData>(mem, serverData, PrefixStyle.Fixed32); //Compress And Send

        FileData = mem.ToArray();
    }

    public static bool StartServer()
    {
        if (FileData == null)
        {
            return false;
        }
        else
        {
            NetworkUtility.TCP.TCP_Send(FileData);
            return true;
        }
    }

    public static void Receive(byte[] ServerData)
    {
        MemoryStream mem = new MemoryStream(ServerData); //Receive Byte Data

        ServerData tempdata = ProtoBuf.Serializer.DeserializeWithLengthPrefix<ServerData>(mem, PrefixStyle.Fixed32); //Deserialize Data

        string path = LevelSerializer.ReturnPath(tempdata.Map_Name);

        mem.Close();

        try
        {
            bool Exists = File.ReadAllBytes(path).SequenceEqual(tempdata.Map_Data);

            if (Exists == false)
            {
                // Save
                FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write);
                file.Write(tempdata.Map_Data, 0, tempdata.Map_Data.Length);
                file.Close();
            }
        }
        catch (FileNotFoundException)
        {
            // Save
            FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write);
            file.Write(tempdata.Map_Data, 0, tempdata.Map_Data.Length);
            file.Close();
        }

        serverData = tempdata; //Set Global Variable

        Debug.Log("Map Data Received : " + serverData.Map_Name);
    }

}
