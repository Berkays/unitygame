﻿using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    float deltatime = 0.0f;
    Text gui;
	// Use this for initialization
	void Start ()
    {
        gui = this.GetComponent<Text>();
        InvokeRepeating("UpdateText", 0.5f, 0.4f);       
	}
	
	// Update is called once per frame
	void Update ()
    {
        deltatime += (Time.deltaTime - deltatime) * 0.1f;

        
    }
    void UpdateText()
    {
        int fps = (int)(1 / deltatime);
        gui.text = "FPS : " + fps;
    }
}
