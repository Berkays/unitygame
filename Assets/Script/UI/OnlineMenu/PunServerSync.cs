﻿using UnityEngine;
using LevelSerializerProtocol;

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GameUI.Online
{
    public class PunServerSync : ScriptableObject
    {
        private static byte[] serialized;
        public static byte[] Serialized
        {
            get
            {
                if (serialized == null)
                {
                    serialized = LevelSerializer.Read(selectedMaps.Peek());
                }
                return serialized;
            }

            set
            {
                serialized = value;
            }
        }

        public static Queue<string> selectedMaps = new Queue<string>();

        public static PunGameSettings.GameType gameType { get; set; }

        public static int gameScore { get; set; }


    }
}