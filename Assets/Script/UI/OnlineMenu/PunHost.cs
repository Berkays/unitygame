﻿using UnityEngine;
using UnityEngine.UI;
using Photon;
using System.Collections;

namespace GameUI.Online
{
    public class PunHost : PunBehaviour
    {
        public OnlinePanel OnlineMenu;

        #region UI
        public Button createButton, disconnectButton;
        public Text statusLabel;
        //Room Info
        public Text playerCount, selectedMap, photonPing, gameType, gameScore;

        //Room Settings
        public Text playerCountField;
        public InputField nameField, passwordField;
        public Dropdown hostTypeDropdown;

        #endregion

        bool createPressed = false;

        void Start()
        {
            PhotonNetwork.automaticallySyncScene = true;

            //Leave room when switching to client mode
            OnlineMenu.changeToClient += leaveRoom;

            //Button events
            createButton.onClick.AddListener(createRoom);
            disconnectButton.onClick.AddListener(leaveRoom);
        }

        RoomOptions roomSetup()
        {
            //Get MD5 hash of the password string
            string hash = Hashing.Hash.hashToText(passwordField.text.Trim());

            RoomOptions options = new RoomOptions()
            {
                maxPlayers = byte.Parse(playerCountField.text),

                //Can join the room?
                isOpen = true,
                //Is the room visible?
                isVisible = hostTypeDropdown.value == 0 ? true : false,

                //Send password MD5 hash and Map Name
                customRoomProperties = new ExitGames.Client.Photon.Hashtable
                {
                    { "Password",hash },
                    { "Map",PunServerSync.selectedMaps.Peek()},
                    { "Ping",PhotonNetwork.GetPing() },
                    { "GameType" , (int)PunServerSync.gameType },
                    { "GameScore", PunServerSync.gameScore }

                },
                //Password and Map name is visible before join
                customRoomPropertiesForLobby = new string[]
                {
                    "Password",
                    "Map",
                    "Ping"
                }
            };

            return options;

        }

        /// <summary>
        /// UI REFERENCE
        /// Host/Buttons/ServerCreateButton
        /// </summary>
        public void createRoom()
        {

            bool isDisconnected = !isConnected();
            if (isDisconnected)
            {
                UI_punConnection();

                createPressed = true;

                PhotonNetwork.ConnectUsingSettings("0.1");
            }
            else if (!PhotonNetwork.inRoom && PhotonNetwork.insideLobby)
            {
                UI_roomCreate();


                PhotonNetwork.CreateRoom(nameField.text, roomSetup(), TypedLobby.Default);
            }
        }
        /// <summary>
        /// UI REFERENCE
        /// Host/Buttons/ServerDisconnectButton
        /// </summary>
        public void leaveRoom()
        {
            if (PhotonNetwork.inRoom)
                PhotonNetwork.LeaveRoom();
        }

        void startGame()
        {
            PhotonNetwork.LoadLevel("PunPlayMap");
        }


        public override void OnJoinedLobby()
        {
            statusLabel.text = "Status : Connected to photon service";

            PunConnectPanel.instance.ClosePanel();

            if (createPressed)
            {
                createPressed = false;
                createRoom();
            }

        }
        public override void OnLeftLobby()
        {
            statusLabel.text = "Status : Disconnected from photon service";
        }

        public override void OnCreatedRoom()
        {
            createButton.onClick.RemoveAllListeners();
            createButton.onClick.AddListener(startGame);

            UI_roomCreated();
        }
        public override void OnLeftRoom()
        {
            createButton.onClick.RemoveAllListeners();
            createButton.onClick.AddListener(createRoom);

            UI_roomDiconnected();
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            playerCount.text = "Player Count : " + PhotonNetwork.room.playerCount + "/" + PhotonNetwork.room.maxPlayers;
        }
        public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
        {
            playerCount.text = "Player Count : " + PhotonNetwork.room.playerCount + "/" + PhotonNetwork.room.maxPlayers;
        }

        void UI_punConnection()
        {
            PunConnectPanel.instance.SetStatus("CONNECTING...");
        }
        void UI_roomCreate()
        {
            if (string.IsNullOrEmpty(nameField.text))
            {
                nameField.text = "Server(" + Random.Range(10000, 99999).ToString() + ")";

            }
            createButton.Text("Creating room...");
        }
        void UI_roomCreated()
        {
            //UI
            statusLabel.text = "Status : Created a public room";

            updateUiInfo();

            InvokeRepeating("Ping", 0.01f, 10);
            createButton.Text("Start Game");

            disconnectButton.interactable = true;

        }
        void UI_roomDiconnected()
        {
            statusLabel.text = "Status : Stopped hosting";
            playerCount.text = "Player Count : None";
            selectedMap.text = "Map : None";
            gameType.text = "Game Type : None";
            gameScore.text = "Game Score : None";
            photonPing.text = "Server Ping : None";
            createButton.Text("Host Game");

            disconnectButton.interactable = false;

            CancelInvoke("Ping");
        }

        void updateUiInfo()
        {
            playerCount.text = "Player Count : 1/" + PhotonNetwork.room.maxPlayers;
            selectedMap.text = "Map : " + PunServerSync.selectedMaps.Peek();
            gameType.text = "Game Type : " + PunServerSync.gameType.ToString();
            string scoreStr = PunServerSync.gameType == PunGameSettings.GameType.Timed ? " Minutes" : " Kiils";
            gameScore.text = "Game Score : " + PunServerSync.gameScore + scoreStr;
        }

        /// <summary>
        /// UNITY REF
        /// Pun Game Settings /Close Button
        /// </summary>
        public void updateGameInfo()
        {
            if (!inRoom())
                return;

            updateUiInfo();
            ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable()
            {
                { "Map",PunServerSync.selectedMaps.Peek()},
                { "GameType" , (int)PunServerSync.gameType },
                { "GameScore", PunServerSync.gameScore }
            };

            PhotonNetwork.room.SetCustomProperties(properties);
        }
        /// <summary>
        /// UNITY REF
        /// Host Panel / Increase Counter / Decrease Counter Buttons
        /// </summary>
        public void updatePlayerCount()
        {

            if (!inRoom())
                return;

            playerCount.text = "Player Count : 1/" + playerCountField.text;
            PhotonNetwork.room.maxPlayers = int.Parse(playerCountField.text);
        }


        void Ping()
        {
            int pingTime = PhotonNetwork.GetPing();
            photonPing.text = "Server Ping : " + pingTime.ToString() + " ms";

            if (PhotonNetwork.inRoom)
            {
                ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
                properties.Add("Ping", pingTime);

                PhotonNetwork.room.SetCustomProperties(properties);
            }

        }



        public static bool isConnected()
        {
            bool isConnected = (PhotonNetwork.connectionState == ConnectionState.Connecting || PhotonNetwork.connectionState == ConnectionState.Connected);
            return isConnected;
        }
        public static bool inRoom()
        {
            if (isConnected() == false)
                return false;

            return PhotonNetwork.inRoom;
        }

    }
}