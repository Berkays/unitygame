﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameUI.Online
{
    public class PunConnectPanel : MonoBehaviour
    {

        public static PunConnectPanel instance;

        public System.Action OnClose;

        CanvasGroup cGroup;
        RadialRotater radial;
        public Text Status;

        // Use this for initialization
        void Awake()
        {
            instance = this;
            cGroup = this.GetComponent<CanvasGroup>();
            radial = this.GetComponentInChildren<RadialRotater>();
        }

        //Wait for Seconds
        private IEnumerator IClosePanel(int seconds)
        {
            yield return new WaitForSeconds(seconds);
            ClosePanel();
        }
        //Starts Coroutine
        public void ClosePanel(int seconds)
        {
            StartCoroutine("IClosePanel", seconds);
        }

        //Closes Panel
        public void ClosePanel()
        {
            cGroup.alpha = 0;
            cGroup.blocksRaycasts = false;

            //Action on Close
            if (OnClose != null)
                OnClose();
        }
        //Opens Panel
        void OpenPanel()
        {
            cGroup.alpha = 1;
            cGroup.blocksRaycasts = true;

            radial.Fill();
        }

        //Set Text and open panel if closed
        public void SetStatus(string text)
        {
            bool isActive = cGroup.alpha == 1;

            if (isActive == false)
                OpenPanel();

            Status.text = text;
        }
        //Overloaded method with time
        public void SetStatus(string text, int Time)
        {
            bool isActive = cGroup.alpha == 1;

            if (isActive == false)
                OpenPanel();

            Status.text = text;

            ClosePanel(Time);
        }


    }
}
