﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.Online
{
    public class OnlinePanel : MonoBehaviour
    {
        bool isHost = true;

        public CanvasGroup HostCanvas, ClientCanvas;
        public Toggle hostToggle, clientToggle;

        public System.Action changeToClient;
        public System.Action changeToHost;

        public void canvasChange(bool onHost)
        {
            if (isHost == true && onHost == false)
            {
                //Change to client
                HostCanvas.alpha = 0;
                hostToggle.interactable = true;
                HostCanvas.blocksRaycasts = false;

                ClientCanvas.blocksRaycasts = true;
                clientToggle.interactable = false;
                ClientCanvas.alpha = 1;

                if (changeToClient != null)
                {
                    changeToClient();
                }

                isHost = false;

                return;
            }
            if (isHost == false && onHost == true)
            {
                //Change to Host

                HostCanvas.alpha = 1;
                hostToggle.interactable = false;
                HostCanvas.blocksRaycasts = true;

                ClientCanvas.blocksRaycasts = false;
                clientToggle.interactable = true;
                ClientCanvas.alpha = 0;

                if (changeToHost != null)
                {
                    changeToHost();
                }

                isHost = true;

                return;
            }

        }

        public void closePanel()
        {
            if (isHost)
                HostCanvas.gameObject.GetComponent<PunHost>().leaveRoom();
            else
                ClientCanvas.gameObject.GetComponent<PunClient>().leaveRoom();

            bool isConnected = (PhotonNetwork.connectionState == ConnectionState.Connecting || PhotonNetwork.connectionState == ConnectionState.Connected);

            if (isConnected)
                PhotonNetwork.Disconnect();

        }

    }
}