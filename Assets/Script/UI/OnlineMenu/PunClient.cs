﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Photon;
using System;
using System.Collections.Generic;

namespace GameUI.Online
{

    public class PunClient : PunBehaviour
    {
        //Host Entry Objects
        public Transform ListOfServers;
        public GameObject ServerPrefab;

        //Selected host component
        public PunHostEntry SelectedServer;

        //UI Elements
        public Text StatusText;
        public RadialRotater radial;


        void Start()
        {
            var Component = GameObject.Find("OnlineMenu").GetComponent<OnlinePanel>();

            Component.changeToHost += () =>
            {
                Refresh_Off();
            };

            onPressed = (x) =>
            {
                Refresh_Off();
                SelectedServer = x;
            };

        }

        #region Refresh

        public Button refreshButton;

        private bool refreshing = false;
        private Action<PunHostEntry> onPressed;

        private Filter filter = Filter.None;
        private Sort sort = Sort.None;

        void RefreshList()
        {
            //Retrieve new room list
            var rooms = PhotonNetwork.GetRoomList();

            //filter servers based on Filter Enum
            filterServers(ref rooms, filter);
            //Sort rooms based on Sort Enum
            sortRooms(ref rooms, sort);

            //add or remove prefabs
            preparePrefabs(rooms.Length);

            //Set room info to prefabs
            for (int i = 0; i < rooms.Length; i++)
                ListOfServers.GetChild(i).GetComponent<PunHostEntry>().SetInfo(rooms[i], onPressed);

            StatusText.text = "Found " + rooms.Length + " Servers";

            radial.Fill();
        }

        [Flags]
        enum Filter
        {
            None = 1,
            Empty = 2,
            Open = 4,
            Ping = 8
        }
        enum Sort
        {
            None,
            Ping,
            FillRatio,
            Password
        }

        public void sortAction(Dropdown d)
        {
            sort = (Sort)d.value;
        }
        public void filterAction(Dropdown d)
        {
            filter = (Filter)(Math.Pow(2,d.value));
        }

        void filterServers(ref RoomInfo[] rooms, Filter hostFilter)
        {
            if((hostFilter & Filter.None) == Filter.None)
                return;

            IEnumerable<RoomInfo> result = rooms;

            if ((hostFilter & Filter.Empty) == Filter.Empty)
            {
                //Show only empty rooms
                result = result.Where(x => x.playerCount != x.maxPlayers);
            }
            if ((hostFilter & Filter.Open) == Filter.Open)
            {
                result = result.Where(x => string.IsNullOrEmpty(x.customProperties["Password"].ToString()));
            }
            if ((hostFilter & Filter.Ping) == Filter.Ping)
            {
                //Show rooms that have ping less than 200
                result = result.Where(x => (int)x.customProperties["Ping"] < 100);
            }

            rooms = result.ToArray();
        }
        void sortRooms(ref RoomInfo[] rooms, Sort sortCriteria)
        {
            if (sortCriteria == Sort.None)
                return;

            IEnumerable<RoomInfo> result = rooms;

            if (sortCriteria == Sort.FillRatio)
            {
                result = rooms.OrderByDescending(x => (float)(x.playerCount / x.maxPlayers));
            }
            else if (sortCriteria == Sort.Ping)
            {
                result = rooms.OrderBy(x => (int)x.customProperties["Ping"]);
            }
            else if (sortCriteria == Sort.Password)
            {
                result = rooms.OrderBy(x => string.IsNullOrEmpty(x.customProperties["Password"].ToString()));
            }
            else
            {
                result = rooms.OrderBy(x => x.visible);
            }

            rooms = result.ToArray();
        }

        void preparePrefabs(int roomCount)
        {
            int prefabCount = ListOfServers.childCount;

            if (prefabCount > roomCount)
            {
                //Remove extra prefabs

                for (int i = 0; i < prefabCount - roomCount; i++)
                {
                    Destroy(ListOfServers.GetChild(i).gameObject);
                }

            }
            else if (prefabCount < roomCount)
            {
                //Create new prefabs

                for (int i = 0; i < roomCount - prefabCount; i++)
                {
                    GameObject entry = Instantiate(ServerPrefab) as GameObject;
                    entry.transform.SetParent(ListOfServers, false);
                }
            }

        }

        public void ChangeAutoRefresh()
        {
            if (refreshing == true)
            {
                //Stop Refreshing When Clicked
                Refresh_Off();
            }
            else
            {
                //Start Refreshing
                Refresh_On();
            }
        }
        public void Refresh_Off() //Stop Refreshing When Clicked
        {
            if (refreshing == false)
                return;

            refreshing = false;

            //UI
            refreshButton.Text("Start Searching");
            radial.OnFill -= RefreshList;
            radial.StopFill();

        }
        public void Refresh_On() //Start Refreshing
        {
            if (refreshing == true)
                return;

            if (!(PhotonNetwork.connectionState == ConnectionState.Connecting || PhotonNetwork.connectionState == ConnectionState.Connected))
                PhotonNetwork.ConnectUsingSettings("0.1");

            refreshing = true;

            //UI
            refreshButton.Text("Cancel Searching");

            radial.OnFill += RefreshList;
            radial.Fill();
        }

        

        #endregion

        #region Connection

        public void joinRoom()
        {
            if (SelectedServer == null)
                return;

            //TODO Check Password

            Join();
        }
        public void leaveRoom()
        {
            if (PhotonNetwork.inRoom)
                PhotonNetwork.LeaveRoom();

            SelectedServer = null;
        }

        void Join()
        {
            UI_roomJoining();
            PhotonNetwork.JoinRoom(SelectedServer.roomName);
        }

        public override void OnJoinedRoom()
        {
            //Only work for client
            if (!PhotonNetwork.isMasterClient)
                UI_roomConnected();
        }
        public override void OnLeftRoom()
        {
            //Only work for client
            if (!PhotonNetwork.isMasterClient)
                UI_roomDisconnected();
        }

        public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
        {
            UI_roomFailed(codeAndMsg);
        }


        #endregion

        #region UI
        void UI_roomJoining()
        {
            PunConnectPanel.instance.SetStatus("CONNECTING...");
        }
        void UI_roomFailed(object[] codeAndMsg)
        {
            PunConnectPanel.instance.SetStatus("CONNECTION FAILED" + "\n",5);
        }
        void UI_roomConnected()
        {
            PunConnectPanel.instance.SetStatus("CONNECTION SUCCESSFUL" + "\n" + "WAITING FOR SERVER TO START");
            PunConnectPanel.instance.OnClose = leaveRoom;
        }
        void UI_roomDisconnected()
        {
            PunConnectPanel.instance.ClosePanel();
            PunConnectPanel.instance.OnClose -= leaveRoom;
        }

        #endregion


    }
}