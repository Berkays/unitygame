﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

namespace GameUI.Online
{
    public class PunGameSettings : MonoBehaviour
    {
        void Start()
        {
            //Load Maps
            loadMaps();

            //set initial values
            initSettings();

        }

        #region Map

        //Public Variables
        public GameObject mapEntry;
        public Transform availableMapContainer, selectedMapContainer;


        List<string> availableMaps = new List<string>(16);
        List<string> selectedMaps = new List<string>();

        void loadMaps()
        {

            string path = Application.dataPath + @"\Maps\";

            availableMaps.Clear();
            selectedMaps.Clear();

            for (int i = 0; i < availableMaps.Count; i++)
                Destroy(availableMapContainer.GetChild(i).gameObject);
            for (int i = 0; i < selectedMaps.Count; i++)
                Destroy(selectedMapContainer.GetChild(i).gameObject);


            if (Directory.Exists(path) == false)
                return;

            foreach (string file in Directory.GetFiles(path))
            {
                if (file.EndsWith(".map"))
                {
                    //Retrieve FileInfo
                    FileInfo info = new FileInfo(file);

                    //Get file name without extension
                    string map = info.Name.Substring(0, info.Name.Length - 4);

                    //Check if duplicate
                    if (availableMaps.Contains(map) == false)
                    {
                        availableMaps.Add(map);
                        createObject(map);
                    }
                }
            }


            if (availableMaps.Count > 0)
                addToSelected(availableMaps[0]);

            constructQueue();
        }

        public void constructQueue()
        {
            PunServerSync.selectedMaps.Clear();

            foreach (string map in selectedMaps)
            {
                PunServerSync.selectedMaps.Enqueue(map);
            }
        }

        void createObject(string name)
        {
            GameObject obj = Instantiate(mapEntry) as GameObject;

            obj.transform.SetParent(availableMapContainer, false);

            obj.GetComponent<Button>().Text(name);

            obj.GetComponent<Button>().onClick.AddListener(() =>
           {
               if (availableMaps.Contains(name))
               {
                   //Move to Selected
                   addToSelected(name);
               }
               else
               {
                   //Move to Available
                   addToAvailable(name);
               }
           });

        }

        void addToSelected(string mapName)
        {
            //Update UI
            availableMapContainer.GetChild(availableMaps.IndexOf(mapName)).SetParent(selectedMapContainer, false);

            //Update Logic
            selectedMaps.Add(mapName);
            availableMaps.Remove(mapName);

            //Set Selected
            //PunServerSync.selectedMaps.Enqueue(mapName);

        }
        void addToAvailable(string mapName)
        {
            if (!checkNullCondition())
                return;

            //Update UI
            selectedMapContainer.GetChild(selectedMaps.IndexOf(mapName)).SetParent(availableMapContainer, false);

            //Update Logic
            availableMaps.Add(mapName);
            selectedMaps.Remove(mapName);

            //Remove from Selected
            //PunServerSync.selectedMaps.Remove(mapName);
        }



        //Prevent from having no maps as selected
        bool checkNullCondition()
        {
            if (selectedMaps.Count == 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Settings

        public GameObject Timed, Scored;

        void initSettings()
        {
            PunServerSync.gameScore = 5;
            PunServerSync.gameType = GameType.Timed;
        }
        public void OnGameTypeChange(Dropdown dD)
        {
            PunServerSync.gameType = (GameType)dD.value;

            if (PunServerSync.gameType == GameType.Scored)
            {
                Scored.SetActive(true);
                Timed.SetActive(false);

                OnTypeValueChange(Scored.GetComponentInChildren<Dropdown>());
            }
            else if (PunServerSync.gameType == GameType.Timed)
            {
                Scored.SetActive(false);
                Timed.SetActive(true);

                OnTypeValueChange(Timed.GetComponentInChildren<Dropdown>());

            }
        }
        public void OnTypeValueChange(Dropdown dD)
        {
            int value = int.Parse(dD.captionText.text.Split(' ')[0]);
            PunServerSync.gameScore = value;
        }

        public enum GameType : int
        {
            Timed = 0,
            Scored,
            TimedAndScored,
            CaptureTheFlag
        }

        #endregion
    }
}