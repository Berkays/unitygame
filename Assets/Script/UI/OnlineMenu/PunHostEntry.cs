﻿using UnityEngine;
using UnityEngine.UI;

public class PunHostEntry : MonoBehaviour
{

    //public Text lbl_Name, lbl_Players, lbl_MapName, lbl_Password;
    public Text lbl_Name, lbl_Players, lbl_MapName, lbl_Ping;
    public Button JoinButton;

    public string roomName  { get; private set; }
    public string Hash      { get; private set; }
    public bool   isPrivate { get; private set; }

    public void SetInfo(RoomInfo room, System.Action<PunHostEntry> pressEvent)
    {
        //OLD
        //lbl_Name.text = roomName = room.name;
        //lbl_Players.text = room.playerCount + "/" + room.maxPlayers;
        //lbl_MapName.text = room.customProperties["Map"].ToString();

        //Hash = room.customProperties["Password"].ToString();
        //isPrivate = string.IsNullOrEmpty(Hash) ? false : true;
        //lbl_Password.text = isPrivate.ToString();

        //JoinButton.onClick.RemoveAllListeners();
        //JoinButton.onClick.AddListener(() => { pressEvent(this); });

        lbl_Players.text = room.playerCount + "/" + room.maxPlayers;
        lbl_MapName.text = room.customProperties["Map"].ToString();

        Hash = room.customProperties["Password"].ToString();
        isPrivate = string.IsNullOrEmpty(Hash) ? false : true;
        string privateString = isPrivate == true ? "(Private)" : string.Empty;
        roomName = room.name;
        lbl_Name.text = roomName + privateString;

        lbl_Ping.text = room.customProperties["Ping"].ToString() + " ms";

        JoinButton.onClick.RemoveAllListeners();
        JoinButton.onClick.AddListener(() => { pressEvent(this); });
    }
}
