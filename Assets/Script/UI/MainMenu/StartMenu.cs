﻿using UnityEngine;
using UnityEngine.SceneManagement;

using GameUI.SettingsMenu.SoundSettings;
using GameUI.SettingsMenu.VideoSettings;

namespace GameUI.MainMenu
{
    public class StartMenu : MonoBehaviour
    {

        public Transform PanelContainer;
        public CanvasGroup Buttons;
        public GameObject InfoText;

        public SoundSettings SoundComponent;
        public VideoSettings VideoComponent;

        public InputSelect inputSelector;

        private Transform[] Panels;

        int current = -1;

        void Start()
        {
            //Init Settings Scripts
            SoundComponent.Init();
            VideoComponent.Init();

            Panels = new Transform[6];
            for (int i = 0; i < 6; i++)
                Panels[i] = PanelContainer.GetChild(i);
        }


        public void GoBack()
        {
            inputSelector.enabled = true;
            InputSelect.ResetCurrent();

            try
            {
                Animator anim = Panels[current].GetComponent<Animator>();
                anim.Play("Close");
            }
            catch
            { }
            Invoke("EnableMainMenu", 0.3f); //Wait for Animation

        }

        void EnableMainMenu()
        {
            EnableButtons();

            foreach (Transform tf in Panels)
            {
                tf.gameObject.SetActive(false);
            }
        }
        void DisableButtons()
        {
            Buttons.interactable = false;
            InfoText.SetActive(false);
        }
        void EnableButtons()
        {
            Buttons.interactable = true;
            InfoText.SetActive(true);
        }

        public void Open_Panel(int Index)
        {

            if (Index == 6)
            {
                SceneManager.LoadScene("Editor");
                return;
            }
            else
            {
                Panels[Index].gameObject.SetActive(true);
                current = Index;
                inputSelector.enabled = false;
            }

            try
            {
                Animator anim = Panels[current].GetComponent<Animator>();
                anim.Play("Open");
            }
            catch
            { }
            DisableButtons();

        }

    }

}