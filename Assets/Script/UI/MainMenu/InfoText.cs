﻿using UnityEngine;
using UnityEngine.UI;

namespace GameUI.MainMenu
{
    public class InfoText : MonoBehaviour
    {
        Text _Component;

        private const string OnlineText = "Çevrimiçi bir sunucu kurar veya mevcut bir sunucuya girer.";
        private const string LocalText = "Yerel bir sunucu kurar veya mevcut bir yerel sunucuya girer.";
        private const string EditorText = "Bölüm tasarlama aracını çalıştırır.";
        private const string SettingsText = "Oyun ayarlarını görüntülemenizi veya değiştirmenizi sağlar.";
        private const string StoreText = "Çeşitli karakter temalarını mağazadan alabilirsiniz.";
        private const string CreditsText = "Gelişitirici ekibini görüntüler.";
        private const string ExitText = "Oyundan çıkış.";

        void Start()
        {
            _Component = this.GetComponent<Text>();
        }

        public void OnlineInfo()
        {
            _Component.text = OnlineText;
        }
        public void LocalInfo()
        {
            _Component.text = LocalText;
        }
        public void EditorInfo()
        {
            _Component.text = EditorText;
        }
        public void SettingsInfo()
        {
            _Component.text = SettingsText;
        }
        public void StoreInfo()
        {
            _Component.text = StoreText;
        }
        public void Credits()
        {
            _Component.text = CreditsText;
        }
        public void Exit()
        {
            _Component.text = ExitText;
        }
        public void ClearText()
        {
            _Component.text = string.Empty;
        }
    }
}