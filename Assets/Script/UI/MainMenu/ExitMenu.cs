﻿using UnityEngine;

namespace GameUI.MainMenu.ExitMenu
{
    public class ExitMenu : MonoBehaviour
    {
        public void ExitGame()
        {
            GameObject.Find("Fader").GetComponent<Animator>().Play("FadeIn");
            Invoke("FadeOut", 0.7f);
        }

        void FadeOut()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;

#else
            Application.Quit();

#endif
        }
    }
}