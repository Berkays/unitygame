﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PunGameBar : MonoBehaviour
{
    public Text GameInfo;
    int Time = 0;

    public void startCountdown(int t)
    {
        Time = t;

        StartCoroutine("countDown");
    }

    IEnumerator countDown()
    {
        while (Time > 0)
        {

            GameInfo.text = (int)(Time / 60) + ":" + (Time % 60);

            yield return new WaitForSeconds(1);
        }
    }
}
