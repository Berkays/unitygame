﻿using UnityEngine;
using Photon;
using System.Collections;
using PowerUpSystem;

public class PunPowerPick : PunBehaviour
{
    readonly Color infoColor = new Color(0,0.8f,1f);

    PowerUpBase power;

    void Start()
    {
        power = this.GetComponent<PowerUpBase>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag == "Player")
        {

            power.AddComponent(int.Parse(name),PhotonNetwork.player.ID);

            //photonView.RPC("addPower", PhotonTargets.All, int.Parse(name), PhotonNetwork.player.ID);

            if (collider.gameObject.GetComponent<PhotonView>().isMine)
                ChangeInfo.instance.ChangeText(power.Name(), infoColor);

            //Destroy PowerUp Gameobject
            //! Only master client can destroy objects
            photonView.RPC("destroyObject", PhotonTargets.MasterClient);

        }
    }

    [PunRPC]
    void addPower()
    {

    }

    [PunRPC]
    void destroyObject()
    {
        PhotonNetwork.Destroy(this.gameObject);
    }
}
