﻿using UnityEngine;
using System.Linq;
using Photon;

public class PunPowerUpManager : PunBehaviour
{
    public static PunPowerUpManager instance;
    

    #region VARIABLES

    [Range(0, 1)]
    public float AreaTolerance = 0.2f; //Area Coliison Detection Offset
    public float SpawnFrequency = 5; //Spawn frequency
    public float StartDelay; //Spawn Start Time

    public static System.Type[] powerups; //Holds PowerUp Types
    #endregion

    void Awake()
    {
        instance = this;
    }

    public void getPowers()
    {
        //powerups = new System.Type[1] { typeof(PowerUpSystem.PowerUps.IncreaseMovementSpeed) };
        powerups = System.Reflection.Assembly.GetAssembly(typeof(PowerUpSystem.PowerUpBase)).GetTypes().Where(x => x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Self)) || x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Enemy))).ToArray();
    }

    public void startSpawn()
    {
        //Plugin.Load();

        if (PhotonNetwork.isMasterClient)
        {
            InvokeRepeating("spawnPower", StartDelay, SpawnFrequency);
        }

    }

    void spawnPower()
    {
        Vector2 spawnlocation = generateRandomLocation();
        int rng = Random.Range(0, powerups.Length - 1);

        PhotonNetwork.InstantiateSceneObject("PunPowerUp", spawnlocation, Quaternion.identity, 0, new object[] { rng });

        PunInfoManager.instance.NetworkInfo("Powerup Spawned", Color.blue);
    }

    Vector2 generateRandomLocation()
    {
        float min_x = AutoBounds.Bounds.Min_X;
        float min_y = AutoBounds.Bounds.Min_Y;
        float max_x = AutoBounds.Bounds.Max_X;
        float max_y = AutoBounds.Bounds.Max_Y;

        float pos_x;
        float pos_y;

        int ExceptionBreak = 0;

        do
        {
            pos_x = Random.Range(min_x, max_x);
            pos_y = Random.Range(min_y, max_y);
            ExceptionBreak++;
            if (ExceptionBreak > 500)
            {
                Debug.LogError("No Options Available");
                Debug.Break();
                throw new System.Exception();
            }

        }
        while (Physics2D.OverlapArea(new Vector2(pos_x - AreaTolerance, pos_y + AreaTolerance), new Vector2(pos_x + AreaTolerance, pos_y - AreaTolerance)) == true);

        return new Vector2(pos_x, pos_y);

    }

}

