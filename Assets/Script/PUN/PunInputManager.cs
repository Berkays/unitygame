﻿using UnityEngine;
using System.Collections;

public class PunInputManager : MonoBehaviour
{
    public static PunInputManager instance;

    PunController character_Controller;
    PunSwordRotation sword_Controller;

    PunStatManager stat_Controller;
    InGameMenuHandler ui_Controller;
    PunGameStarter start_Controller;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	public void Init()
    {
        var Player = ((Transform)PhotonNetwork.player.TagObject).parent;

        sword_Controller = Player.GetComponentInChildren<PunSwordRotation>();
        character_Controller = Player.GetComponentInChildren<PunController>(); 
	}


    public void disable_BallControls()
    {

        character_Controller.enabled = false;
        sword_Controller.enabled = false;
    }
    public void enable_BallControls()
    {
        character_Controller.enabled = true;
        sword_Controller.enabled = true;
    }

    public void enable_ChatControls()
    {

    }
    public void disable_ChatControls()
    {

    }

}
