﻿using System;
using UnityEngine;

public class PunPlayer : Photon.MonoBehaviour
{

    public class PlayerData : System.IComparable<PlayerData>
    {
        public int ID;
        public string Name;
        public int Kill;
        public int Death;
        public int Ping;
        public bool isAlive;

        public PlayerData()
        {
            //For Remote Initialization
            isAlive = true;
        }
        public PlayerData(int id, string name)
        {
            ID = id;
            Name = name;
            Kill = 0;
            Death = 0;
            Ping = 0;
            isAlive = true;
        }

        public int CompareTo(PlayerData other)
        {
            if (this.Kill > other.Kill)
            {
                return 1;
            }
            else if (this.Kill < other.Kill)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
    public PlayerData player;

    [PunRPC]
    void updateStatRPC(int Id, string Name, int Kill, int Death, int Ping)
    {
        if (photonView.ownerId == Id)
        {
            if (this.player == null)
            {
                player = new PlayerData();
            }

            player.ID = Id;
            player.Name = Name;
            player.Kill = Kill;
            player.Death = Death;
            player.Ping = Ping;

            PunStatManager.instance.UpdateValues(player);
        }
    }

    public void createData()
    {
        player = new PlayerData(PhotonNetwork.player.ID, PhotonNetwork.playerName);

        updateStat();

        InvokeRepeating("Ping", 2f, 10f);
    }

    void updateStat()
    {
        PunStatManager.instance.UpdateValues(player);
        PunGameManager.instance.checkGameCondition();

        photonView.RPC("updateStatRPC", PhotonTargets.OthersBuffered, player.ID, player.Name, player.Kill, player.Death, player.Ping);
    }

    public void increaseKill()
    {
        player.Kill++;
        updateStat();
    }
    public void increaseDeath()
    {
        player.Death++;
        updateStat();
    }

#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (photonView.isMine)
            {
                player.Kill++;
                updateStat();
            }
        }
    }
#endif

    /// <summary>
    /// Update ping value every 10 seconds
    /// </summary>
    void Ping()
    {
        int pingTripTime = PhotonNetwork.GetPing();
        player.Ping = pingTripTime;
        updateStat();
    }

    /// <summary>
    /// Stop Pinging
    /// </summary>
    public void OnApplicationQuit()
    {
        CancelInvoke();
    }


}