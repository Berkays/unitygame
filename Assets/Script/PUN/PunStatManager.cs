﻿using UnityEngine;
using UnityEngine.UI;
using Photon;


public class PunStatManager : PunBehaviour
{
    public static PunStatManager instance;
    
    public GameObject StatPrefab;
    public Transform StatParent;

    private System.Collections.Generic.HashSet<int> idSet = new System.Collections.Generic.HashSet<int>();

    void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Stat panel controls
    /// </summary>
    void Update()
    {
        bool showStats = Input.GetKey(KeyCode.Tab);

        if (showStats == true && InGameMenuHandler.instance.StatPanel.activeSelf == false)
        {
            InGameMenuHandler.instance.StatPanel.SetActive(true);
        }
        if (showStats == false && InGameMenuHandler.instance.StatPanel.activeSelf == true)
        {
            InGameMenuHandler.instance.StatPanel.SetActive(false);
        }

    }

    public void UpdateValues(PunPlayer.PlayerData player)
    {
        if (idSet.Contains(player.ID) == false && PhotonNetwork.isMasterClient)
        {
            createEntry(player.ID);
        }

        for (int i = 0; i < StatParent.childCount; i++)
        {
            Transform t = StatParent.GetChild(i);
            if (t.name == player.ID.ToString())
            {
                ChildText.Set(t, player);
                break;
            }
        }

        //Sort
        sortEntry(player.ID);
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        //Add entry
        if (PhotonNetwork.isMasterClient)
            createEntry(newPlayer.ID);

    }
    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        //Delete object
        if (PhotonNetwork.isMasterClient)
            removeEntry(otherPlayer.ID);
    }

    void createEntry(int playerID)
    {
        //Player is present
        if (idSet.Contains(playerID))
        {
            return;
        }
        else
        {
            idSet.Add(playerID);
        }

        if (PhotonNetwork.isMasterClient)
            PhotonNetwork.InstantiateSceneObject("PunStat", Vector3.one, Quaternion.identity, 0, new object[] { playerID });
    }
    void removeEntry(int id)
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        //Remove Gameobject
        for (int i = 0; i < StatParent.childCount; i++)
        {
            if (StatParent.GetChild(i).name == id.ToString())
            {
                PhotonNetwork.Destroy(StatParent.GetChild(i).gameObject);
                break;
            }

        }
    }
    void sortEntry(int id)
    {
        for (int i = 0; i < StatParent.childCount; i++)
        {
            Transform t = StatParent.GetChild(i);
            if (t.name == id.ToString())
            {
                if (i > 0)
                {
                    Transform upper = StatParent.GetChild(i - 1);
                    if (int.Parse(ChildText.retrieve(t, 1)) > int.Parse(ChildText.retrieve(upper, 1)))
                    {
                        int temp = t.GetSiblingIndex();
                        t.SetSiblingIndex(upper.GetSiblingIndex());
                        upper.SetSiblingIndex(temp);
                    }
                }
                break;
            }

        }
    }

}

public static class ChildText
{

    public static void Set(Transform parent, PunPlayer.PlayerData player)
    {
        parent.GetChild(0).GetComponent<Text>().text = player.Name;
        parent.GetChild(1).GetComponent<Text>().text = player.Kill.ToString();
        parent.GetChild(2).GetComponent<Text>().text = player.Death.ToString();
        parent.GetChild(3).GetComponent<Text>().text = player.Ping.ToString() + " ms";

    }

    public static void New(Transform parent, string name, int pingTime)
    {

        parent.GetChild(0).GetComponent<Text>().text = name;
        parent.GetChild(1).GetComponent<Text>().text = "0";
        parent.GetChild(2).GetComponent<Text>().text = "0";
        parent.GetChild(3).GetComponent<Text>().text = pingTime + " ms";
    }

    public static string retrieve(Transform parent, int index)
    {
        return parent.GetChild(index).GetComponent<Text>().text;
    }

}
