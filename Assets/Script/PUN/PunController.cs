﻿using UnityEngine;
using System.Collections;
using Photon;

public class PunController : PunBehaviour
{

    private Rigidbody2D _rigidbody;

    [HideInInspector]
    public UISlider slider;

    #region Movement Variables
    [Range(0, 20)]
    public float Speed = 5;
    [Range(0, 50)]
    public float MaxSpeed;
    [Range(0, 200)]
    public float RotateSpeed = 200;
    [Range(0, 20)]
    public float Acceleration = 1;
    [Range(0, 100)]
    public float Jumpforce;
    [Range(0, 10)]
    public float GroundOffset;
    [Range(0, 20)]
    public float BoostValue;
    [Range(0, 60)]
    public float BoostCooldown;

    [HideInInspector]
    public float MovementMultiplier = 1;
    [HideInInspector]
    public bool isBoostUsed = false;

    #endregion


    float Horizontal;
    bool touch = false;
    private float initialSpeed;
    int layermask;

    #region Network Variables
    private double lastTime = 0.0f;
    private double currentTime = 0.0f;
    private double currentPackageTime = 0.0f;
    private Vector2 syncStartPosition = Vector2.zero;
    private Vector2 syncEndPosition = Vector2.zero;
    private Vector2 syncVelocity = Vector2.zero;
    private Quaternion syncEndrotation = Quaternion.identity;
    private Quaternion syncStartRotation = Quaternion.identity;
    #endregion

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (_rigidbody == null)
            return;

        if (stream.isWriting)
        {
            syncEndrotation = this.transform.rotation;
            syncEndPosition = this.transform.position;
            stream.Serialize(ref syncEndPosition);
            stream.Serialize(ref syncEndrotation);

            stream.SendNext(_rigidbody.velocity);
        }
        else
        {
            stream.Serialize(ref syncEndPosition);
            stream.Serialize(ref syncEndrotation);

            syncVelocity = (Vector2)stream.ReceiveNext();

            currentTime = 0;
            lastTime = currentPackageTime;
            currentPackageTime = info.timestamp;

            syncStartPosition = this.transform.position;
            syncStartRotation = this.transform.rotation;
        }

    }
    private void SyncedMovement()
    {
        double travelTime = currentPackageTime - lastTime;
        currentTime += Time.deltaTime;

        _rigidbody.velocity = syncVelocity;
        this.transform.position = Vector2.Lerp(syncStartPosition, syncEndPosition, (float)(currentTime / travelTime));
        this.transform.rotation = Quaternion.Lerp(syncStartRotation, syncEndrotation, (float)(currentTime / travelTime));


    }


    // Use this for initialization
    void Start()
    {
        

        initialSpeed = Speed;
        _rigidbody = this.GetComponent<Rigidbody2D>(); //Cache

        if (photonView.isMine)
        {

            slider = GameObject.Find("DashProgress").GetComponent<UISlider>(); // Get Dash ProgressSlider
            Camera.main.gameObject.GetComponent<CameraBound>().target = this.transform; //CameraBound-Target
        }

        Camera.main.gameObject.GetComponent<CameraBound>().enabled = true;

        layermask = 1 << LayerMask.NameToLayer("Elements");

    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.isMine)
        {
            SyncedMovement();
            return;
        }
        //Get Horizontal movement
        Horizontal = Input.GetAxis("Horizontal");
        //Acceleration
        AdjustSpeed(Horizontal);
        transform.Rotate(Vector3.back, Horizontal * MovementMultiplier * RotateSpeed * Time.deltaTime * Speed, Space.Self);

        //Debug.Log("VELOCITY : " + _rigidbody.velocity.normalized);
    }
    void FixedUpdate()
    {
        if (!photonView.isMine)
            return;

        if (isGrounded() == true && Input.GetKey(KeyCode.W) && touch == true)
        {
            _rigidbody.AddForce(Vector2.up * Jumpforce, ForceMode2D.Impulse);
        }
        if ((Input.GetMouseButton(0) == true || Input.GetKey(KeyCode.Space)) && isBoostUsed == false && (Horizontal != 0 || _rigidbody.velocity.y != 0))
        {
            _rigidbody.AddForce(_rigidbody.velocity.normalized * BoostValue, ForceMode2D.Impulse);
            StartCoroutine("BoostCo");

            StartCoroutine("DashSliderCo");
        }
        

        _rigidbody.velocity = new Vector2(Horizontal * Speed * MovementMultiplier, _rigidbody.velocity.y);
    }

    bool isGrounded()
    {
        return Physics2D.Linecast(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x, transform.position.y - GroundOffset), layermask);

    }
    void OnCollisionStay2D(Collision2D col)
    {

        if (col.collider.gameObject.layer == LayerMask.NameToLayer("Elements"))
        {
            touch = true;
        }

    }
    void OnCollisionExit2D(Collision2D col)
    {

        if (col.collider.gameObject.layer == LayerMask.NameToLayer("Elements"))
        {
            touch = false;
        }
    }

    void AdjustSpeed(float Input)
    {
        if (Input == 0)
        {
            Speed = initialSpeed;
            return;
        }
        if (Speed < MaxSpeed)
        {
            Speed += Acceleration;
            //_rigidbody.AddForce(Horizontal, ForceMode2D.Force);
            //! CHANGES SLOWLY Speed = Mathf.Lerp(Speed, MaxSpeed, Time.deltaTime * Acceleration);
        }


    }

    IEnumerator BoostCo()
    {
        //Horizontal *= BoostValue;
        //yield return new WaitForSeconds(0.05f);
        //Horizontal /= BoostValue;

        isBoostUsed = true;

        float cooltime = BoostCooldown;
        while (cooltime != 0)
        {
            yield return new WaitForSeconds(1f);
            cooltime--;
        }

        isBoostUsed = false; //TODO IF FREEZED DONT ACTIVATE
    }
    IEnumerator DashSliderCo()
    {
        slider.value = 0;
        slider.alpha = 0.5f;
        float rate = 1 / BoostCooldown;
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime * rate;
            slider.value = i;
            yield return null;
        }
        slider.alpha = 1;
    }
}
