﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PunConnectionManager : Photon.PunBehaviour
{
    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        PunInfoManager.instance.NetworkInfo(otherPlayer.name + " disconnected from the game",null);
    }
    public override void OnDisconnectedFromPhoton()
    {
        //Load Main Menu
        SceneManager.LoadScene("MainMenu");
    }

    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        //Restart PowerUpManager
        PunPowerUpManager.instance.startSpawn();
    }
}
