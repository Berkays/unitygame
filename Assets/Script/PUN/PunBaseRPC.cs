﻿using UnityEngine;
using Photon;
using System.Linq;
using System.Collections;

public class PunBaseRPC : PunBehaviour
{
    public static PunBaseRPC instance;

    void Start()
    {
        instance = this;
    }

    public void UniversalRPC(string Method, PhotonTargets Receivers)
    {
        photonView.RPC(Method, Receivers);
    }
    public void UniversalRPC<T>(string Method, PhotonTargets Receivers, T obj)
    {
        photonView.RPC(Method, Receivers, obj);
    }
    public void UniversalRPC<T, K>(string Method, PhotonTargets Receivers, T obj, K _obj)
    {
        photonView.RPC(Method, Receivers, obj, _obj);
    }
    public void UniversalRPC<T, K, J>(string Method, PhotonTargets Receivers, T obj, K _obj, J _t)
    {
        photonView.RPC(Method, Receivers, obj, _obj, _t);
    }

    [PunRPC]
    private void AddPower_Enemy(int pIndex, int Id)
    {
        ////RPC goes to all players
        //if (!photonView.isMine)
        //    return;

        ////Don't add enemy power to picker
        //if (PhotonNetwork.player.ID == Id)
        //    return;

        var arr = GameObject.FindGameObjectsWithTag("Player").Where(x => x.GetComponent<PhotonView>().ownerId != Id);

        System.Type type = PunPowerUpManager.powerups[pIndex];

        foreach (var player in arr)
        {
            //Get local player transform
            //var playerObject = ((Transform)PhotonNetwork.player.TagObject).gameObject;

            if (player.GetComponent(type) == null)
            {
                var Component = player.AddComponent(type) as PowerUpSystem.PowerUpBase;
                Component.Enable(player);
            }

            //Powers dont stack
            //if (playerObject.GetComponent(type) == null)
            //{
            //    var Component = playerObject.AddComponent(type) as PowerUpSystem.PowerUpBase;
            //    Component.Enable(playerObject);
            //}
        }

    }

    [PunRPC]
    private void AddPower_Self(int pIndex, int Id)
    {
        var playerObject = GameObject.FindGameObjectsWithTag("Player").First(x => x.GetComponent<PhotonView>().ownerId == Id);

        System.Type type = PunPowerUpManager.powerups[pIndex];

        //Powers dont stack
        if (playerObject.GetComponent(type) == null)
        {
            var Component = playerObject.AddComponent(type) as PowerUpSystem.PowerUpBase;
            Component.Enable(playerObject);
        }
    }

}
