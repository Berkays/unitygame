﻿using UnityEngine;
using System.Collections;

public class PunSwordRotation : Photon.MonoBehaviour
{

    public Material Mat_Normal, Mat_Transparent;

    private SpriteRenderer _TSprite;
    [HideInInspector]
    public UISlider slider; //RotationSliderUI

    private bool effective = false;
    public bool Effective
    {
        get
        {
            return effective;
        }

        set
        {
            effective = value;

            if (_TSprite == null)
                return;

            if (effective)
            {
                _TSprite.material = Mat_Normal;
            }
            else
            {
                _TSprite.material = Mat_Transparent;
            }
        }
    }


    [HideInInspector]
    public bool isRotated = false;

    #region Customizable
    public float ManualRotationCooldown = 5;
    public float SwordRotateSpeed = 400;
    #endregion

    void Start()
    {
        _TSprite = this.GetComponent<SpriteRenderer>();

        if (!photonView.isMine)
            return;

        slider = GameObject.Find("RotationProgress").GetComponent<UISlider>();

        //Rotate Randomly
        int rnd = Random.Range(0, 360);
        this.transform.parent.Rotate(Vector3.back, rnd, Space.Self);
    }
    void Update()
    {

        if (!photonView.isMine)
            return;

        this.transform.parent.Rotate(Vector3.back, SwordRotateSpeed * Time.deltaTime, Space.Self);

        #region GIRIS
        if (Input.GetMouseButtonDown(1) == true && isRotated == false)
        {
            SwordRotateSpeed *= -1; // Reverse Sword
            StartCoroutine("RotationCo"); //Corotine
            StartCoroutine("RotationSliderCo"); //ProgressBar         
        }
        #endregion


    }

    void OnTriggerEnter2D(Collider2D col)
    {

        int CollidedObjectLayer = col.gameObject.layer;

        if (CollidedObjectLayer == LayerMask.NameToLayer("Elements"))
        {
            Effective = false;
        }
        else if (CollidedObjectLayer == LayerMask.NameToLayer("Sword") && Effective == true)
        {
            SwordRotateSpeed *= -1;
        }

    }
    void OnTriggerStay2D(Collider2D col)
    {
        int CollidedObjectLayer = col.gameObject.layer;

        if (CollidedObjectLayer == LayerMask.NameToLayer("Elements"))
        {
            Effective = false;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {

        if (col.gameObject.layer == 9) // 9 Element
        {
            Effective = true;
        }
    }

    public void DisableEnableCollider() //Used by powerup system
    {
        BoxCollider2D component = this.GetComponent<BoxCollider2D>();
        if (component.enabled == true)
        {
            component.enabled = false;
        }
        else
        {
            component.enabled = true;
        }
    }

    #region COROUTINES

    IEnumerator RotationCo()
    {
        isRotated = true;
        yield return new WaitForSeconds(ManualRotationCooldown);
        isRotated = false;
    }
    IEnumerator RotationSliderCo()
    {
        slider.value = 0;
        slider.alpha = 0.5f;
        float rate = 1 / ManualRotationCooldown;
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime * rate;
            slider.value = i;
            yield return null;
        }
        slider.alpha = 1;
    }
    #endregion
}

