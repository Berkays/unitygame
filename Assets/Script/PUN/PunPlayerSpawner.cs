﻿using UnityEngine;
using Photon;
using System.Collections.Generic;

public class PunPlayerSpawner : PunBehaviour
{
    const string SPAWN_TRANSFORM = "SpawnLocations";
    private Transform SpawnLocations;

    public CameraBound cameraScript;

    void Start()
    {
        MapLoader.OnLoaded += setSpawn;
    }
    private void setSpawn()
    {
        SpawnLocations = GameObject.Find(SPAWN_TRANSFORM).transform;
        MapLoader.OnLoaded -= setSpawn;
    }


    public void startPlayers()
    {
        if (PhotonNetwork.isMasterClient)
            masterClientSpawn();
    }

    private void masterClientSpawn()
    {
        List<int> usedLocations = new List<int>(SpawnLocations.childCount);

        foreach (var player in PhotonNetwork.playerList)
        {
            int loc = getSpawnPoint(usedLocations);

            photonView.RPC("spawnRPC", player, loc);
        }
    }

    /// <summary>
    /// Return a random position from available spawn points
    /// </summary>
    /// <returns>Random position</returns>
    private int getSpawnPoint(List<int> usedList)
    {
        if (usedList.Count == SpawnLocations.childCount)
            usedList.Clear();

        int rng = Random.Range(0, SpawnLocations.childCount);

        while (usedList.Contains(rng) == true)
        {
            rng = Random.Range(0, SpawnLocations.childCount);
        }

        usedList.Add(rng);

        return rng;
    }

    [PunRPC]
    private void spawnRPC(int childIndex)
    {
        spawnPlayer(childIndex);
    }
    private void spawnPlayer(int childIndex)
    {
        Vector2 pos = SpawnLocations.GetChild(childIndex).transform.position;

        GameObject obj = PhotonNetwork.Instantiate("PunPlayer", pos, Quaternion.identity, 0);

        PhotonNetwork.player.TagObject = obj.transform.GetChild(0); //REFERENCES BALL OBJECT

        obj.GetComponent<PunPlayer>().createData();

        photonView.RPC("informRPC", PhotonTargets.Others, PhotonNetwork.playerName);

        cameraScript.target = obj.transform.GetChild(0);

        if (photonView.isMine)
            cameraScript.enabled = true;

    }

    [PunRPC]
    private void informRPC(string playerName)
    {
        ChangeInfo.instance.ChangeText(playerName + " Joined The Game", Color.yellow);
    }


}
