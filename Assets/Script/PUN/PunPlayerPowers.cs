﻿using UnityEngine;
using Photon;
using PowerUpSystem;
using System.Collections;
using System.Collections.Generic;

public class PunPlayerPowers : Photon.MonoBehaviour
{
    public void addPower(PowerUpBase power)
    {
        var Type = power.GetType();

        if (this.gameObject.GetComponent(Type) == null)
        {
            var Component = this.gameObject.AddComponent(Type) as PowerUpBase;
            Component.Enable(this.gameObject);
        }
    }
}
