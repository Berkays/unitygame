﻿using UnityEngine;
using UnityEngine.UI;

public class PunOnStatInstantiate : Photon.MonoBehaviour
{
    readonly Color ownColor = new Color(0,255,150,0.16f);

    void Start()
    {
        int playerId = (int)photonView.instantiationData[0];
        transform.name = playerId.ToString();
        //ChildText.New(transform, (string)photonView.instantiationData[0], 100);
        transform.SetParent(InGameMenuHandler.instance.StatInfoTransform, false);

        if (PhotonNetwork.player.ID == playerId)
            transform.GetComponent<Image>().color = ownColor;
    }
}
