﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Collections;
using Photon;
using GameUI.Online;

public class PunGameStarter : PunBehaviour
{
    #region Variables
    //To be activated
    public GameObject MainCamera;
    public PunPowerUpManager powerupScript;
    public PunStatManager statScript;
    public PunPlayer playerScript;
    public PunInputManager inputScript;

    public UIPanel NGUI_A;

    //UI Elements
    public Button JoinButton;

    public Text serverInfo;
    public Text gameInfo;

    public GameObject playerEntry;
    public Transform playerTransform;
    #endregion

    void Start()
    {
        //? TEST
        PhotonNetwork.sendRate = 30;
        PhotonNetwork.sendRateOnSerialize = 30;

        Invoke("Init", 0.5f);
    }

    void Init()
    {

        if (PhotonNetwork.isMasterClient)
        {
            foreach (var Player in PhotonNetwork.playerList)
            {
                PhotonNetwork.RemoveRPCs(Player);
            }
        }
        
        //Update player name and ping
        initPlayer();


        //update server stats
        InvokeRepeating("setUi", 0.1f, 8);

        //Sync map to other clients
        if (PhotonNetwork.isMasterClient)
            startSync();
    }

    void setUi()
    {
        //Server Info
        // Name
        // PlayerCount
        string serverInfo = "Connected to " + PhotonNetwork.room.name + "\n(" + PhotonNetwork.room.playerCount + "/" + PhotonNetwork.room.maxPlayers + ")";
        this.serverInfo.text = serverInfo;

        int score = (int)PhotonNetwork.room.customProperties["GameScore"];
        string gameInfo_type = (PunGameSettings.GameType)PhotonNetwork.room.customProperties["GameType"] == PunGameSettings.GameType.Timed ? "Most kills in " + score + " Minutes" : "First to reach " + score + " kills";
        gameInfo.text = "Objective : " + gameInfo_type;
    }

    #region Player List

    private void initPlayer()
    {
        string Name = PlayerPrefs.GetString("PlayerName");
        PhotonNetwork.playerName = Name;

        //Update ping every 10 sec
        InvokeRepeating("updatePing", 0.01f, 10);

        //Update ready state
        updateReady(false);

    }
    private void updatePing()
    {
        ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
        properties.Add("Ping", PhotonNetwork.GetPing());

        PhotonNetwork.player.SetCustomProperties(properties);

        photonView.RPC("refreshRPC", PhotonTargets.All);
    }
    private void updateReady(bool value)
    {
        ExitGames.Client.Photon.Hashtable newProperties = new ExitGames.Client.Photon.Hashtable();
        newProperties.Add("Ready", value);

        PhotonNetwork.player.SetCustomProperties(newProperties);

        JoinButton.GetComponent<Text>().text = value == true ? "Waiting to start" : "Get Ready";

        //Send Update UI RPC
        photonView.RPC("refreshRPC", PhotonTargets.All);

        //Inform master client that we are ready
        if (!PhotonNetwork.isMasterClient)
            photonView.RPC("readyRPC", PhotonTargets.MasterClient);
    }

    private void createPlayers()
    {
        try
        {
            var playerList = PhotonNetwork.otherPlayers;

            preparePrefabs(playerList.Length + 1);

            Text label = playerTransform.GetChild(0).GetComponent<Text>();

            label.text = PhotonNetwork.player.name + " - " + (int)PhotonNetwork.player.customProperties["Ping"] + " ms";
            label.color = Color.green;

            bool isReady = (bool)PhotonNetwork.player.customProperties["Ready"];
            playerTransform.GetChild(0).GetComponentInChildren<Image>().enabled = isReady;

            for (int i = 0; i < playerList.Length; i++)
            {
                label = playerTransform.GetChild(i + 1).GetComponent<Text>();

                label.text = playerList[i].name + " - " + (int)playerList[i].customProperties["Ping"] + " ms";

                isReady = (bool)playerList[i].customProperties["Ready"];
                playerTransform.GetChild(i + 1).GetComponentInChildren<Image>().enabled = isReady;
            }
        }
        catch
        {
            createPlayers();
        }

    }
    private void preparePrefabs(int playerCount)
    {
        int prefabCount = playerTransform.childCount;

        if (prefabCount > playerCount)
        {
            //Remove extra prefabs

            for (int i = 0; i < prefabCount - playerCount; i++)
            {
                Destroy(playerTransform.GetChild(i).gameObject);
            }

        }
        else if (prefabCount < playerCount)
        {
            //Create new prefabs

            for (int i = 0; i < playerCount - prefabCount; i++)
            {
                GameObject instance = Instantiate(playerEntry) as GameObject;
                instance.transform.SetParent(playerTransform, false);
            }
        }

    }

    [PunRPC]
    private void refreshRPC()
    {
        Invoke("createPlayers", 0.5f);
    }

    [PunRPC]
    private void readyRPC()
    {
        if (checkPlayers())
        {
            //ALl Ready
            JoinButton.interactable = true;
            JoinButton.GetComponent<Text>().text = "Start Game";
        }
        else
        {
            JoinButton.interactable = false;
            JoinButton.GetComponent<Text>().text = "Waiting For Players...";
        }
    }
    private bool checkPlayers()
    {
        return !(PhotonNetwork.otherPlayers.Any(x => (bool)x.customProperties["Ready"] == false));
    }
    #endregion

    #region Map Sync

    void startSync()
    {
        //Send MD5 Hash to clients
        byte[] hash = LevelSerializerProtocol.LevelSerializer.GetHash(PunServerSync.selectedMaps.Peek());

        photonView.RPC("hashRPC", PhotonTargets.OthersBuffered, PunServerSync.selectedMaps.Peek(), hash);

        //load Map
        loadMap(PunServerSync.selectedMaps.Peek());
    }

    [PunRPC]
    void hashRPC(string mapName, byte[] md5)
    {
        bool valid = LevelSerializerProtocol.LevelSerializer.HashCheck(mapName, md5);

        if (!valid)
        {
            JoinButton.GetComponent<Text>().text = "Downloading Map...";

            //Request Map
            photonView.RPC("sendRequestRPC", PhotonTargets.MasterClient, PhotonNetwork.player);
        }
        else
        {
            loadMap(mapName);
        }
    }
    [PunRPC]
    void sendRequestRPC(PhotonPlayer player)
    {
        //Runs in master client
        photonView.RPC("sendRPC", player, PunServerSync.selectedMaps.Peek(), PunServerSync.Serialized);
    }
    [PunRPC]
    void sendRPC(string mapName, byte[] data)
    {
        //If save enabled
        LevelSerializerProtocol.LevelSerializer.Save(mapName, data);

        loadMap(mapName);
    }

    void loadMap(string mapToLoad)
    {
        //When Map Load Finishes
        Action OnMapLoad = null;

        OnMapLoad = () =>
        {
            //Activate Join Button
            JoinButton.interactable = true;
            JoinButton.transform.GetComponent<Text>().text = PhotonNetwork.isMasterClient == true ? "Start Game" : "Get Ready";

            //Unsubscribe event to prevent memory leak
            MapLoader.OnLoaded -= OnMapLoad;

        };

        MapLoader.OnLoaded += OnMapLoad;

        JoinButton.GetComponent<Text>().text = "Loading Map...";
        //Start Loading Map
        MapLoader.LoadMap(mapToLoad);
    }

    #endregion

    #region Setup Game Player


    #endregion

    //Join Game Button
    public void joinGame()
    {
        if (!PhotonNetwork.isMasterClient)
        {
            bool isReady = (bool)PhotonNetwork.player.customProperties["Ready"];
            //Update player property
            //Switch to ready or not
            updateReady(!isReady);
        }
        else
        {
            //Master Client Setup
            disableJoin();

            PunSpawnManager.instance.initialSpawn();
            PunGameManager.instance.startTimer();

            photonView.RPC("setupGame", PhotonTargets.AllViaServer);
        }
    }
    private void disableJoin()
    {
        PhotonNetwork.room.visible = false;
    }

    [PunRPC]
    private void setupGame()
    {
        //Show Dock
        NGUI_A.alpha = 1;

        //Enable Camera
        MainCamera.SetActive(true);

        //PowerUp Spawn
        powerupScript.getPowers();
        powerupScript.startSpawn();

        //Input Manager Script
        inputScript.Init();


        //Disable this Panel
        this.gameObject.SetActive(false);
    }

    //Exit Game Button
    public void exitGame()
    {
        photonView.RPC("refreshRPC", PhotonTargets.All);

        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("MainMenu");
    }
}
