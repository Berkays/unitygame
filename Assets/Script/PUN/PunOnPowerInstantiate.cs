﻿public class PunOnPowerInstantiate : Photon.MonoBehaviour
{
    void Start()
    {
        int powerUp = (int)photonView.instantiationData[0];
        gameObject.AddComponent(PunPowerUpManager.powerups[powerUp]);
        name = powerUp.ToString();
        //Set Texture
    }
}
