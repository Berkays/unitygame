﻿using UnityEngine;
using System.Linq;
using GameUI.Online;
using UnityEngine.SceneManagement;
using System.Collections;

public class PunGameManager : Photon.MonoBehaviour
{
    public static PunGameManager instance;


    PhotonPlayer winnerPlayer;

    bool TimeFinished = false;
    int Time = 0;

    void Awake()
    {
        instance = this;
    }

    public void startTimer()
    {
        if (PunServerSync.gameType == PunGameSettings.GameType.Timed && PhotonNetwork.isMasterClient)
        {
            Time = PunServerSync.gameScore;
            //InvokeRepeating("clock", 0, 1);
            InvokeRepeating("clock", 60, 60);
        }
    }

    void clock()
    {
        if (Time > 0)
        {
            Time--;
            //PunInfoManager.instance.NetworkBanner(Time + " Minutes Left", Color.red);
        }
        else
        {
            TimeFinished = true;
            CancelInvoke();
            checkGameCondition();
        }
    }

    public void checkGameCondition()
    {
        photonView.RPC("checkGameConditionRPC", PhotonTargets.MasterClient);
    }

    [PunRPC]
    private void checkGameConditionRPC()
    {
        if (PunServerSync.gameType == PunGameSettings.GameType.Scored)
        {
            GameObject obj = GameObject.FindGameObjectsWithTag("Player").FirstOrDefault(x => x.GetComponentInParent<PunPlayer>().player.Kill >= PunServerSync.gameScore);
            if (obj != null)
            {
                winnerPlayer = PhotonPlayer.Find(obj.GetPhotonId());

                gameFinish();
            }
        }
        else if (PunServerSync.gameType == PunGameSettings.GameType.Timed)
        {
            if (TimeFinished == true)
            {
            var i = FindObjectsOfType<PunPlayer>().OrderBy(x => x.player.Kill).First();
            winnerPlayer = PhotonPlayer.Find(i.gameObject.GetPhotonId());

                gameFinish();
            }
        }
    }

    void gameFinish()
    {
        //Works in MasterClient


        //Disable inputs for all clients
        disableInput();


        PunInfoManager.instance.NetworkInfo("Game Finished", Color.blue);
        PunInfoManager.instance.NetworkInfo("Winner is : " + winnerPlayer.name, Color.green);

        // Discard Current Map
        PunServerSync.selectedMaps.Dequeue();

        if (PunServerSync.selectedMaps.Count == 0)
        {
            //If no map left
            //return to main menu

            PunInfoManager.instance.NetworkBanner("Returning To Main Menu...", Color.red);

            photonView.RPC("gameFinishRPC", PhotonTargets.All, 0);

        }
        else
        {
            PunInfoManager.instance.NetworkBanner("Loading Next Map...", Color.red);

            //Load Next Map
            photonView.RPC("gameFinishRPC", PhotonTargets.All,3);

        }

    }

    [PunRPC]
    void gameFinishRPC(int SceneIndex)
    {
        //TODO Show Finished UI For everyone
        //TODO Show Stats
        //TODO Countdown

        StartCoroutine("loadCountdown", SceneIndex);

    }

    IEnumerator loadCountdown(int SceneIndex)
    {
        
        yield return new WaitForSeconds(5);
        
        //Unload
        SceneManager.UnloadScene(SceneManager.GetActiveScene().buildIndex);
        //load next level
        SceneManager.LoadScene(SceneIndex, LoadSceneMode.Single);        
    }


    #region Controls

    void disableInput()
    {
        photonView.RPC("disableInputRPC", PhotonTargets.All);
    }

    [PunRPC]
    void disableInputRPC()
    {
        Transform Ball = ((Transform)PhotonNetwork.player.TagObject);
        Ball.GetComponent<PunController>().enabled = false;
        Ball.parent.GetComponentInChildren<PunSwordCollision>().enabled = false;
        Ball.parent.GetComponentInChildren<PunSwordRotation>().enabled = false;
    }

    #endregion


}
