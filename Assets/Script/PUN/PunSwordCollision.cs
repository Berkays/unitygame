﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PunSwordCollision : Photon.MonoBehaviour
{
    //This is for fixing multiple kill counts
    List<int> kList = new List<int>(8);

    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (!photonView.isMine)
            return;

        int CollidedObjectLayer = col.gameObject.layer;

        //This script only cares about the player collision
        if (CollidedObjectLayer != LayerMask.NameToLayer("Player"))
            return;
        
        //Check if the target is alive or not
        if (col.gameObject != transform.parent.parent.GetChild(0).gameObject) //Check 
        {
            if (col.transform.GetComponentInParent<PunPlayer>().player.isAlive == true)
            {
                var targetId = col.gameObject.GetPhotonId();

                if (kList.Contains(targetId) == false)
                {
                    StartCoroutine("listHandle", targetId);
                    PunSpawnManager.instance.sendKill(PhotonNetwork.player.ID, targetId);
                    PunSpawnManager.instance.sendDeath(targetId);
                    PunGameManager.instance.checkGameCondition();
                }
            }
        }

    }

    IEnumerator listHandle(int id)
    {
        kList.Add(id);

        yield return new WaitForSeconds(4); //USE SPAWN TIME HERE

        kList.Remove(id);
    }

}
