﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PunInfoManager : Photon.MonoBehaviour
{
    public static PunInfoManager instance;

    #region Info_VARIABLES
    public Transform textContainer;
    public GameObject textPrefab;


    const int ENTRY_LIMIT = 10;
    const int MESSAGE_DURATION = 5;

    int lastIndex = 0;
    bool running = false;
    Coroutine FadeRoutine;
    #endregion

    #region Banner_VARIABLES
    Coroutine BannerRoutine;
    public Text Banner;
    #endregion

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < ENTRY_LIMIT; i++)
        {
            var entry = GameObject.Instantiate(textPrefab);
            entry.transform.SetParent(textContainer, false);
        }

    }


    #region Info_METHODS
    public void NetworkInfo(string Message, Color? TextColor)
    {
        TextColor = TextColor ?? Color.white;

        int colorKey = colorToNum[(Color)TextColor];

        photonView.RPC("NetworkInfoRPC", PhotonTargets.All, Message, colorKey);
    }

    [PunRPC]
    void NetworkInfoRPC(string Message, int ColorKey)
    {
        QueueInfo(Message, numToColor[ColorKey]);
    }

    public void QueueInfo(string Message, Color TextColor)
    {
        if (lastIndex > ENTRY_LIMIT - 1)
        {
            lastIndex = ENTRY_LIMIT - 1;

            //Stop Coroutine for child(0)
            StopCoroutine(FadeRoutine);
            //Start corotuine for child(1)
            FadeRoutine = StartCoroutine("FadeOut", 1);

            InstantFadeOut();
        }
        else if (running == false)
        {
            running = true;
            FadeRoutine = StartCoroutine("FadeOut", 0);
        }

        string msg = Message;

        var textComponent = textContainer.GetChild(lastIndex).GetComponent<Text>();

        textComponent.text = msg;

        textComponent.color = TextColor;

        lastIndex++;

    }

    IEnumerator FadeOut(int index)
    {
        do
        {
            Text Component = textContainer.GetChild(index).GetComponent<Text>();

            yield return new WaitForSeconds(MESSAGE_DURATION - 1);

            //Fade Out to alpha
            StartCoroutine("LerpAlpha", Component);
            //Wait for alpha lerp
            yield return new WaitForSeconds(0.98f);

            //Set it to empty string
            Component.text = string.Empty;

            //Carry to bottom;
            Component.gameObject.transform.SetAsLastSibling();

            lastIndex--;

            if (index == 1)
                break;

        }
        while (lastIndex > 0);
        running = false;
    }
    void InstantFadeOut()
    {
        running = false;

        var Component = textContainer.GetChild(0).GetComponent<Text>();

        Component.text = string.Empty;
        Component.color = Color.white;

        textContainer.GetChild(0).SetAsLastSibling();
    }

    IEnumerator LerpAlpha(Text Component)
    {
        float rate = 1 / 1;
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime * rate;
            Component.CrossFadeAlpha(1 - i, 0, true);
            yield return null;
        }

        Component.CrossFadeAlpha(1, 0, true);

    }
    #endregion

    #region Banner_METHODS
    public void NetworkBanner(string Message, Color? TextColor)
    {
        TextColor = TextColor ?? Color.white;

        int colorKey = colorToNum[(Color)TextColor];

        photonView.RPC("NetworkBannerRPC", PhotonTargets.All, Message, colorKey);

    }
    [PunRPC]
    void NetworkBannerRPC(string Message, int ColorKey)
    {
        BannerInfo(Message, numToColor[ColorKey]);
    }

    public void BannerInfo(string Message, Color TextColor)
    {
        CancelInvoke("BannerFadeOut");

        Banner.color = TextColor;
        Banner.text = Message;

        //Animation Play
        Banner.GetComponent<Animator>().Play("FadeIn");

        Invoke("BannerFadeOut", 3f);
        //BannerRoutine = StartCoroutine("BannerFadeOut");

    }

    void BannerFadeOut()
    {
        Banner.GetComponent<Animator>().Play("FadeOut");
    }

    #endregion

    #region Color Lookup Tables

    readonly Dictionary<int, Color> numToColor = new Dictionary<int, Color>()
    {
        { 0,Color.white },
        { 1,Color.blue },
        { 2,Color.green },
        { 3,Color.red },
        { 4,Color.yellow }
    };
    readonly Dictionary<Color, int> colorToNum = new Dictionary<Color, int>()
    {
        { Color.white,0 },
        { Color.blue,1 },
        { Color.green,2 },
        { Color.red,3 },
        { Color.yellow,4 }
    };

    #endregion
}
