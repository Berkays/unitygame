﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class PunChat : Photon.MonoBehaviour
{
    public GameObject ChatPrefab;
    public Transform ChatParent;

    private InputField input;
    private CanvasGroup textArea;

    private const int FADE_TIME = 5;
    private const int ENTRY_LIMIT = 11;
    private int entryCount = 1;

    bool canWrite = false;
    bool fading = false;

    //ID and COLOR INDEX
    private Dictionary<int, int> playerColors = new Dictionary<int, int>();
    private Color[] Colors =
    {
        Color.red,
        Color.yellow,
        Color.green,
        Color.cyan,
        Color.magenta,
        Color.blue
    };

    // Use this for initialization
    void Start()
    {
        input = ChatParent.GetChild(0).GetComponent<InputField>();
        textArea = ChatParent.GetComponent<CanvasGroup>();

        //Create Instances
        for (int i = 0; i < ENTRY_LIMIT; i++)
        {

            GameObject obj = Instantiate<GameObject>(ChatPrefab);

            obj.transform.SetParent(ChatParent, false);

        }


    }

    // Update is called once per frame
    void Update()
    {

        if (canWrite == true && Input.GetKeyDown(KeyCode.Escape))
        {
            PunInputManager.instance.enable_BallControls();

            canWrite = false;

            textArea.alpha = 0.4f;

            input.DeactivateInputField();
        }

        //If Enter Trigger Chat Disable Controls
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (canWrite == false)
            {
                PunInputManager.instance.disable_BallControls();

                //Start Typing
                canWrite = true;

                textArea.alpha = 1;

                input.ActivateInputField();

            }
            else
            {
                PunInputManager.instance.enable_BallControls();

                //Send Typed Message
                if (string.IsNullOrEmpty(input.text) == false)
                {

                    photonView.RPC("Chat", PhotonTargets.All, PhotonNetwork.player.name + ": " + input.text, PhotonNetwork.player.ID);

                    input.text = string.Empty;

                }
                canWrite = false;
            }

        }

    }

    private void ProcessMessage(string Message, int sender)
    {
        int colorIndex = 0;

        if (playerColors.ContainsKey(sender) == false)
        {

            //Create Random Unique Color
            colorIndex = getUniqueColor();

            playerColors.Add(sender, colorIndex);
        }
        else
        {
            colorIndex = playerColors[sender];
        }

        adjustSpace();

        var Text = ChatParent.GetChild(entryCount).GetComponent<Text>();

        Text.color = Colors[colorIndex];
        Text.text = Message;

        entryCount++;
    }

    private int getUniqueColor()
    {
        int ColorIndex = 0;


        int[] frequency = new int[Colors.Length];

        int min = 128;


        foreach (var i in playerColors)
        {
            frequency[i.Value]++;
        }

        for (int i = 0; i < frequency.Length; i++)
        {

            if (frequency[i] < min)
            {
                min = frequency[i];
                ColorIndex = i;
            }
        }

        return ColorIndex;
    }
    private void adjustSpace()
    {
        if (string.IsNullOrEmpty(ChatParent.GetChild(ENTRY_LIMIT).GetComponent<Text>().text) == false)
        {
            ChatParent.GetChild(1).SetAsLastSibling();
            entryCount--;
        }

    }

    [PunRPC]
    private void Chat(string Message, int sender)
    {
        ProcessMessage(Message, sender);

        startFade();

    }


    void startFade()
    {
        if (fading)
            cancelFade();

        StartCoroutine("Fade");
    }
    void cancelFade()
    {


        StopCoroutine("LerpAlpha");
        StopCoroutine("Fade");

        textArea.alpha = 1f;

        fading = false;
    }

    System.Collections.IEnumerator Fade()
    {
        fading = true;

        yield return new WaitForSeconds(FADE_TIME);

        StartCoroutine("LerpAlpha");
    }
    System.Collections.IEnumerator LerpAlpha()
    {
        float rate = 1 / 1;
        float i = 0;

        float startAlpha = textArea.alpha;
        float targetAlpha = 0.4f;



        //Iterate Through to textArea.alpha to targetAlpha

        while (i < 1 - targetAlpha) //Runs 0.6 sec
        {
            i += Time.deltaTime * rate;

            if (textArea.alpha < 1 - i)
            {
                continue;
            }
            textArea.alpha = 1 - i;
            yield return null;
        }
    }



}


