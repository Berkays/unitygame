﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PunSpawnManager : Photon.MonoBehaviour
{
    public static PunSpawnManager instance;

    //Spawn Time
    const int SPAWN_TIME = 5;

    //Materials for Respawn
    public Material NormalMat, DeadMat;

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }

    #region GameEvents
    /// <summary>
    /// A player calls sendkill which pushes the players and target's photon ID to Master Client
    /// Master Client updates stat and pushes info rpc
    /// </summary>
    /// <param name="player">Killer's id</param>
    /// <param name="target">Died player's id</param>
    public void sendKill(int player, int target)
    {
        photonView.RPC("sendKillRPC", PhotonTargets.MasterClient, player, target);
    }
    [PunRPC]
    void sendKillRPC(int player, int target)
    {
        //Killer
        var Player = PhotonPlayer.Find(player);

        photonView.RPC("updateKillRPC", Player);

        sendInfo(Player.name + " k!lled " + PhotonPlayer.Find(target).name);
    }
    [PunRPC]
    void updateKillRPC()
    {
        //if (PhotonNetwork.player.ID == player)
        ((Transform)PhotonNetwork.player.TagObject).parent.GetComponent<PunPlayer>().increaseKill();
    }

    /// <summary>
    /// A player calls sendDeath with sendKill which updates stat and respawns target on Master Client
    /// </summary>
    /// <param name="target">Died player's id</param>
    public void sendDeath(int target)
    {
        photonView.RPC("sendDeathRPC", PhotonTargets.MasterClient, target);
    }
    [PunRPC]
    void sendDeathRPC(int target)
    {
        photonView.RPC("updateDeathRPC", PhotonPlayer.Find(target));

        respawnPlayer(target);
    }
    [PunRPC]
    void updateDeathRPC()
    {
        ((Transform)PhotonNetwork.player.TagObject).parent.GetComponent<PunPlayer>().increaseDeath();
    }
    #endregion

    #region RESPAWN

    void respawnPlayer(int player)
    {
        photonView.RPC("respawnPlayerRPC", PhotonTargets.All, player);
    }
    [PunRPC]
    void respawnPlayerRPC(int player)
    {
        StartCoroutine("spawnRoutine", player);
    }
    IEnumerator spawnRoutine(int player)
    {
        var Ball = GameObject.FindGameObjectsWithTag("Player").First(x => x.GetPhotonId() == player);

        var playerInstance = Ball.transform.parent.GetComponent<PunPlayer>().player;

        if (PhotonNetwork.player.ID == player)
            Ball.transform.position = new Vector3(Random.Range(1000, 10000), Random.Range(1000, 10000), Random.Range(0, 100));

        playerInstance.isAlive = false;

        Ball.GetComponent<SpriteRenderer>().material = DeadMat;
        Ball.transform.parent.GetComponentInChildren<PunSwordRotation>().Effective = false;
        Ball.transform.parent.GetChild(1).GetChild(0).GetComponent<Collider2D>().enabled = false;

        yield return new WaitForSeconds(SPAWN_TIME);

        if (PhotonNetwork.player.ID == player)
            Ball.transform.position = GetSpawnLocation();

        yield return new WaitForSeconds(SPAWN_TIME);

        playerInstance.isAlive = true;
        Ball.GetComponent<SpriteRenderer>().material = NormalMat;
        Ball.transform.parent.GetComponentInChildren<PunSwordRotation>().Effective = true;
        Ball.transform.parent.GetChild(1).GetChild(0).GetComponent<Collider2D>().enabled = true;

    }
    Vector2 GetSpawnLocation()
    {
        var Locations = GameObject.Find("SpawnLocations").transform;
        int rng = Random.Range(0, Locations.childCount);

        return Locations.GetChild(rng).transform.position;
    }

    //Works only in MASTER CLIENT
    public void initialSpawn()
    {
        int locationCount = GameObject.Find("SpawnLocations").transform.childCount;
        List<int> usedLocations = new List<int>(locationCount);

        foreach (var player in PhotonNetwork.playerList)
        {
            int loc = getUniqueLocation(usedLocations, locationCount);

            photonView.RPC("initialSpawnRPC", player, loc);

        }

        //? FIX
        Invoke("spawnTEST", 1f);
    }
    [PunRPC]
    void initialSpawnRPC(int location)
    {
        //This code only works in client
        Vector2 position = GameObject.Find("SpawnLocations").transform.GetChild(location).position;

        GameObject obj = PhotonNetwork.Instantiate("PunPlayer", position, Quaternion.identity, 0);

        Transform Ball = obj.transform.GetChild(0);

        PhotonNetwork.player.TagObject = Ball;

        Ball.GetComponentInParent<PunPlayer>().createData();

        var cameraComponent = Camera.main.GetComponent<CameraBound>();

        cameraComponent.target = Ball;

        cameraComponent.enabled = true;

        PunInfoManager.instance.NetworkInfo(PhotonNetwork.player.name + " Joined The Game", Color.yellow);

        //StartCoroutine("initialSpawnRoutine", Ball);

    }

    IEnumerator initialSpawnRoutine(Transform Ball)
    {
        Ball.GetComponent<SpriteRenderer>().material = DeadMat;
        Ball.parent.GetComponentInChildren<PunSwordRotation>().Effective = false;
        Ball.parent.GetChild(1).GetChild(0).GetComponent<Collider2D>().enabled = false;

        yield return new WaitForSeconds(SPAWN_TIME);

        Ball.GetComponent<SpriteRenderer>().material = NormalMat;
        Ball.parent.GetComponentInChildren<PunSwordRotation>().Effective = true;
        Ball.parent.GetChild(1).GetChild(0).GetComponent<Collider2D>().enabled = true;
    }

    void spawnTEST()
    {
        photonView.RPC("spawnTESTRPC", PhotonTargets.All);

    }
    [PunRPC] 
    void spawnTESTRPC()
    {
        foreach (GameObject i in GameObject.FindGameObjectsWithTag("Player"))
        {
            StartCoroutine("initialSpawnRoutine",i.transform);
        }

    }

    /// <summary>
    /// Return a random position from available spawn points
    /// </summary>
    /// <returns>Random position</returns>
    private int getUniqueLocation(List<int> usedList, int locationCount)
    {
        if (usedList.Count == locationCount)
            usedList.Clear();

        int rng = Random.Range(0, locationCount);

        while (usedList.Contains(rng) == true)
        {
            rng = Random.Range(0, locationCount);
        }

        usedList.Add(rng);

        return rng;
    }

    #endregion


    #region UI_RPC

    void sendInfo(string message)
    {
        photonView.RPC("gameInfoRPC", PhotonTargets.All, message);
    }

    [PunRPC]
    void gameInfoRPC(string message)
    {
        //Add to info pane
        //ChangeInfo.instance.ChangeText(message);
        PunInfoManager.instance.QueueInfo(message, Color.red);
    }

    #endregion
}
