﻿using UnityEngine;
using System.Collections;
using BeardedManStudios.Network;

public class InGameMenuHandler : MonoBehaviour
{
    public static InGameMenuHandler instance;

    public GameObject PausePanel,StatPanel;

    public Transform StatInfoTransform;
    void Awake()
    {
        instance = this;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            bool isOpened = PausePanel.activeSelf;

            if (isOpened == true)
            {
                PausePanel.SetActive(false);
            }
            else
            {
                PausePanel.SetActive(true);
            }

        }
        
    }

    public void OpenStat()
    {
        StatPanel.SetActive(true);
        PausePanel.SetActive(false);
    }
    public void OpenPaused()
    {
        StatPanel.SetActive(false);
        PausePanel.SetActive(true);
    }

}
