﻿using UnityEngine;
using BeardedManStudios.Network;
using UnityEngine.SceneManagement;

public class PausedMenuScript : MonoBehaviour
{

    public GameObject PausedButtonContainer;
    public GameObject SettingsButtonContainer;

    public void Button_Resume()
    {
        this.gameObject.SetActive(false);
    }
    public void Button_Respawn()
    {

    }
    public void Button_Settings()
    {

        SettingsButtonContainer.SetActive(true);
        PausedButtonContainer.SetActive(false);
    }
    public void Button_Exit()
    {
        //Networking.NetworkingReset();
        //PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        //Application.LoadLevel(0);
        SceneManager.LoadScene("MainMenu");
    }
}
