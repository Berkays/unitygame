﻿using UnityEngine;
using System.Collections;

public class InGameSettingsHandler : MonoBehaviour
{
    public GameObject ButtonContainer, PanelContainer;

    private Transform Genel, Ses;
    private GameObject PauseMainButtonContainer;

	void Start ()
    {
        Genel = PanelContainer.transform.GetChild(0);
        Ses = PanelContainer.transform.GetChild(1);

        PauseMainButtonContainer = this.transform.GetChild(0).gameObject;
	}
    public void GoBackToMainPaused()
    {
        PauseMainButtonContainer.SetActive(true);
        ButtonContainer.SetActive(false);
    }

    public void GoBack()
    {
        foreach (Transform tf in PanelContainer.transform)
        {
            tf.gameObject.SetActive(false);
        }
        PanelContainer.SetActive(false);
        ButtonContainer.SetActive(true);
    }
    private void SetState()
    {
        PanelContainer.SetActive(true);
        ButtonContainer.SetActive(false);
    }
    public void Open_Panel(int Index)
    {
        SetState();

        switch (Index)
        {
            case 0:
                Genel.gameObject.SetActive(true);
                break;
            case 1:
                Ses.gameObject.SetActive(true);
                break;
        }


    }
}
