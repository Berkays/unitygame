﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UILabel))]
public class ChangeInfo : MonoBehaviour
{
    public static ChangeInfo instance;

    UILabel label;
    public int DefaultTextDuration;

    void Start()
    {
        instance = this;
        label = this.gameObject.GetComponent<UILabel>();
    }
    [System.Obsolete("Use PunInfoManager instead.")]
    public void ChangeText(string Text)
    {
        label.text = Text;
    }
    //[System.Obsolete("Use PunInfoManager")]
    public void ChangeColor(Color clr)
    {
        label.color = clr;
    }
    //[System.Obsolete("Use PunInfoManager")]
    public void ChangeText(string Text, Color clr)
    {
        label.text = Text;
        label.color = clr;
        StopAllCoroutines();
        StartCoroutine("ShowText",DefaultTextDuration);
    }
    //[System.Obsolete("Use PunInfoManager")]
    public void ChangeText(string Text, Color clr, int Duration)
    {
        label.text = Text;
        label.color = clr;
        StopAllCoroutines();
        StartCoroutine("ShowText", Duration);
    }
    public void ClearText()
    {
        label.text = "";
    }
    private IEnumerator ShowText(int Duration)
    {
        yield return new WaitForSeconds(Duration);
        label.text = "";
    }


}