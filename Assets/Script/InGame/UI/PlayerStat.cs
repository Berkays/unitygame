﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using BeardedManStudios.Network;

namespace Player
{
    public enum Stat
    {
        Name,
        Kill,
        Death,
        Ping
    }

    public static class SetStat
    {
        public static void Set(Transform parent, Stat stat, string value)
        {
            parent.GetChild((int)stat).GetComponent<Text>().text = value;
        }

        public static void Set(Transform parent, PlayerData data)
        {
            parent.GetChild(0).GetComponent<Text>().text = data.Name;
            parent.GetChild(1).GetComponent<Text>().text = data.Kill.ToString();
            parent.GetChild(2).GetComponent<Text>().text = data.Death.ToString();
        }

        public static void NewItem(Transform parent, string name)
        {
            parent.GetChild(0).GetComponent<Text>().text = name;
            parent.GetChild(1).GetComponent<Text>().text = "0";
            parent.GetChild(2).GetComponent<Text>().text = "0";
            parent.GetChild(3).GetComponent<Text>().text = "ms";
        }
    }

    public class PlayerStat : SimpleNetworkedMonoBehavior
    {
        public static PlayerStat Instance;

        //Panel to activate
        public Transform StatPanel;
        //Prefab for each player
        public GameObject StatPrefab;

        //Player list with unique Id
        private Dictionary<ulong, PlayerData> _players = new Dictionary<ulong, PlayerData>(16);

        /// <summary>
        /// Dictionary sort by player data
        /// </summary>
        void sortStat()
        {
            _players = _players.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            for (int i = 0; i < _players.Count; i++)
            {
                //Stat_SetText.SetChildText(StatPanel.GetChild(i), Players.ElementAt(i).Value);
                SetStat.Set(StatPanel.GetChild(i), _players.ElementAt(i).Value);
            }
        }

        /// <summary>
        /// Stat panel controls
        /// </summary>
        void Update()
        {
            bool showStats = Input.GetKey(KeyCode.Tab);

            if (showStats == true && InGameMenuHandler.instance.StatPanel.activeSelf == false)
            {
                InGameMenuHandler.instance.StatPanel.SetActive(true);
            }
            if (showStats == false && InGameMenuHandler.instance.StatPanel.activeSelf == true)
            {
                InGameMenuHandler.instance.StatPanel.SetActive(false);
            }

        }

        /// <summary>
        /// Add this player to the list
        /// Ping from server to clients
        /// </summary>
        public void Init()
        {
            this.enabled = true;

            Instance = this;

            addPlayer(Networking.PrimarySocket.Me.NetworkId);

            
            //ConnectionRegister.Instance.AddClientDisconnected(RemovePlayer);

            if (Networking.PrimarySocket.IsServer)
            {
                InvokeRepeating("pingUpdate", 1f, 5);
            }
        }

        /// <summary>
        /// Ping clients
        /// </summary>
        void pingUpdate()
        {
            NetworkingManager.Instance.PollPlayerList(players =>
            {
                UnityThreadHelper.CreateThread(() =>
               {
                   foreach (var ply in players)
                   {
                       string playerIp = ply.Ip.Split('+')[0];
                       string ping = string.Empty;

                       if (playerIp == NetworkUtility.Ip || playerIp == "127.0.0.1")
                       {
                       //If Server Pass ping
                       ping = "Server";
                       }
                       else
                       {
                       //If Not Server Ping Normally
                       ping = NetworkUtility.Ping(playerIp);
                       }

                       UnityThreadHelper.Dispatcher.Dispatch(() =>
                      {
                          //updateStat(ply.NetworkId, ping, Stat.Ping);
                          updateStat(ply.NetworkId, ping, Stat.Ping);
                      });

                   }
               });

            });
        }

        #region RPC Calls

        void addPlayer(ulong id)
        {
            //RPC("_AddPlayer", NetworkReceivers.All, Networking.PrimarySocket.Me.NetworkId, PlayerPrefs.GetString("PlayerName"));
            RPC("addPlayerRPC", NetworkReceivers.All,id, PlayerPrefs.GetString("PlayerName"));
        }
        [BRPC]
        void addPlayerRPC(ulong Id, string name)
        {
            _players.Add(Id, new PlayerData(name, Id));

            createListItem(Id, name);

            sortStat();
        }
        void createListItem(ulong id, string name)
        {
            GameObject instance = GameObject.Instantiate(StatPrefab) as GameObject;
            instance.name = "Id : " + id;

            SetStat.NewItem(instance.transform, name);
            instance.transform.SetParent(InGameMenuHandler.instance.StatInfoTransform, false);
        }

        void rmeovePlayer(NetworkingPlayer player)
        {
            RPC("removePlayerRPC", NetworkReceivers.All, player.NetworkId);
        }
        [BRPC]
        private void removePlayerRPC(ulong Id)
        {
            try
            {
                //Remove Player
                _players.Remove(Id);

                //RemovePrefab
                Destroy(StatPanel.FindTransform("Id : " + Id));

                sortStat();
            }
            catch (System.NullReferenceException)
            { }
        }

        public void updateStat(ulong Id, string Value, Stat type)
        {
            RPC("updateStatRPC", NetworkReceivers.All, Id, Value, (int)type);
        }
        public void increaseKill(ulong Id)
        {
            string val = (_players[Id].Kill++).ToString();
            RPC("updateStatRPC", NetworkReceivers.All, Id, val, (int)Stat.Kill);
        }
        public void increaseDeath(ulong Id)
        {
            string val = (_players[Id].Death++).ToString();
            RPC("updateStatRPC", NetworkReceivers.All, Id, val, (int)Stat.Death);
        }
        [BRPC]
        void updateStatRPC(ulong Id, string Value, int type)
        {
            if (string.IsNullOrEmpty(Value))
                return;

            SetStat.Set(StatPanel.FindTransform("Id : " + Id), (Stat)type, Value);

            sortStat();
        }

        #endregion

    }
}