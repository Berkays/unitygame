﻿using UnityEngine;

public class AutoBounds : ScriptableObject
{
    private static float Bound_Left,Bound_Right,Bound_Top,Bound_Bottom;
    public static CameraBounds Bounds;
	// Use this for initialization
	public static void GetBounds()
    {
        Bounds = new CameraBounds();

        GameObject[] arr = GameObject.FindGameObjectsWithTag("Element");

        Bound_Left = Mathf.Infinity;
        Bound_Right = -Mathf.Infinity;
        Bound_Top = -Mathf.Infinity;
        Bound_Bottom = Mathf.Infinity;

        foreach (GameObject i in arr)
        {

            try
            {
                Bounds b = i.GetComponent<SpriteRenderer>().bounds;

                if (b.min.x < Bound_Left)
                    Bound_Left = b.max.x - 0.4f;
                if (b.max.x > Bound_Right)
                    Bound_Right = b.min.x + 0.4f;
                if (b.max.y > Bound_Top)
                    Bound_Top = b.min.y + 0.5f;
                if (b.min.y < Bound_Bottom)
                    Bound_Bottom = b.max.y - 0.4f;
            }
            catch
            {

            }
        }

        //! DEBUG
        //Debug.Log("Min X : " + Bound_Left); 
        //Debug.Log("Max X : " + Bound_Right);
        //Debug.Log("Min Y : " + Bound_Top);
        //Debug.Log("Max Y : " + Bound_Bottom);

        Bounds.Min_X = Bound_Left;
        Bounds.Min_Y = Bound_Bottom;
        Bounds.Max_X = Bound_Right;
        Bounds.Max_Y = Bound_Top;

    }

    public struct CameraBounds
    {
        public float Min_X;
        public float Max_X;
        public float Min_Y;
        public float Max_Y;
    }

}
