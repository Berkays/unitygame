﻿using UnityEngine;
using System.Collections;

public class CameraBound : Photon.MonoBehaviour
{

    Camera cam;

    [Range(1, 20)]
    public int TrackSpeed;

    public Transform target;
    public float VerticalOffset = -1.15f;

    float leftbound;
    float bottombound;
    float topbound;
    float rightbound;

    void Start()
    {
        cam = this.GetComponent<Camera>();

        float camvertical = cam.orthographicSize;
        float camhorizontal = cam.aspect * camvertical;

        leftbound = AutoBounds.Bounds.Min_X + camhorizontal;  //Sınırları Belirler Kamera görüşü - offset
        rightbound = AutoBounds.Bounds.Max_X - camhorizontal;
        topbound = AutoBounds.Bounds.Max_Y - camvertical;
        bottombound = AutoBounds.Bounds.Min_Y + camvertical;

    }

     void Update ()
     {
        
            float x = Mathf.Clamp(target.position.x, leftbound, rightbound); // Kamera posizyonunu sınırlar.
            float y = Mathf.Clamp(target.position.y, bottombound + VerticalOffset, topbound - VerticalOffset);

            if (Mathf.Abs(this.transform.position.y - target.position.y) > 1.8f)
                TrackSpeed *= 2;

            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(x, y, this.transform.position.z), Time.deltaTime * TrackSpeed);
            TrackSpeed = 5;    
     }

     
 }

