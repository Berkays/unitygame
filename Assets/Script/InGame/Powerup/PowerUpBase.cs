﻿using UnityEngine;
using System.Collections;

namespace PowerUpSystem
{
    abstract public class PowerUpBase : MonoBehaviour
    {
        protected float TimeToDestroy = 10;//Time to destroy the powerup if its not picked up
        protected float Duration; //Time Duration for the effect of PowerUp
        public System.Action OnFinish;

        /// <summary>
        /// Destroys PowerUp if it is not picked
        /// </summary>
        protected PowerUpBase()
        {
            StartCoroutine("SelfDestroy");
        }

        /// <summary>
        /// Triggers picked up boolean
        /// Enables Power Effect for N seconds
        /// Runs Routine Ienumarator Method
        /// </summary>
        public void Enable(GameObject player)
        {
            StopCoroutine("SelfDestroy");
            StartCoroutine("destroyReference");
            StartCoroutine("Routine", player);
            Invoke("OnFinishCallback", Duration);
        }

        private void OnFinishCallback()
        {
            if (OnFinish != null)
                OnFinish();
        }

        /// <summary>
        /// Abstract method to AddComponent to a gameobject
        /// Overrided in derived classes
        /// </summary>
        abstract public void AddComponent(int pIndex, int Player);

        /// <summary>
        /// Activated by enable method
        /// Base effect for powerup override in powerup to overload different powers
        /// </summary>
        /// <returns></returns>
        abstract public IEnumerator Routine(GameObject player);


        /// <summary>
        /// Routine to destroy powerup if the picked up == false
        /// </summary>
        /// <param name="Time"></param>
        /// <returns></returns>
        private IEnumerator SelfDestroy()
        {
            yield return new WaitForSeconds(TimeToDestroy);

            Destroy(this.gameObject);
        }
        private IEnumerator destroyReference()
        {
            yield return new WaitForSeconds(Duration);

            Destroy(this);
        }

        /// <summary>
        /// Returns power up name for the gui
        /// </summary>
        /// <returns></returns>
        virtual public string Name()
        {
            return "Picked a Power Up";
        }
    }

    abstract public class PowerUp_Self : PowerUpBase
    {
        protected PowerUp_Self(float T_Duration)
        {
            Duration = T_Duration;
        }
        public sealed override void AddComponent(int pIndex,int player)
        {

            //PowerUpBase object_instance = (PowerUpBase)NetworkingManager.ControllingSocket.Me.PlayerObject.gameObject.transform.GetChild(0).gameObject.AddComponent(this.GetType()); //Getcurrent
            string t = this.GetType().ToString();

            //? FORGE
            //BaseRPC.instance.UniversalRPC<string,ulong>("AddPower_Self", BeardedManStudios.Network.NetworkReceivers.All, t,id);

            //? PUN
            PunBaseRPC.instance.UniversalRPC<int,int>("AddPower_Self", PhotonTargets.All, pIndex , player);
        }
    }
    abstract public class PowerUp_Enemy : PowerUpBase
    {
        protected PowerUp_Enemy(float T_Duration)
        {
            Duration = T_Duration;
        }
        public sealed override void AddComponent(int pIndex, int player)
        {
            string t = this.GetType().ToString();

            //? Forge
            //BaseRPC.instance.UniversalRPC<string,ulong>("AddPower_Enemy", BeardedManStudios.Network.NetworkReceivers.All, t,id);
            
            //? PUN
            PunBaseRPC.instance.UniversalRPC<int,int>("AddPower_Enemy", PhotonTargets.All, pIndex,player);

        }
    }

}