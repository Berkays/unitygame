﻿using UnityEngine;
using System.IO;
using System.Reflection;
using System.Linq;
using BeardedManStudios.Network;

namespace PowerUpSystem
{
    public class PowerUpManager : SimpleNetworkedMonoBehavior
    {
        #region VARIABLES
        public GameObject Prefab; //PowerUp Prefab

        [Range(0, 1)]
        public float AreaTolerance = 0.2f; //Area Coliison Detection Offset
        public float SpawnFrequency = 5; //Spawn frequency
        public float StartDelay; //Spawn Start Time

        System.Type[] powerups; //Holds PowerUp Types
        uint counter = 0;
        #endregion

        public void getPowers()
        {
            //powerups = new System.Type[1] { typeof(PowerUps.IncreaseMovementSpeed)};
            powerups = System.Reflection.Assembly.GetAssembly(typeof(PowerUpSystem.PowerUpBase)).GetTypes().Where(x => x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Self)) || x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Enemy))).ToArray();
        }

        // Use this for initialization
        public void startSpawn()
        {
            //Plugin.Load();

            if (!(Networking.PrimarySocket.IsServer))
                return;

            InvokeRepeating("spawnPower", StartDelay, SpawnFrequency);
        }

        void spawnPower()
        {
            Vector2 spawnlocation = generateRandomLocation();
            int rng = Random.Range(0, powerups.Length - 1);

            RPC("instantiateRPC", NetworkReceivers.AllBuffered, spawnlocation, rng, counter);
            counter++;
        }
        [BRPC]
        void instantiateRPC(Vector2 pos, int index, uint Counter)
        {
            GameObject cube = Instantiate(Prefab, pos, Prefab.transform.rotation) as GameObject;
            cube.AddComponent(powerups[index]);
            cube.name = Counter.ToString();

            ChangeInfo.instance.ChangeText("Powerup Spawned", Color.yellow);
        }

        Vector2 generateRandomLocation()
        {
            float min_x = AutoBounds.Bounds.Min_X;
            float min_y = AutoBounds.Bounds.Min_Y;
            float max_x = AutoBounds.Bounds.Max_X;
            float max_y = AutoBounds.Bounds.Max_Y;

            float pos_x;
            float pos_y;

            int ExceptionBreak = 0;

            do
            {
                pos_x = Random.Range(min_x, max_x);
                pos_y = Random.Range(min_y, max_y);
                ExceptionBreak++;
                if (ExceptionBreak > 500)
                {
                    Debug.LogError("No Options Available");
                    Debug.Break();
                    throw new System.Exception();
                }

            }
            while (Physics2D.OverlapArea(new Vector2(pos_x - AreaTolerance, pos_y + AreaTolerance), new Vector2(pos_x + AreaTolerance, pos_y - AreaTolerance)) == true);

            return new Vector2(pos_x, pos_y);

        }

    }

    public static class Plugin
    {
        public static void Load()
        {
            try
            {
                string path = Application.dataPath + "/Plugins/PowerUps/";

                string[] files = Directory.GetFiles(path, "*.dll");
                foreach (string file in files)
                {
                    Assembly asm = Assembly.LoadFrom(file);
                    var Powers = asm.GetTypes().Where(x => x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Self)) || x.IsSubclassOf(typeof(PowerUpSystem.PowerUp_Enemy))).ToArray();
                    foreach (System.Type t in Powers)
                    {
                        Debug.Log(t.Name);
                        //PowerUpSystem.PowerUpBase baseclass = (PowerUpSystem.PowerUpBase)System.Activator.CreateInstance(t);
                        //Debug.Log(baseclass.GetType());
                        //Debug.Log(baseclass.PowerUpName());
                        //baseclass.enable();
                    }



                }
            }
            catch { }
        }
    }
}