﻿using UnityEngine;
using System.Collections;

namespace PowerUpSystem.PowerUps
{
    public sealed class FreezeEnemy : PowerUp_Enemy
    {
        public FreezeEnemy() : base(4)
        {
            //this.Duration = 3;
        }

        public override IEnumerator Routine(GameObject player)
        {
            #region Components
            PunSwordRotation enemy_SwordRotationComponent = player.transform.parent.GetComponentInChildren<PunSwordRotation>(); // Get Enemy GameObject
                                                                                                                                   //TODO STAY ON AIR OR FALL
                                                                                                                                   //Rigidbody2D enemy_Rigidbody = GameObject.Find("Ball").GetComponent<Rigidbody2D>();
            PunController enemy_Controller = player.GetComponent<PunController>();
            #endregion

            #region FREEZING
            enemy_SwordRotationComponent.Effective = false;

            float initial_value = enemy_Controller.MovementMultiplier;
            enemy_Controller.MovementMultiplier = 0;

            enemy_Controller.isBoostUsed = true;
            enemy_Controller.slider.alpha = 0.5f;
            #endregion FREEZING
            #region SWORD FIX

            float initial_speed = enemy_SwordRotationComponent.SwordRotateSpeed; //Store initial speed so we can restore it later
            enemy_SwordRotationComponent.SwordRotateSpeed = 0; //Set Sword Speed TO 0
            enemy_SwordRotationComponent.slider.alpha = 0.5f; //Slider Transparency
            enemy_SwordRotationComponent.isRotated = true; //Prevents from Wasting RotationPower
            #endregion

            yield return new WaitForSeconds(this.Duration);

            #region FREEZING
            enemy_SwordRotationComponent.Effective = false;

            enemy_Controller.MovementMultiplier = initial_value;

            enemy_Controller.isBoostUsed = false;
            enemy_Controller.slider.alpha = 1;
            #endregion FREEZING
            #region SWORD FIX
            enemy_SwordRotationComponent.isRotated = false;
            enemy_SwordRotationComponent.SwordRotateSpeed = initial_speed;
            enemy_SwordRotationComponent.slider.alpha = 1f;


            #endregion

            Destroy(this);
        }

        public override string Name()
        {
            return "Düşman Donduruldu";
        }

    }

    public sealed class IncreaseSwordSpeed : PowerUp_Self
    {
        public IncreaseSwordSpeed() : base(5)
        {

        }

        public override IEnumerator Routine(GameObject player)
        {

            PunSwordRotation player_Component = player.transform.parent.GetComponentInChildren<PunSwordRotation>();

            player_Component.SwordRotateSpeed *= 2;

            yield return new WaitForSeconds(this.Duration);

            player_Component.SwordRotateSpeed /= 2;



            Destroy(this);

        }
        public override string Name()
        {
            return "Kılıcın Dönmesi Hızlandırıldı";
        }
    }

    public sealed class DualSword : PowerUp_Self
    {
        public DualSword() : base(5)
        {
            //Duration = 5;
        }

        public override IEnumerator Routine(GameObject player)
        {
            yield return null;
            Destroy(this);
        }
        public override string Name()
        {
            return "İkinci Kılıç Eklendi  (EKLENMEDİ)";
        }
    }

    public sealed class IncreaseMovementSpeed : PowerUp_Self
    {
        public IncreaseMovementSpeed() : base(7)
        {
            //Duration = 7;
        }

        public override IEnumerator Routine(GameObject player)
        {
            //Increase Movement Speed for Duration Seconds...
            player.GetComponent<PunController>().MovementMultiplier = 1.5f;
            yield return new WaitForSeconds(this.Duration);
            player.GetComponent<PunController>().MovementMultiplier = 1;

        }
        public override string Name()
        {
            return "Hareket Hızı Arttırıldı";
        }
    }

    public sealed class Shield : PowerUp_Self
    {
        public Shield() : base(1)
        {
            //Duration = 1;
            //this.Self = true;

        }

        public override IEnumerator Routine(GameObject player)
        {

            yield return new WaitForSeconds(this.Duration);
            Destroy(this);
        }
        public override string Name()
        {
            return "Kalkan Aktifleştirildi  (EKLENMEDİ)";
        }
    }

    public sealed class FixEnemySword : PowerUp_Enemy
    {
        public FixEnemySword() : base(5)
        {
            //Duration = 5;
            //this.Self = false;
        }

        public override IEnumerator Routine(GameObject player)
        {

            PunSwordRotation enemy_Component = player.transform.parent.GetComponentInChildren<PunSwordRotation>(); // SWORD COMPONENT

            #region PowerStart
            float initial_speed = enemy_Component.SwordRotateSpeed; //Store initial speed so we can restore it later
            enemy_Component.SwordRotateSpeed = 0; //Set Sword Speed TO 0
            enemy_Component.slider.alpha = 0.5f; //Slider Transparency
            enemy_Component.isRotated = true; //Prevents from Wasting RotationPower
            #endregion

            yield return new WaitForSeconds(this.Duration);

            #region PowerEnd
            enemy_Component.isRotated = false;
            enemy_Component.SwordRotateSpeed = initial_speed;
            enemy_Component.slider.alpha = 1f;
            #endregion

            Destroy(this);
        }
        public override string Name()
        {
            return "Düşman Kılıcı Sabitlendi";
        }
    }

    public sealed class DestroyEnemySword : PowerUp_Enemy
    {

        public DestroyEnemySword() : base(5)
        {
            //Duration = 5;
        }

        public override IEnumerator Routine(GameObject player)
        {
            PunSwordRotation enemy_Component = player.transform.parent.GetComponentInChildren<PunSwordRotation>();

            #region PowerStart
            enemy_Component.DisableEnableCollider();
            enemy_Component.Effective = false;
            enemy_Component.slider.alpha = 0.5f;
            enemy_Component.isRotated = true;
            #endregion

            yield return new WaitForSeconds(this.Duration);

            #region PowerEnd
            enemy_Component.DisableEnableCollider();
            enemy_Component.Effective = true;
            enemy_Component.slider.alpha = 1;
            enemy_Component.isRotated = false;
            #endregion

            Destroy(this);
        }

        public override string Name()
        {
            return "Rakibin Kılıcı Etkisiz Hale Geldi";
        }
    }

}