﻿using UnityEngine;
using System.Collections;

public class OffscreenRenderer : MonoBehaviour
{
    public float OffsetX_Axis;
    public float OffsetY_Axis;

    public GameObject DebugObject;
    public GameObject TobeTracked;
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Vector3 position = GameObject.Find("PowerUP").transform.position;
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        position = TobeTracked.transform.position;
        if (isOffScreen(position) == true)
        {
            
            //! Object Offscreen
            //Show Indicator
            //this.transform.GetChild(0).gameObject.SetActive(true);
            //this.transform.GetChild(0).position = CalculateIndicatorPosition(position);
            //this.transform.GetChild(0).localRotation = Quaternion.Euler(0, 0, Rotation(position));
            DebugObject.GetComponent<Renderer>().enabled = true;
            DebugObject.transform.position = CalculateIndicatorPosition(position);
        }
        else
        {
            //this.transform.GetChild(0).gameObject.SetActive(false);
            DebugObject.GetComponent<Renderer>().enabled = false;
        }
        
	}
    Vector3 NormalizeToCenter(Vector3 position)
    {
        position = new Vector3(position.x - Screen.width / 2, position.y - Screen.height / 2, position.z);
        return position;
    }
    bool isOffScreen(Vector3 position)
    {
        bool OffScreen = false;
        Vector3 bounds_RT = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        Vector3 bounds_LB = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        if (position.x > bounds_RT.x || position.y > bounds_RT.y || position.x < bounds_LB.x || position.y < bounds_LB.y)
        {
            OffScreen = true;
        }
        return OffScreen;
        
    }
    float Rotation(Vector2 position)
    {
        float rotation = Mathf.Atan2(position.y,position.x);
        rotation = Mathf.Rad2Deg * rotation;
        return rotation;
    }
    Vector3 CalculateIndicatorPosition(Vector3 position)
    {
        float x = 0;
        float y = 0;
        
        
        Vector3 bounds_RT = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        Vector3 bounds_LB = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        float slope = position.y / position.x;
        if (position.x > bounds_RT.x)
        {
            //Right Side
            x = bounds_RT.x - OffsetX_Axis;
            y = slope * bounds_RT.x;
        }
        else if (position.x < bounds_LB.x)
        {
            x = bounds_LB.x + OffsetX_Axis;
            y = slope * bounds_LB.x;
            
        }

        if (position.y > bounds_RT.y)
        {
            y = bounds_RT.y - OffsetY_Axis;
            
            x = bounds_RT.y / slope;
            
        }
        else if (position.y < bounds_LB.y)
        {
            y = bounds_LB.y + 0.3f + OffsetY_Axis;
            x = bounds_LB.y / slope;
        }

        //if (position.x > bounds_RT.x && position.y > bounds_RT.y)
        //{
        //    //! Top Rigth
        //    x = bounds_RT.x - OffsetX_Axis;
        //    y = bounds_RT.y - OffsetY_Axis;

        //}
        //else if (position.x < bounds_LB.x && position.y > bounds_RT.y)
        //{
        //    //! TOP LEFT
        //    x = bounds_LB.x + OffsetX_Axis;
        //    y = bounds_RT.y - OffsetY_Axis;
        //}
        //else if (position.x > bounds_RT.x && position.y < bounds_LB.y)
        //{
        //    x = bounds_RT.x - OffsetX_Axis;
        //    y = bounds_LB.y + 0.3f + OffsetY_Axis;
        //}
        //else if (position.x < bounds_LB.x && position.y < bounds_LB.y)
        //{
        //    x = bounds_LB.x + OffsetX_Axis;
        //    y = bounds_LB.y + 0.3f + OffsetY_Axis;
        //}
        Debug.Log("X : " + x + " Y : "+y);
        
        return new Vector3(x,y, position.z);




    }
}
