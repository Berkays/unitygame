﻿using BeardedManStudios.Network;
using PowerUpSystem;
using System.Collections;
using UnityEngine;

public class PickUpPowerUp : SimpleNetworkedMonoBehavior
{

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (!IsOwner)
            return;

        if (collider.tag == "PowerUp")
        {
            if (this.gameObject.GetComponent(collider.GetComponent<PowerUpBase>().GetType()) == null)
            {

                PowerUpBase power = collider.gameObject.GetComponent<PowerUpBase>();
                //power.AddComponent(Networking.PrimarySocket.Me.NetworkId);

                
                ChangeInfo.instance.ChangeText(power.Name(), new Color(0, 0.8f, 1f, 1)); //Static
            }


            //ulong id = collider.gameObject.GetComponent<SimpleNetworkedMonoBehavior>().NetworkedId;
            //ushort name = ushort.Parse(collider.gameObject.name);
            //RPC("DestroyObjectRPC",name);

            PhotonNetwork.Destroy(collider.gameObject.GetComponent<PhotonView>());
            //Networking.Destroy(collider.gameObject.GetComponent<SimpleNetworkedMonoBehavior>());
        }
    }

    [BRPC]
    private void DestroyObjectRPC(ushort name)
    {
        GameObject.Destroy(GameObject.Find(name.ToString()));
    }
}
