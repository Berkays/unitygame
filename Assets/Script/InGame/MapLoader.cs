﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

using LevelSerializerProtocol;

public class MapLoader : ScriptableObject
{
    public static string LoadedName, LoadedAuthor, LoadedDescription;

    public static Action OnLoaded;

    const string Element = "Base0";
    const string Spawn = "Base1";
    const string Elements = "Elements";
    const string Spawns = "SpawnLocations";

    public static void LoadMap(string Map)
    {
        Level lvl = LevelDeserializer.Deserialize(Map);

        if (lvl == null)
        {
            return; // File Invalid
        }

        CleanUp();

        #region RetrieveResources
        List<byte[]> ResourceList = new List<byte[]>();

        foreach (GameObjectData data in lvl.Objects)
        {
            if (data.ObjectType == ObjectType.Element)
            {
                byte[] image = data.Resource;
                if (image.Length == 1)
                {
                    continue;
                }
                ResourceList.Add(image);
            }
        }
        #endregion
        #region Populate GameObjects

        GameObject baseObject = GameObject.Find(Element);
        Transform ElementContainer = GameObject.Find(Elements).transform;

        foreach (GameObjectData data in lvl.Objects.Where(x => x.ObjectType == ObjectType.Element))
        {

            GameObject instance = Instantiate(baseObject) as GameObject;

            instance.transform.SetParent(ElementContainer, false);
            instance.transform.eulerAngles = new Vector3(data.position.rot_x, data.position.rot_y, data.position.rot_z);
            instance.transform.localScale = new Vector3(data.position.size_x, data.position.size_y, 0);
            instance.transform.position = new Vector3(data.position.pos_x, data.position.pos_y, 0);

            Texture2D tex = ResourceList[data.ResourceName].ByteToTexture();
            instance.GetComponent<SpriteRenderer>().sprite = tex.CreateSprite(); //Texture Extension
            instance.AddMissingComponent<BoxCollider2D>();
        }
        #endregion
        #region Populate SpawnPoints
        GameObject SpawnObject = GameObject.Find(Spawn); //? Spawn Object Empty Gameobject Will do fine
        Transform SpawnContainer = GameObject.Find(Spawns).transform;

        foreach (GameObjectData data in lvl.Objects.Where(x => x.ObjectType == ObjectType.SpawnPoint))
        {
            GameObject instance = Instantiate(SpawnObject) as GameObject;

            instance.transform.SetParent(SpawnContainer, false);
            instance.transform.position = new Vector3(data.position.pos_x, data.position.pos_y, 0);
        }
        #endregion


        DestroyImmediate(GameObject.Find(Element));
        DestroyImmediate(GameObject.Find(Spawn));

        LoadedName = lvl.Name;
        LoadedAuthor = lvl.Author;
        LoadedDescription = lvl.Description;

        AutoBounds.GetBounds();

        if (OnLoaded != null)
            OnLoaded();
    }

    private static void CleanUp()
    {
        Transform List = GameObject.Find(Elements).transform; ; 

        foreach (Transform i in List)
        {
            Destroy(i.gameObject);
        }

        List = GameObject.Find(Spawns).transform; 

        foreach (Transform i in List)
        {
            Destroy(i.gameObject);
        }
        
    }
}
