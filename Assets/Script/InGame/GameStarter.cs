﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using BeardedManStudios.Network;

public class GameStarter : SimpleNetworkedMonoBehavior
{
    //Ready to join
    bool isReady = false;
    bool isServerReady = false;

    //To be activated
    public GameObject MainCamera;
    public PowerUpSystem.PowerUpManager powerupScript;
    public Player.PlayerSpawner spawnerScript;
    public Player.PlayerStat statScript;
    public ConnectionRegister connectionScript;

    //UI Elements
    public UIPanel NGUI_A;
    public GameObject PlayerPrefab;
    public Button JoinButton;

    // Use this for initialization
    void Start()
    {
        Invoke("Init", 0.8f);
    }

    void Init()
    {
        string Name = PlayerPrefs.GetString("PlayerName");

        NetworkingManager.Instance.SetName(Name);

        setUi();

        //Send player updates to clients
        if (Networking.PrimarySocket.IsServer)
            InvokeRepeating("sendPlayerData", 0.2f, 5);

        #region Map Loading
        //When Map Load Finishes
        Action OnMapLoad = null;
        OnMapLoad = () =>
            {
                //Activate Join Button
                JoinButton.interactable = true;
                JoinButton.transform.GetComponent<Text>().text = "Join Game";

                //Unsubscribe event to prevent memory leak
                MapLoader.OnLoaded -= OnMapLoad;

                //TODO READY SYSTEM
                isReady = true;
            };

        MapLoader.OnLoaded += OnMapLoad;

        //Start Loading Map
        MapLoader.LoadMap(ServerSync.serverData.Map_Name);
        #endregion
    }

    void setUi()
    {
        Text serverInfo = this.transform.GetChild(0).GetComponent<Text>(); //Title Text Component

        //Server Info
        // Name - Ip
        // PlayerCount
        System.Text.StringBuilder sBuilder = new System.Text.StringBuilder(42);
        sBuilder.Append(ServerSync.serverData.Server_Name);
        sBuilder.Append(" - ");
        sBuilder.Append(ServerSync.serverData.Server_Ip);
        sBuilder.Append("\n(");
        sBuilder.Append(ServerSync.serverData.PlayerCount.ToString());
        sBuilder.Append("/");
        sBuilder.Append(ServerSync.serverData.MaxPlayerCount.ToString());
        sBuilder.Append(")");
        serverInfo.text = sBuilder.ToString();
    }

    #region Player List

    //InvokeRepeating in Server Sends PLayer Data
    void sendPlayerData()
    {
        NetworkingManager.Instance.PollPlayerList(getPlayers);
    }

    //Format players into string then send to clientss
    private void getPlayers(List<NetworkingPlayer> Players)
    {
        UnityThreadHelper.CreateThread(() =>
       {

           var sBuilder = new System.Text.StringBuilder(Players.Count);
           foreach (var i in Players)
           {
               string ip = i.Ip.Split('+')[0];

               sBuilder.Append(i.Name);
               sBuilder.Append(',');

               sBuilder.Append(ip);
               sBuilder.Append(',');

               #region .NET Socket

               //try
               //{


               //    System.Net.NetworkInformation.ThreadedPing p = new ThreadedPing();

               //    var reply = p.Send(ip, 1000);

               //    string ping = reply.RoundtripTime.ToString();

               //    sBuilder.Append(ping);
               //    sBuilder.Append(" ms");
               //}
               //catch (Exception)
               //{
               //    sBuilder.Append("client");
               //}

               #endregion


               sBuilder.Append(NetworkUtility.Ping(ip));

               sBuilder.Append("\n");
           }


           string plyData = sBuilder.ToString().TrimEnd('\n');


           UnityThreadHelper.Dispatcher.Dispatch(() =>
          {
              RPC("createPlayers", NetworkReceivers.All, plyData);
          });
       });

    }

    [BRPC]
    private void createPlayers(string playerList)
    {
        //Player List Transform
        foreach (Transform i in this.transform.GetChild(2))
            Destroy(i.gameObject);

        foreach (var str in playerList.Split('\n'))
        {
            createPrefab(str.Split(','));
        }
    }
    private void createPrefab(string[] player)
    {
        //Player List Transform
        Transform parent = this.transform.GetChild(2);

        GameObject instance = Instantiate(PlayerPrefab) as GameObject;
        instance.transform.SetParent(parent, false);

        Text label = instance.GetComponent<Text>();

        //[0] NAME
        //[1] IP
        //[2] PING
        string playerName = player[0];
        string playerIp = player[1];
        string playerPing = player[2];


        const string localhost = "127.0.0.1";
        string myIp = NetworkUtility.Ip;

        if (playerIp == localhost || playerIp == ServerSync.serverData.Server_Ip) //If Server
        {
            label.text = playerName + "-" + ServerSync.serverData.Server_Ip + " - " + "SERVER";

            if (myIp == ServerSync.serverData.Server_Ip) //If player is server and current player
            {
                label.color = Color.green;
            }
            else
            {
                label.color = Color.blue; //Color - Light Blue
            }
        }
        else //If Not Server Mark Blue
        {

            if (myIp == playerIp) //If the player is current player
                label.color = Color.green;

            label.text = playerName + "-" + playerIp + " - " + playerPing;
        }
    }

    #endregion

    //Join Game Button
    public void joinGame()
    {
        setupGame();
    }
    private void setupGame()
    {
        //Stop Getting Update From the server
        CancelInvoke("sendPlayerData");

        //Show Dock
        NGUI_A.alpha = 1;

        //Enable Camera
        MainCamera.SetActive(true);

        //Player Spawn
        spawnerScript.Init();

        //PowerUp Spawn
        powerupScript.getPowers();
        powerupScript.startSpawn();

        //Stat Script
        statScript.Init();

        //Disable this Panel
        this.gameObject.SetActive(false);
    }

    //Exit Game Button
    public void exitGame()
    {
        //TODO RPC UPDATE SERVER

        Networking.Disconnect();
        SceneManager.LoadScene("MainMenu");
    }
}
