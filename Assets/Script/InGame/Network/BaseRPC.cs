﻿using UnityEngine;
using System.Collections;
using System.Linq;
using BeardedManStudios.Network;
using System.Collections.Generic;

public class BaseRPC : SimpleNetworkedMonoBehavior
{
    public static BaseRPC instance;

    void Start()
    {
        instance = this;
    }

    public void UniversalRPC(string Method, NetworkReceivers Receivers)
    {
        RPC(Method, Receivers);
    }
    public void UniversalRPC<T>(string Method, NetworkReceivers Receivers, T obj)
    {
        RPC(Method, Receivers, obj);
    }
    public void UniversalRPC<T, K>(string Method, NetworkReceivers Receivers, T obj, K _obj)
    {
        RPC(Method, Receivers, obj, _obj);
    }
    public void UniversalRPC<T, K, J>(string Method, NetworkReceivers Receivers, T obj, K _obj, J _t)
    {
        RPC(Method, Receivers, obj, _obj, _t);
    }

    [BRPC]
    private void AddPower_Enemy(string name, ulong id)
    {
        System.Type type = System.Type.GetType(name);

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (obj.GetComponent<Player.Controller>().OwnerId != id)
            {
                var Comp = obj.AddComponent(type) as PowerUpSystem.PowerUpBase;
                //Comp.Enable();
            }
        }

    }

    [BRPC]
    private void AddPower_Self(string name, ulong id)
    {
        System.Type type = System.Type.GetType(name);

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (obj.GetComponent<Player.Controller>().OwnerId == id)
            {
                var Comp = obj.AddComponent(type) as PowerUpSystem.PowerUpBase;
                //Comp.Enable();

                break;

            }
        }
    }

    [BRPC]
    public void DestroyObject(string name)
    {
        GameObject.Destroy(GameObject.Find(name));
    }





}
