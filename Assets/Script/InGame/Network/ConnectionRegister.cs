﻿using UnityEngine;
using BeardedManStudios.Network;

public class ConnectionRegister : SimpleNetworkedMonoBehavior
{
    public static ConnectionRegister Instance;

    public event NetWorker.PlayerConnectionEvent PlayerConnected;
    public event NetWorker.PlayerConnectionEvent PlayerDisconnected;

    void Start()
    {
        Instance = this;
    }

    public void AddClientConnected(NetWorker.PlayerConnectionEvent someMethod)
    {
        if (!Networking.PrimarySocket.IsServer)
            return;

        Networking.PrimarySocket.playerConnected += someMethod;
    }
    public void AddClientDisconnected(NetWorker.PlayerConnectionEvent someMethod)
    {
        if (!Networking.PrimarySocket.IsServer)
            return;

        Networking.PrimarySocket.playerDisconnected += someMethod;
    }


}
