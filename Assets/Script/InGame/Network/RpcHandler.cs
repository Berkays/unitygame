﻿using UnityEngine.SceneManagement;
using BeardedManStudios.Network;

public class RpcHandler : SimpleNetworkedMonoBehavior
{
    public static RpcHandler instance;

    void Start()
    {
        instance = this;
    }

    public void UniversalRPC(string Method, NetworkReceivers Receivers)
    {
        RPC(Method, Receivers);
    }
    public void UniversalRPC<T>(string Method, NetworkReceivers Receivers, T obj)
    {
        RPC(Method, Receivers, obj);
    }
    public void UniversalRPC<T, K>(string Method, NetworkReceivers Receivers, T obj, K _obj)
    {
        RPC(Method, Receivers, obj, _obj);
    }
    public void UniversalRPC<T, K, J>(string Method, NetworkReceivers Receivers, T obj, K _obj, J _t)
    {
        RPC(Method, Receivers, obj, _obj, _t);
    }



    [BRPC]
    private void LoadLevel()
    {
        //Application.LoadLevel("PlayMap");
        SceneManager.LoadScene("PlayMap");
    }


    public void TCPRequest(NetworkingPlayer ply)
    {
        AuthoritativeRPC("TCPReply", Networking.PrimarySocket, ply, false, NetworkUtility.Ip);
    }

    [BRPC]
    void TCPReply(string Ip)
    {
        NetworkUtility.TCP.OnReceived = () =>
        {
            RPC("InformServerRPC", NetworkReceivers.Server);
        };
        ServerSync.Receive(NetworkUtility.TCP.TCP_Receive(Ip));
    }
    [BRPC]
    void InformServerRPC()
    {
        ServerSync.Transfered = true;
    }

}
