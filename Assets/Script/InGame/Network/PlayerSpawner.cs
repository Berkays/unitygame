﻿using UnityEngine;
using BeardedManStudios.Network;

namespace Player
{

    public class PlayerSpawner : SimpleNetworkedMonoBehavior
    {
        public GameObject objectToSpawn = null; //Player Prefab

        const string SPAWN_TRANSFORM = "SpawnLocations";
        private Transform SpawnLocations;

        public void Init()
        {
            SpawnLocations = GameObject.Find(SPAWN_TRANSFORM).transform;

            if (Networking.PrimarySocket.Connected)
                spawnPlayer();
            else
                Networking.PrimarySocket.connected += spawnPlayer;
        }

        void spawnPlayer()
        {
            Vector2 pos = getSpawnPoint();
            Networking.Instantiate(objectToSpawn, pos, Quaternion.identity, NetworkReceivers.AllBuffered);
            RPC("changeTextRPC", NetworkReceivers.Others, PlayerPrefs.GetString("PlayerName"));
        }

        /// <summary>
        /// Return a random position from available spawn points
        /// </summary>
        /// <returns>Random position</returns>
        Vector2 getSpawnPoint()
        {
            int rng = Random.Range(0, SpawnLocations.childCount);

            return SpawnLocations.GetChild(rng).transform.position;
        }

        /// <summary>
        /// Inform other players with RPC
        /// </summary>
        /// <param name="playerName"></param>
        [BRPC]
        void changeTextRPC(string playerName)
        {
            ChangeInfo.instance.ChangeText(playerName + " Joined The Game", Color.yellow);
        }
    }

}