﻿using UnityEngine;
using System.Collections;

namespace Player
{
    public class Spawn : MonoBehaviour
    {

        Vector3 Position; // Random Generated;
        [Range(1, 30)]
        public int SpawnTime;

        void Start()
        {
            Position = this.gameObject.transform.position;
        }
        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.name == "Colliders")
            {
                StartCoroutine("Death");

                Position = GenerateRandomPosition();
            }
        }

        IEnumerator Death()
        {
            int respawntime = SpawnTime;
            ChangeInfo.instance.ChangeColor(Color.red);

            while (respawntime > -1)
            {

                ChangeInfo.instance.ChangeText("Öldün!\nYeniden Dirilmeye " + respawntime.ToString() + " Saniye");
                yield return new WaitForSeconds(1);
                respawntime--;

            }

            this.gameObject.transform.position = Position;
            ChangeInfo.instance.ClearText();
        }
        Vector3 GenerateRandomPosition()
        {
            return new Vector3(0, 0, 0);
        }
    }
}