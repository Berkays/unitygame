﻿using System;

namespace Player
{
    public class PlayerData : IComparable<PlayerData>
    {
        //Events to trigger when a value changes
        public event Action<Stat> ValueChange;
        void OnValueChanged(Stat spc)
        {
            if (ValueChange != null)
            {
                ValueChange(spc);
            }
        }

        private Values values;

        /// <summary>
        /// Properties that invoke ValueChange event
        /// </summary>
        public string Name
        {
            get { return values.Name; }
            set
            {
                OnValueChanged(Stat.Name);
                values.Name = value;
            }
        }
        public ulong Kill
        {
            get { return values.Kill; }
            set
            {
                OnValueChanged(Stat.Kill);
                values.Kill = value;
            }
        }
        public ulong Death
        {
            get { return values.Death; }
            set
            {
                OnValueChanged(Stat.Death);
                values.Death = value;
            }
        }
        public ulong Id
        {
            get { return values.Id; }
            set
            {
                OnValueChanged(Stat.Ping);
                values.Id = value;
            }
        }

        //Default Constructor
        public PlayerData(string name, ulong id)
        {
            values.Name = name;
            values.Id = id;
            values.Kill = 0;
            values.Death = 0;
        }

        //Data
        private struct Values
        {
            public string Name;
            public ulong Kill;
            public ulong Death;
            public ulong Id;
        }

        public int CompareTo(PlayerData other)
        {
            if (this.Kill > other.Kill)
                return 1;
            else if (this.Kill == other.Kill)
                return 0;
            else
                return -1;
        }
    }
}
