﻿using UnityEngine;
using System.Collections;
using BeardedManStudios.Network;

namespace Player
{
    public class SwordRotation : SimpleNetworkedMonoBehavior
    {

        public Material Mat_Normal, Mat_Transparent;

        private SpriteRenderer _TSprite;
        [HideInInspector]
        public UISlider slider; //RotationSliderUI
        [HideInInspector]
        public bool Effective = true;
        [HideInInspector]
        public bool isRotated = false;

        #region Customizable
        public float ManualRotationCooldown = 5;
        public float SwordRotateSpeed = 400;
        #endregion


        void Start()
        {
            _TSprite = this.GetComponent<SpriteRenderer>();
            //Index = this.GetComponentInParent<PlayerWrapper>().stats.Index;
            slider = GameObject.Find("RotationProgress").GetComponent<UISlider>();

            //Rotate Randomly
            int rnd = Random.Range(0, 360);
            this.transform.parent.Rotate(Vector3.back,rnd, Space.Self);
        }
        void Update()
        {
            #region HAREKET
            //this.transform.position = _BallTransform.position; //MoveSword
            //this.transform.Rotate(Vector3.back,SwordRotateSpeed * Time.deltaTime,Space.Self);
            this.transform.parent.Rotate(Vector3.back, SwordRotateSpeed * Time.deltaTime, Space.Self);
            #endregion

            if (!IsOwner)
                return;

            #region GIRIS
            if (Input.GetMouseButtonDown(1) == true && isRotated == false)
            {
                SwordRotateSpeed *= -1; // Reverse Sword
                StartCoroutine("RotationCo"); //Corotine
                StartCoroutine("RotationSliderCo"); //ProgressBar         
            }
            #endregion
            

        }

        void OnTriggerEnter2D(Collider2D col)
        {
           
            int CollidedObjectLayer = col.gameObject.layer;
            
            if (CollidedObjectLayer == LayerMask.NameToLayer("Elements"))
            {
                ChangeTransparency(true);
                Effective = false;
            }
            else if (CollidedObjectLayer == LayerMask.NameToLayer("Sword") && Effective == true)
            {
                SwordRotateSpeed *= -1;
            }

        }

        void OnTriggerExit2D(Collider2D col)
        {

            if (col.gameObject.layer == 9) // 9 Element
            {
                ChangeTransparency(false);
                Effective = true;
            }
        }

        public void ChangeTransparency(bool isTransparent)
        {
            if (isTransparent == true)
            {
                _TSprite.material = Mat_Transparent;
            }
            else
            {
                _TSprite.material = Mat_Normal;
            }
        }
        public void DisableEnableCollider() //Used by powerup system
        {
            BoxCollider2D component = this.GetComponent<BoxCollider2D>();
            if (component.enabled == true)
            {
                component.enabled = false;
            }
            else
            {
                component.enabled = true;
            }
        }

        #region COROUTINES

        IEnumerator RotationCo()
        {
            isRotated = true;
            yield return new WaitForSeconds(ManualRotationCooldown);
            isRotated = false;
        }
        IEnumerator RotationSliderCo()
        {
            slider.value = 0;
            slider.alpha = 0.5f;
            float rate = 1 / ManualRotationCooldown;
            float i = 0;
            while (i < 1)
            {
                i += Time.deltaTime * rate;
                slider.value = i;
                yield return null;
            }
            slider.alpha = 1;
        }
        #endregion
    }
}