﻿using UnityEngine;
using System.Linq;
using BeardedManStudios.Network;
using Player;

public class SwordCollision : SimpleNetworkedMonoBehavior
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (!IsOwner)
            return;

        int CollidedObjectLayer = col.gameObject.layer;


        if (CollidedObjectLayer == LayerMask.NameToLayer("Player"))
        {
            if (col.gameObject != transform.parent.parent.GetChild(0).gameObject) //Check 
            {
                Debug.Log("I died!");


                var enemyComponent = col.GetComponent<SimpleNetworkedMonoBehavior>().OwningNetWorker.Me;

                string Name = enemyComponent.Name;
                ChangeInfo.instance.ChangeText("You Killed " + Name,Color.green);

                
                PlayerStat.Instance.increaseKill(Networking.PrimarySocket.Me.NetworkId);
                //PlayerStat.Instance.increaseDeath(enemyComponent.NetworkId);
                //Play Particle Effect
                //Play Sound
                //Set Spawn
                //Destroy(col.gameObject); // Move Object For Pooling , Remove PowerU ps
            }
        }
    }
}
