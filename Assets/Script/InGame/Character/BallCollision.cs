﻿using BeardedManStudios.Network;
using UnityEngine;

namespace Player
{
    public class BallCollision : SimpleNetworkedMonoBehavior
    {

        void OnTriggerEnter2D(Collider2D col)
        {
            if (!IsOwner)
                return;


            int CollidedObjectLayer = col.gameObject.layer;

            if (CollidedObjectLayer == LayerMask.NameToLayer("Sword")) //Layer : 10 = Sword
            {
                if (col.gameObject != this.transform.parent.GetChild(1).GetChild(0).gameObject)
                {
                    Debug.Log("I died!");
                    this.transform.position = new Vector3(100, 100, 10);
                    PlayerStat.Instance.increaseDeath(OwningNetWorker.Me.NetworkId);

                    Invoke("TimeInvoke", 5);
                }
                //if (col.GetComponent<SwordHash>().Hash != SwordHash.instance.Hash)
                //{
                //    //MovePlayer to inactive position
                //    PlayerWrapper.stats.Death++;
                //    this.transform.position = new Vector3(100, 100, 10);
                //    Invoke("TimeInvoke", 10);
                //}
            }
        }

        private void TimeInvoke()
        {
            this.transform.position = GetSpawnLocation();
        }
        Vector2 GetSpawnLocation()
        {
            var Locations = GameObject.Find("SpawnLocations").transform;
            int rng = Random.Range(0, Locations.childCount);

            return Locations.GetChild(rng).transform.position;
        }
    }
}