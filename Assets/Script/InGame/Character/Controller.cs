﻿using UnityEngine;
using BeardedManStudios.Network;
using System.Collections;

namespace Player
{
    public class Controller : NetworkedMonoBehavior
    {

        private Rigidbody2D _rigidbody;
        [HideInInspector]
        public UISlider slider;
        #region Movement Variables
        [Range(0, 20)]
        public float Speed = 5;
        [Range(0, 50)]
        public float MaxSpeed;
        [Range(0, 200)]
        public float RotateSpeed = 200;
        [Range(0, 20)]
        public float Acceleration = 1;
        [Range(0, 100)]
        public float Jumpforce;
        [Range(0, 10)]
        public float GroundOffset;
        [Range(0, 20)]
        public float BoostValue;
        [Range(0, 60)]
        public float BoostCooldown;

        [HideInInspector]
        public float MovementMultiplier = 1;
        [HideInInspector]
        public bool isBoostUsed = false;

        #endregion



        float Horizontal;
        bool touch = false;
        private float initialSpeed;
        int layermask;

        // Use this for initialization
        void Start()
        {
            if (NetworkingManager.IsOnline && !IsOwner)
                return;

            initialSpeed = Speed;

            slider = GameObject.Find("DashProgress").GetComponent<UISlider>(); // Get Dash ProgressSlider
            _rigidbody = this.GetComponent<Rigidbody2D>(); //Cache

            Camera.main.gameObject.GetComponent<CameraBound>().target = this.transform; //CameraBound-Target
            Camera.main.gameObject.GetComponent<CameraBound>().enabled = true;


            layermask = 1 << LayerMask.NameToLayer("Elements");

        }

        // Update is called once per frame
        void Update()
        {
            if (NetworkingManager.IsOnline && !IsOwner)
                return;

            Horizontal = Input.GetAxis("Horizontal");
            AdjustSpeed(Horizontal);
            transform.Rotate(Vector3.back, Horizontal * MovementMultiplier * RotateSpeed * Time.deltaTime * Speed, Space.Self);

            if (Input.GetMouseButton(0) == true && isBoostUsed == false && Horizontal != 0)
            {
                StartCoroutine("BoostCo");
                StartCoroutine("DashSliderCo");
            }

        }
        void FixedUpdate()
        {
            if (NetworkingManager.IsOnline && !IsOwner)
                return;

            _rigidbody.velocity = new Vector2(MovementMultiplier * Horizontal * Speed, _rigidbody.velocity.y);

            if (isGrounded() == true && Input.GetKey(KeyCode.W) && touch == true)
            {
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, Jumpforce);
            }
        }

        bool isGrounded()
        {
            return Physics2D.Linecast(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x, transform.position.y - GroundOffset), layermask);

        }
        void OnCollisionStay2D(Collision2D col)
        {

            if (col.collider.gameObject.layer == LayerMask.NameToLayer("Elements"))
            {
                touch = true;
            }

        }
        void OnCollisionExit2D(Collision2D col)
        {

            if (col.collider.gameObject.layer == LayerMask.NameToLayer("Elements"))
            {
                touch = false;
            }
        }

        void AdjustSpeed(float Input)
        {
            if (Input == 0)
            {
                Speed = initialSpeed;
                return;
            }
            if (Speed < MaxSpeed)
            {
                Speed += Acceleration;
                //! CHANGES SLOWLY Speed = Mathf.Lerp(Speed, MaxSpeed, Time.deltaTime * Acceleration);
            }


        }

        IEnumerator BoostCo() //TODO CHANGE DASH SYSTEM
        {
            Horizontal *= BoostValue;
            yield return new WaitForSeconds(0.05f);
            Horizontal /= BoostValue;

            isBoostUsed = true;

            float cooltime = BoostCooldown;
            while (cooltime != 0)
            {
                yield return new WaitForSeconds(1f);
                cooltime--;
            }

            isBoostUsed = false; //TODO IF FREEZED DONT ACTIVATE
        }
        IEnumerator DashSliderCo()
        {
            slider.value = 0;
            slider.alpha = 0.5f;
            float rate = 1 / BoostCooldown;
            float i = 0;
            while (i < 1)
            {
                i += Time.deltaTime * rate;
                slider.value = i;
                yield return null;
            }
            slider.alpha = 1;
        }








    }
}