﻿using UnityEngine;

namespace Player
{
    public class ParentFollow : Photon.MonoBehaviour
    {

        void Update()
        {
            this.transform.position = this.transform.parent.GetChild(0).position;

            if (!photonView.isMine)
            {
                SyncRotation();
            }

        }



        private double currentTime = 0;
        private double currentPackageTime = 0;
        private double lastTime = 0;
        private Quaternion startRotation = Quaternion.identity;
        private Quaternion endRotation = Quaternion.identity;

        void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {

            if (stream.isWriting)
            {
                endRotation = this.transform.rotation;
                stream.Serialize(ref endRotation);
            }
            else
            {
                stream.Serialize(ref endRotation);

                currentTime = 0;
                lastTime = currentPackageTime;
                currentPackageTime = info.timestamp;

                startRotation = this.transform.rotation;
            }

        }

        private void SyncRotation()
        {
            double travelTime = currentPackageTime - lastTime;
            currentTime += Time.deltaTime;

            this.transform.rotation = Quaternion.Lerp(startRotation, endRotation, (float)(currentTime / travelTime));
        }


    }
}