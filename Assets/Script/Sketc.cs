﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Sketc : MonoBehaviour
{
    public GameObject obj;
    public EventTriggerType eventType = EventTriggerType.BeginDrag;

    void Start()
    {
        //array = component.GetType().GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).Where(x => x.PropertyType.Name == "Single").ToArray();
        //foreach (var a in array)
        //{
        //    Debug.Log(a.Name + " : " + a.PropertyType.Name + " : " + a.MemberType.ToString());
        //]

        
    }

    public void AddEvents(Transform[] Selected, System.Action Method)
    {

        foreach (var i in Selected)
        {
            var eventComponent = i.gameObject.AddMissingComponent<EventTrigger>();


            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eventType;
            entry.callback = new EventTrigger.TriggerEvent();
            entry.callback.AddListener( (eventData) => { Method(); });

            eventComponent.triggers.Add(entry);
        }
    }

}
