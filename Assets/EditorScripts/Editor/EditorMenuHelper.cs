﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MenuHelper))]
public class EditorMenuHelper : Editor
{
    Vector2 scrollPos = new Vector2(0, 0);

    string GroupName = string.Empty;
    string State = "Ready";

    bool CheckName()
    {
        bool Valid = !string.IsNullOrEmpty(GroupName);

        if (!Valid)
            Debug.LogWarning("Enter A Group Name");

        return Valid;
    }

    public override void OnInspectorGUI()
    {
        MenuHelper script = target as MenuHelper;

        EditorGUILayout.LabelField("Group Count", script.GroupCount.ToString());


        GroupName = EditorGUILayout.TextField("Group Name", GroupName);

        #region Add
        bool Add_Pressed = GUILayout.Button("Add Selected");

        if (Add_Pressed)
        {
            if (CheckName())
            {
                //var Selected = Selection.GetTransforms(SelectionMode.Unfiltered);
                var Selected = Selection.activeTransform.GetComponentsInChildren<Transform>(true);
                script.AddGroup(GroupName, Selected);

                State = "Added " + GroupName;
            }
        }
        #endregion

        #region Remove
        bool Remove_Pressed = GUILayout.Button("Remove Group");

        if (Remove_Pressed)
        {
            if (CheckName())
            {
                bool Code = script.RemoveGroup(GroupName);

                if (Code == false)
                    Debug.LogWarning("Group name doesnt exist");
            }

        }
        #endregion

        #region RemoveAll
        bool RemoveAll_Pressed = GUILayout.Button("Remove All Groups");

        if (RemoveAll_Pressed)
        {
            script.RemoveAllGroups();

            State = "Removed all group data";
        }
        #endregion

        #region SaveToXml

        bool Save_Pressed = GUILayout.Button("Save To Xml");

        if (Save_Pressed)
        {
            bool Code = script.Save();

            if (Code == true)
                State = "Saved group data";
            else
                State = "No groups found";
        }

        #endregion

        #region LoadFromXml

        bool Load_Pressed = GUILayout.Button("Load From Xml");

        if (Load_Pressed)
        {
            bool Code = script.Load();

            if (Code)
                State = "Loaded group data";
            else
                State = "Loading failed";
        }

        #endregion

        #region State

        EditorGUILayout.LabelField("Status : " + State);

        #endregion

        #region ScrollGroup

        EditorGUILayout.LabelField("Groups :");


        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.MinHeight(250));

        string[] Groups = script.GetNameList();

        for (int i = 0; i < Groups.Length; i++)
        {
            bool Pressed = EditorGUILayout.Toggle(i + " : " + Groups[i], false);
            if (Pressed)
            {
                script.Trigger(Groups[i]);
                break;
            }
        }
        EditorGUILayout.EndScrollView();
        #endregion
    }
}
