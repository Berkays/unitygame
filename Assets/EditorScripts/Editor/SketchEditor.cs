﻿using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[CustomEditor(typeof(Sketc))]
public class SketchEditor : Editor
{

    string[] methodNames;

    int componentIndex = 0;
    int methodIndex = 0;
    string methodName = string.Empty;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Sketc script = (Sketc)target;

        if (script.obj != null)
        {
            var Components = script.obj.GetComponents(typeof(Component));

            var arr = from comp in Components
                      select comp.GetType().Name;
            var strArray = arr.ToArray();

            componentIndex = EditorGUILayout.Popup(componentIndex, strArray);


            var type = Components[componentIndex].GetType();

            var methodCollection = type.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);

            methodNames = methodCollection.Where(x => x.Name != "Start" || x.Name != "Update" || x.Name != "Awake").Select(x => x.Name).ToArray();
            

            methodIndex = EditorGUILayout.Popup(methodIndex, methodNames);
            methodName = methodNames[methodIndex];



            if (GUILayout.Button("Add event to selected"))
            {
                Transform[] selected = Selection.GetTransforms(SelectionMode.Deep);

                Action someAction = () =>
                {
                    methodCollection[methodIndex].Invoke(script.obj.GetComponent(type), new object[0]);
                };

                script.AddEvents(selected, someAction);

                foreach (var i in selected)
                {
                    EditorUtility.SetDirty(i);
                }
            }
        }





    }


}
