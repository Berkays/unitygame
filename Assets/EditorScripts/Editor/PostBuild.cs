﻿using UnityEngine;
using System.Collections;
using UnityEditor.Callbacks;
using UnityEditor;

public class PostBuild : ScriptableObject
{

    [PostProcessBuild(999)]
    static void OnPostProcessBuildPlayer(BuildTarget target, string buildPath)
    {
        if (target == BuildTarget.StandaloneWindows || target == BuildTarget.StandaloneWindows64)
        {
            //Move AssetLoader executable to Managed Folder
            MoveToManaged(buildPath);
            MoveMaps(buildPath);
        }
    }

    static void MoveToManaged(string path)
    {
        int cut = path.LastIndexOf('/');
        string sub = path.Substring(0, cut);
        string newPath = sub + "\\Game_Data\\Managed\\file.exe";
        System.IO.File.Copy(Application.dataPath + "\\Managed\\file.exe", newPath);
    }
    static void MoveMaps(string path)
    {
        int cut = path.LastIndexOf('/');
        string sub = path.Substring(0, cut);
        string newPath = sub + "\\Game_Data\\Maps\\";

        if (!System.IO.Directory.Exists(newPath))
            System.IO.Directory.CreateDirectory(newPath);

        foreach (var file in System.IO.Directory.GetFiles(Application.dataPath + "\\Maps\\"))
        {
            System.IO.FileInfo info = new System.IO.FileInfo(file);

            if (System.IO.File.Exists(newPath + info.Name))
                continue;

            if (!info.FullName.EndsWith(".map"))
                continue;

            System.IO.File.Copy(file, newPath + info.Name);
        }
    }
}
